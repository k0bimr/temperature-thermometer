#include "SIM.h"

ULLONG sim_determine_core_sys_clk_freq(void) {
    ULLONG mcgoutclk_freq;
    mcgoutclk_freq = mcg_determine_mcgoutclk_freq();
}

ULLONG sim_determine_bus_clk_freq(void) {
    ULLONG mcgoutclk_freq;
    int  bus_clk_divider = 0;
    long bus_clk_freq = 0;

    
    mcgoutclk_freq = mcg_determine_mcgoutclk_freq();
    

    bus_clk_divider = SIM_CLKDIV1_GET_OUTDIV2 + 1;

    if (bus_clk_divider != 0) {
        bus_clk_freq = mcgoutclk_freq / bus_clk_divider;
    }

    return bus_clk_freq;
}

ULLONG sim_determine_flash_clk_freq(void) {

}