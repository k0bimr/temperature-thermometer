#ifndef MK20DX256_H
#define MK20DX256_H

#include "util.h"
#include "SpiDispController.h"
#include "g_io_pin.h"
#include "MCG.h"


/* ADC (Analog to Digital Converter) Related Registers */
/* ADC0 */
#define ADC0_SC1A (DEREF_REG(0x4003B000))
    #define COCO      7
    #define AIEN      6
    #define DIFF      5
    #define ADCH4     4
    #define ADCH3     3
    #define ADCH2     2
    #define ADCH1     1
    #define ADCH0     0

    /* Successive-Approximation-Converter Disable */
    #define ADC_SAC_DISABLE  ((1 << ADCH4) | (1 << ADCH3)  | (1 << ADCH2) | (1 << ADCH1) | (1 << ADCH0))
    #define ADC_ADCH_BITMASK ((1 << ADCH4) | (1 << ADCH3)  | (1 << ADCH2) | (1 << ADCH1) | (1 << ADCH0))

    #define ADC_ENABLE_INTERRUPT (1 << AIEN)
    #define ADC_DISABLE_INTERRUPT (0 << AIEN)

#define ADC0_SC1B (DEREF_REG(0x4003B004))
#define ADC0_CFG1 (DEREF_REG(0x4003B008))
    #define ADLPC     7
    #define ADIV1     6
    #define ADIV0     5
    #define ADLSMP    4
    #define MODE1     3
    #define MODE0     2
    #define ADICLK1   1
    #define ADICLK0   0

    #define ADC_LOW_POWER_CONFIG     (1 << ADLPC)
    #define ADC_NORMAL_POWER_CONFIG  (0 << ADLPC)
    #define ADC_CLOCK_DIVIDE_RATIO_1 ((0 << ADIV1) | (0 << ADIV0))
    #define ADC_SHORT_SAMPLE_TIME    (0 << ADLSMP)
    #define ADC_LONG_SAMPLE_TIME     (1 << ADLSMP)

    #define ADC_BUS_CLOCK_INPUT_CLOCK ((0 << ADICLK1) | (0 << ADICLK0))

#define ADC0_CFG2 (DEREF_REG(0x4003B00C))
    #define MUXSEL   4
    #define ADACKEN  3
    #define ADHSC    2
    #define ADLSTS_1 1
    #define ADLSTS_0 0

    #define ADC_MUXSEL_ADxxa_CHANNELS (0 << MUXSEL)
    #define ADC_MUXSEL_ADxxb_CHANNELS (1 << MUXSEL)

#define ADC0_RA   (DEREF_REG(0x4003B010))
    #define ADC0_RA_D_BITMASK 0xFFFF
    
#define ADC0_RB   (DEREF_REG(0x4003B014))
#define ADC0_CV1  (DEREF_REG(0x4003B018))
#define ADC0_CV2  (DEREF_REG(0x4003B01C))
#define ADC0_SC2  (DEREF_REG(0x4003B020))
    #define ADACT     7
    #define ADTRG     6
    #define ACFE      5
    #define ACFGT     4
    #define ACREN     3
    #define DMAEN     2
    #define REFSEL1   1
    #define REFSEL0   0

    #define ADC_DISABLE_COMPARE_FUNCTION       (0 << ACFE)
    #define ADC_DISABLE_COMPARE_RANGE_FUNCTION (0 << ACREN)
    #define ADC_DISABLE_DMA                    (0 << DMAEN)
    #define ADC_DEFAULT_VOLT_REF_PIN_PAIR      ((0 << REFSEL1) | (0 << REFSEL0))
    #define ADC_ALTERNATIVE_VOLT_REF_PIN_PAIR  ((0 << REFSEL1) | (1 << REFSEL0))

#define ADC0_SC3  (DEREF_REG(0x4003B024))
    #define CAL       7
    #define CALF      6
    #define ADCO      3
    #define AVGE      2
    #define AVGS1     1
    #define AVGS0     0

    #define ADC_ENABLE_CONTINUOUS_CONVERSIONS  (0 << ADCO)
    #define ADC_DISABLE_CONTINUOUS_CONVERSIONS (1 << ADCO)
    #define ADC_DISABLE_HARDWARE_AVERAGE       (0 << AVGE)

#define ADC0_OFS  (DEREF_REG(0x4003B028))
#define ADC0_PG   (DEREF_REG(0x4003B02C))
#define ADC0_MG   (DEREF_REG(0x4003B030))
#define ADC0_CLPD (DEREF_REG(0x4003B034))
#define ADC0_CLPS (DEREF_REG(0x4003B038))
    #define CLPS_5    5
    #define CLPS_4    4
    #define CLPS_3    3
    #define CLPS_2    2
    #define CLPS_1    1
    #define CLPS_0    0

    #define ADC0_CLPS_BITMASK ((1 << CLPS_0) | (1 << CLPS_1) | (1 << CLPS_2) | (1 << CLPS_3) | (1 << CLPS_4) | (1 << CLPS_5))
    #define ADC0_CLPS_GET_CLPS (ADC0_CLPS & ADC0_CLPS_BITMASK)
#define ADC0_CLP4 (DEREF_REG(0x4003B03C))
    #define CLP4_9    9
    #define CLP4_8    8
    #define CLP4_7    7
    #define CLP4_6    6
    #define CLP4_5    5
    #define CLP4_4    4
    #define CLP4_3    3
    #define CLP4_2    2
    #define CLP4_1    1
    #define CLP4_0    0

    #define ADC0_CLP4_BITMASK ((1 << CLP4_0) | (1 << CLP4_1) | (1 << CLP4_2) | (1 << CLP4_3) | (1 << CLP4_4) | (1 << CLP4_5) | (1 << CLP4_6) | (1 << CLP4_7) | (1 << CLP4_8) | (1 << CLP4_9))
    #define ADC0_CLP4_GET_CLP4 (ADC0_CLP4 & ADC0_CLP4_BITMASK)
#define ADC0_CLP3 (DEREF_REG(0x4003B040))
    #define CLP3_8    8
    #define CLP3_7    7
    #define CLP3_6    6
    #define CLP3_5    5
    #define CLP3_4    4
    #define CLP3_3    3
    #define CLP3_2    2
    #define CLP3_1    1
    #define CLP3_0    0

    #define ADC0_CLP3_BITMASK ((1 << CLP3_0) | (1 << CLP3_1) | (1 << CLP3_2) | (1 << CLP3_3) | (1 << CLP3_4) | (1 << CLP3_5) | (1 << CLP3_6) | (1 << CLP3_7) | (1 << CLP3_8))
    #define ADC0_CLP3_GET_CLP3 (ADC0_CLP3 & ADC0_CLP3_BITMASK)
#define ADC0_CLP2 (DEREF_REG(0x4003B044))
    #define CLP2_7    7
    #define CLP2_6    6
    #define CLP2_5    5
    #define CLP2_4    4
    #define CLP2_3    3
    #define CLP2_2    2
    #define CLP2_1    1
    #define CLP2_0    0

    #define ADC0_CLP2_BITMASK ((1 << CLP2_0) | (1 << CLP2_1) | (1 << CLP2_2) | (1 << CLP2_3) | (1 << CLP2_4) | (1 << CLP2_5) | (1 << CLP2_6) | (1 << CLP2_7))
    #define ADC0_CLP2_GET_CLP2 (ADC0_CLP2 & ADC0_CLP2_BITMASK)
#define ADC0_CLP1 (DEREF_REG(0x4003B048))
    #define CLP1_6    6
    #define CLP1_5    5
    #define CLP1_4    4
    #define CLP1_3    3
    #define CLP1_2    2
    #define CLP1_1    1
    #define CLP1_0    0

    #define ADC0_CLP1_BITMASK ((1 << CLP1_0) | (1 << CLP1_1) | (1 << CLP1_2) | (1 << CLP1_3) | (1 << CLP1_4) | (1 << CLP1_5) | (1 << CLP1_6))
    #define ADC0_CLP1_GET_CLP1 (ADC0_CLP1 & ADC0_CLP1_BITMASK)
#define ADC0_CLP0 (DEREF_REG(0x4003B04C))
    #define CLP0_5    5
    #define CLP0_4    4
    #define CLP0_3    3
    #define CLP0_2    2
    #define CLP0_1    1
    #define CLP0_0    0

    #define ADC0_CLP0_BITMASK ((1 << CLP0_0) | (1 << CLP0_1) | (1 << CLP0_2) | (1 << CLP0_3) | (1 << CLP0_4) | (1 << CLP0_5))
    #define ADC0_CLP0_GET_CLP0 (ADC0_CLP0 & ADC0_CLP0_BITMASK)

#define ADC0_GET_CLP_CAL_VAL ( \
    ADC0_CLP0_GET_CLP0 + ADC0_CLP1_GET_CLP1 + \
    ADC0_CLP2_GET_CLP2 + ADC0_CLP3_GET_CLP3 + \
    ADC0_CLP4_GET_CLP4 + ADC0_CLPS_GET_CLPS \
)
#define ADC0_PGA  (DEREF_REG(0x4003B050))
#define ADC0_CLMD (DEREF_REG(0x4003B054))
#define ADC0_CLMS (DEREF_REG(0x4003B058))
    #define CLMS_5    5
    #define CLMS_4    4
    #define CLMS_3    3
    #define CLMS_2    2
    #define CLMS_1    1
    #define CLMS_0    0

    #define ADC0_CLMS_BITMASK ((1 << CLMS_0) | (1 << CLMS_1) | (1 << CLMS_2) | (1 << CLMS_3) | (1 << CLMS_4) | (1 << CLMS_5))
    #define ADC0_CLMS_GET_CLMS (ADC0_CLMS & ADC0_CLMS_BITMASK)
#define ADC0_CLM4 (DEREF_REG(0x4003B05C))
    #define CLM4_9    9
    #define CLM4_8    8
    #define CLM4_7    7
    #define CLM4_6    6
    #define CLM4_5    5
    #define CLM4_4    4
    #define CLM4_3    3
    #define CLM4_2    2
    #define CLM4_1    1
    #define CLM4_0    0

    #define ADC0_CLM4_BITMASK ((1 << CLM4_0) | (1 << CLM4_1) | (1 << CLM4_2) | (1 << CLM4_3) | (1 << CLM4_4) | (1 << CLM4_5) | (1 << CLM4_6) | (1 << CLM4_7) | (1 << CLM4_8) | (1 << CLM4_9))
    #define ADC0_CLM4_GET_CLM4 (ADC0_CLM4 & ADC0_CLM4_BITMASK)
#define ADC0_CLM3 (DEREF_REG(0x4003B060))
    #define CLM3_8    8
    #define CLM3_7    7
    #define CLM3_6    6
    #define CLM3_5    5
    #define CLM3_4    4
    #define CLM3_3    3
    #define CLM3_2    2
    #define CLM3_1    1
    #define CLM3_0    0

    #define ADC0_CLM3_BITMASK ((1 << CLM3_0) | (1 << CLM3_1) | (1 << CLM3_2) | (1 << CLM3_3) | (1 << CLM3_4) | (1 << CLM3_5) | (1 << CLM3_6) | (1 << CLM3_7) | (1 << CLM3_8))
    #define ADC0_CLM3_GET_CLM3 (ADC0_CLM3 & ADC0_CLM3_BITMASK)
#define ADC0_CLM2 (DEREF_REG(0x4003B064))
    #define CLM2_7    7
    #define CLM2_6    6
    #define CLM2_5    5
    #define CLM2_4    4
    #define CLM2_3    3
    #define CLM2_2    2
    #define CLM2_1    1
    #define CLM2_0    0

    #define ADC0_CLM2_BITMASK ((1 << CLM2_0) | (1 << CLM2_1) | (1 << CLM2_2) | (1 << CLM2_3) | (1 << CLM2_4) | (1 << CLM2_5) | (1 << CLM2_6) | (1 << CLM2_7))
    #define ADC0_CLM2_GET_CLM2 (ADC0_CLM2 & ADC0_CLM2_BITMASK)
#define ADC0_CLM1 (DEREF_REG(0x4003B068))
    #define CLM1_6    6
    #define CLM1_5    5
    #define CLM1_4    4
    #define CLM1_3    3
    #define CLM1_2    2
    #define CLM1_1    1
    #define CLM1_0    0

    #define ADC0_CLM1_BITMASK ((1 << CLM1_0) | (1 << CLM1_1) | (1 << CLM1_2) | (1 << CLM1_3) | (1 << CLM1_4) | (1 << CLM1_5) | (1 << CLM1_6))
    #define ADC0_CLM1_GET_CLM1 (ADC0_CLM1 & ADC0_CLM1_BITMASK)
#define ADC0_CLM0 (DEREF_REG(0x4003B06C))
    #define CLM0_5    5
    #define CLM0_4    4
    #define CLM0_3    3
    #define CLM0_2    2
    #define CLM0_1    1
    #define CLM0_0    0

    #define ADC0_CLM0_BITMASK ((1 << CLM0_0) | (1 << CLM0_1) | (1 << CLM0_2) | (1 << CLM0_3) | (1 << CLM0_4) | (1 << CLM0_5))
    #define ADC0_CLM0_GET_CLM0 (ADC0_CLM0 & ADC0_CLM0_BITMASK)

#define ADC0_GET_CLM_CAL_VAL ( \
    ADC0_CLM0_GET_CLM0 + ADC0_CLM1_GET_CLM1 + \
    ADC0_CLM2_GET_CLM2 + ADC0_CLM3_GET_CLM3 + \
    ADC0_CLM4_GET_CLM4 + ADC0_CLMS_GET_CLMS \
)

/* ADC1 */
#define ADC1_SC1A (DEREF_REG(0x400BB000))
#define ADC1_SC1B (DEREF_REG(0x400BB004))
#define ADC1_CFG1 (DEREF_REG(0x400BB008))
#define ADC1_CFG2 (DEREF_REG(0x400BB00C))
#define ADC1_RA   (DEREF_REG(0x400BB010))
#define ADC1_RB   (DEREF_REG(0x400BB014))
#define ADC1_CV1  (DEREF_REG(0x400BB018))
#define ADC1_CV2  (DEREF_REG(0x400BB01C))
#define ADC1_SC2  (DEREF_REG(0x400BB020))
#define ADC1_SC3  (DEREF_REG(0x400BB024))
#define ADC1_OFS  (DEREF_REG(0x400BB028))
#define ADC1_PG   (DEREF_REG(0x400BB02C))
#define ADC1_MG   (DEREF_REG(0x400BB030))
#define ADC1_CLPD (DEREF_REG(0x400BB034))
#define ADC1_CLPS (DEREF_REG(0x400BB038))
#define ADC1_CLP4 (DEREF_REG(0x400BB03C))
#define ADC1_CLP3 (DEREF_REG(0x400BB040))
#define ADC1_CLP2 (DEREF_REG(0x400BB044))
#define ADC1_CLP1 (DEREF_REG(0x400BB048))
#define ADC1_CLP0 (DEREF_REG(0x400BB04C))
#define ADC1_PGA  (DEREF_REG(0x400BB050))
#define ADC1_CLMD (DEREF_REG(0x400BB054))
#define ADC1_CLMS (DEREF_REG(0x400BB058))
#define ADC1_CLM4 (DEREF_REG(0x400BB05C))
#define ADC1_CLM3 (DEREF_REG(0x400BB060))
#define ADC1_CLM2 (DEREF_REG(0x400BB064))
#define ADC1_CLM1 (DEREF_REG(0x400BB068))
#define ADC1_CLM0 (DEREF_REG(0x400BB06C))

#define NVICISER0 (DEREF_REG(0xE000E100))
#define NVICISER1 (DEREF_REG(0xE000E104))
#define NVICISER2 (DEREF_REG(0xE000E108))
#define NVICISER3 (DEREF_REG(0xE000E10C))
#define NVICISER4 (DEREF_REG(0xE000E110))
#define NVICISER5 (DEREF_REG(0xE000E114))
#define NVICISER6 (DEREF_REG(0xE000E118))
#define NVICISER7 (DEREF_REG(0xE000E11C))

#endif