#include "ADC.h"

volatile UBYTE adc_config_calibrated = B_FALSE;


const UBYTE ard_pin_to_adc_ch_map[ARD_PIN_TO_ADC_CH_MAP_SIZE] = {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5
};

void adc_module_init(void) {
    /*
     * Initialization sequence:
     * 1. Calibrate the ADC by following the calibration instructions in Calibration function.
     *    ^^^^^
     *    [ Nonsense ]
     * 2. Update CFG to select the input clock source and the divide ratio used to generate
     *    ADCK. This register is also used for selecting sample time and low-power
     *    configuration.
     * 3. Update SC2 to select the conversion trigger, hardware or software, and compare
     *    function options, if enabled.
     * 4. Update SC3 to select whether conversions will be continuous or completed only once
     *    (ADCO) and whether to perform hardware averaging.
     * 5. Update SC1:SC1n registers to select whether conversions will be single-ended or
     *    differential and to enable or disable conversion complete interrupts. Also, select the
     *    input channel which can be used to perform conversions.
     * 6. Update the PGA register to enable or disable PGA and configure appropriate gain.
     *    This register is also used to select Power Mode and to check whether the module is
     *    chopper-stabilized.
     */
#if 1
    VREF_TRM = 0x60;
	VREF_SC  = 0xE1;		// enable 1.2 volt ref
#endif

    /* Following Pseudo-code example */
    ADC0_CFG1 = (
        ADC_NORMAL_POWER_CONFIG | ADC_CLOCK_DIVIDE_RATIO_1 | ADC_SHORT_SAMPLE_TIME | 
        ADC_8_BIT_CONV_MODE | ADC_BUS_CLOCK_INPUT_CLOCK
    );

    ADC0_CFG2 = (
        ADC_MUXSEL_ADxxb_CHANNELS | (0 << ADLSTS_0)
    );

    ADC0_SC2 = (
        ADC_SOFTWARE_TRIGGER_MODE | ADC_DISABLE_COMPARE_FUNCTION | 
        ADC_DISABLE_COMPARE_RANGE_FUNCTION | ADC_DISABLE_DMA | ADC_DEFAULT_VOLT_REF_PIN_PAIR
    );

    ADC0_SC3 = (
        ADC_DISABLE_CONTINUOUS_CONVERSIONS | ADC_DISABLE_HARDWARE_AVERAGE
    );

    /* Now we perform calibration after proper configuration */
    adc_perform_calibration();
}

void adc_initiate_calibration(void) {
    ADC_INITIATE_CALIBARTION;
}

/* Assumes that calibration has completed */
void adc_complete_calibration(void) {
    USHORT cal_val = 0;

     /* Start with positive side */
    cal_val = (
        ADC0_CLP0_GET_CLP0 + ADC0_CLP1_GET_CLP1 + 
        ADC0_CLP2_GET_CLP2 + ADC0_CLP3_GET_CLP3 + 
        ADC0_CLP4_GET_CLP4 + ADC0_CLPS_GET_CLPS
    );

    cal_val /= 2;

    cal_val |= (1 << ((sizeof(cal_val) * NUM_BITS_IN_BYTE) - 1));
    //cal_val |= 0x8000;
    ADC0_PG = cal_val;

    /* End with minus side */
    cal_val = 0;

    cal_val = (
        ADC0_CLM0_GET_CLM0 + ADC0_CLM1_GET_CLM1 + 
        ADC0_CLM2_GET_CLM2 + ADC0_CLM3_GET_CLM3 + 
        ADC0_CLM4_GET_CLM4 + ADC0_CLMS_GET_CLMS
    );

    cal_val /= 2;

    cal_val |= (1 << ((sizeof(cal_val) * NUM_BITS_IN_BYTE) - 1));
    //cal_val |= 0x8000;
    ADC0_MG = cal_val;

    adc_config_calibrated = B_TRUE;
}

/* Blocking method */
void adc_perform_calibration(void) {
    adc_initiate_calibration();

    /* Wait for the calibration process to finish */
    while (!ADC_CALIBRATION_COMPLETED);

    adc_complete_calibration();
}

int adc_initiate_conversion(UBYTE in_ch) {
    int ret = RET_OK;

    if (in_ch >= ADC_MAX_SINGLE_ENDED_CHANNELS) {
        ret = RET_FAIL;
    } else {
        ADC0_SC1A = (ADC_DISABLE_INTERRUPT | (0 << DIFF)) | (in_ch /*& ADC_ADCH_BITMASK*/);
    }

    return ret;
}

void adc_retrieve_conversion(int* analog_ret) {
    *analog_ret = ADC_RETRIEVE_CONVERSION;
}

int adc_perform_conversion(UBYTE in_ch, int* analog_ret) {
    /*
     * A conversion is initiated:
     * • Following a write to SC1A, with SC1n[ADCH] not all 1's, if software triggered
     *   operation is selected, that is, when SC2[ADTRG]=0.
     * • Following a hardware trigger, or ADHWT event, if hardware triggered operation is
     *   selected, that is, SC2[ADTRG]=1, and a hardware trigger select event, ADHWTSn,
     *   has occurred. The channel and status fields selected depend on the active trigger
     *   select signal:
     * • ADHWTSA active selects SC1A
     * • ADHWTSn active selects SC1n
     * • if neither is active, the off condition is selected
     * Note
     * Selecting more than one ADHWTSn prior to a conversion
     * completion will result in unknown results. To avoid this,
     * select only one ADHWTSn prior to a conversion
     * completion.
     * • Following the transfer of the result to the data registers when continuous conversion
     * is enabled, that is, when ADCO=1.
     * 
     * If continuous conversions are enabled, a new conversion is automatically initiated after
     * the completion of the current conversion, by:. In software triggered operation, that is,
     * when ADTRG=0, continuous conversions begin after SC1A is written and continue until
     * aborted. In hardware triggered operation, that is, when ADTRG=1 and one ADHWTSn
     * event has occurred, continuous conversions begin after a hardware trigger event and
     * continue until aborted.
     * 
     * If hardware averaging is enabled, a new conversion is automatically initiated after the
     * completion of the current conversion until the correct number of conversions are
     * completed. In software triggered operation, conversions begin after SC1A is written. In
     * hardware triggered operation, conversions begin after a hardware trigger. If continuous
     * conversions are also enabled, a new set of conversions to be averaged are initiated
     * following the last of the selected number of conversions.
     */
    int ret = RET_OK;

    ret = adc_initiate_conversion(in_ch);
    
    if (RET_OK == ret) {
        while (!ADC_CONVERSION_COMPLETED);

        adc_retrieve_conversion(analog_ret);
    }

    return ret;
}