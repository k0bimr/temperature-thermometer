#include "MCG.h"

typedef void (*MCG_MODE_ROUTINE)(void);

MCG_MODE mcg_transition_table[][NUM_MCG_MODES] = {
                          /* MCG MODE INPUTS */
          /* FEI | FEE | FBI | FBE | PEE | PBE | BLPI | BLPE | STOP */
/* FEI  */ { FEI,  FEE,  FBI,  FBE,  FEI,  FEI,  FEI,   FEI,   STOP },
/* FEE  */ { FEI,  FEE,  FBI,  FBE,  FEE,  FEE,  FEE,   FEE,   STOP },
/* FBI  */ { FEI,  FEE,  FBI,  FBE,  FBI,  FBI,  BLPI,  FBI,   STOP },
/* FBE  */ { FEI,  FEE,  FBI,  FBE,  FBE,  PBE,  FBE,   BLPE,  STOP },
/* PEE  */ { PEE,  PEE,  PEE,  PEE,  PEE,  PBE,  PEE,   PEE,   STOP },
/* PBE  */ { PBE,  PBE,  PBE,  FBE,  PEE,  PBE,  PBE,   BLPE,  STOP },
/* BLPI */ { BLPI, BLPI, FBI,  BLPI, BLPI, BLPI, BLPI,  BLPI,  STOP },
/* BLPE */ { BLPE, BLPE, BLPE, FBE,  BLPE, PBE,  BLPE,  BLPE,  STOP },
};

static MCG_MODE current_mode = FEI;

typedef enum CLK_SOURCES {
    FLL_CLK_SRC,
    PLL_CLK_SRC,
    INTREF_CLK_SRC,
    EXTREF_CLK_SRC
} CLK_SRC;

enum MCG_C1_CLKS_ENCODINGS {
    MCG_C1_CLKS_ENC_FLLPLL = 0x00,
    MCG_C1_CLKS_ENC_INTREF = 0x01,
    MCG_C1_CLKS_ENC_EXTREF = 0x02
};

enum MCG_C6_PLLS_ENCODINGS {
    MCG_C6_PLLS_FLL_CLK_SRC_SEL,
    MCG_C6_PLLS_PLL_CLK_SRC_SEL
};

enum MCG_C1_IREFS_ENCODINGS {
    MCG_C1_IREFS_EXTREF_CLK_SEL,
    MCG_C1_IREFS_INTREF_CLK_SEL
};

enum MCG_C2_IRCS_INT_REF_CLK_SELECTIONS {
    MCG_C2_IRCS_SLOW_INT_REF_CLK_SEL,
    MCG_C2_IRCS_FAST_INT_REF_CLK_SEL
};

enum MCG_C7_OSCSEL_EXTREF_CLK_SRC_SELECTIONS {
    MCG_C7_OSCSEL_SYS_OSCCLK,
    MCG_C7_OSCSEL_32kHz_RTC_OSC
};

typedef enum MCG_C5_PRDIV0_PLL_EXTREF_DIVIDERS {
    MCG_C5_PRDIV0_DIVIDE_FACTOR_RE = -1,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_1  = 1,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_2  = 2,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_3  = 3,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_4  = 4,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_5  = 5,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_6  = 6,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_7  = 7,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_8  = 8,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_9  = 9,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_10 = 10,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_11 = 11,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_12 = 12,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_13 = 13,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_14 = 14,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_15 = 15,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_16 = 16,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_17 = 17,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_18 = 18,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_19 = 19,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_20 = 20,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_21 = 21,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_22 = 22,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_23 = 23,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_24 = 24,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_25 = 25
} PLL_EXTREF_CLK_DIV_T;

PLL_EXTREF_CLK_DIV_T pll_extref_clk_dividers[] = {
    MCG_C5_PRDIV0_DIVIDE_FACTOR_1,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_2,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_3,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_4,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_5,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_6,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_7,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_8,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_9,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_10,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_11,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_12,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_13,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_14,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_15,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_16,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_17,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_18,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_19,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_20,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_21,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_22,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_23,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_24,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_25,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_RE,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_RE,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_RE,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_RE,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_RE,
    MCG_C5_PRDIV0_DIVIDE_FACTOR_RE
};

typedef enum MCG_C6_VDIV0_VC0_DIVIDERS {
    MCG_C6_VDIV0_MULT_FACTOR_24 = 24,
    MCG_C6_VDIV0_MULT_FACTOR_25 = 25,
    MCG_C6_VDIV0_MULT_FACTOR_26 = 26,
    MCG_C6_VDIV0_MULT_FACTOR_27 = 27,
    MCG_C6_VDIV0_MULT_FACTOR_28 = 28,
    MCG_C6_VDIV0_MULT_FACTOR_29 = 29,
    MCG_C6_VDIV0_MULT_FACTOR_30 = 30,
    MCG_C6_VDIV0_MULT_FACTOR_31 = 31,
    MCG_C6_VDIV0_MULT_FACTOR_32 = 32,
    MCG_C6_VDIV0_MULT_FACTOR_33 = 33,
    MCG_C6_VDIV0_MULT_FACTOR_34 = 34,
    MCG_C6_VDIV0_MULT_FACTOR_35 = 35,
    MCG_C6_VDIV0_MULT_FACTOR_36 = 36,
    MCG_C6_VDIV0_MULT_FACTOR_37 = 37,
    MCG_C6_VDIV0_MULT_FACTOR_38 = 38,
    MCG_C6_VDIV0_MULT_FACTOR_39 = 39,
    MCG_C6_VDIV0_MULT_FACTOR_40 = 40,
    MCG_C6_VDIV0_MULT_FACTOR_41 = 41,
    MCG_C6_VDIV0_MULT_FACTOR_42 = 42,
    MCG_C6_VDIV0_MULT_FACTOR_43 = 43,
    MCG_C6_VDIV0_MULT_FACTOR_44 = 44,
    MCG_C6_VDIV0_MULT_FACTOR_45 = 45,
    MCG_C6_VDIV0_MULT_FACTOR_46 = 46,
    MCG_C6_VDIV0_MULT_FACTOR_47 = 47,
    MCG_C6_VDIV0_MULT_FACTOR_48 = 48,
    MCG_C6_VDIV0_MULT_FACTOR_49 = 49,
    MCG_C6_VDIV0_MULT_FACTOR_50 = 50,
    MCG_C6_VDIV0_MULT_FACTOR_51 = 51,
    MCG_C6_VDIV0_MULT_FACTOR_52 = 52,
    MCG_C6_VDIV0_MULT_FACTOR_53 = 53,
    MCG_C6_VDIV0_MULT_FACTOR_54 = 54,
    MCG_C6_VDIV0_MULT_FACTOR_55 = 55
} PLL_EXTREF_CLK_MULT_T;

PLL_EXTREF_CLK_MULT_T pll_extref_clk_multipliers[] = {
    MCG_C6_VDIV0_MULT_FACTOR_24,
    MCG_C6_VDIV0_MULT_FACTOR_25,
    MCG_C6_VDIV0_MULT_FACTOR_26,
    MCG_C6_VDIV0_MULT_FACTOR_27,
    MCG_C6_VDIV0_MULT_FACTOR_28,
    MCG_C6_VDIV0_MULT_FACTOR_29,
    MCG_C6_VDIV0_MULT_FACTOR_30,
    MCG_C6_VDIV0_MULT_FACTOR_31,
    MCG_C6_VDIV0_MULT_FACTOR_32,
    MCG_C6_VDIV0_MULT_FACTOR_33,
    MCG_C6_VDIV0_MULT_FACTOR_34,
    MCG_C6_VDIV0_MULT_FACTOR_35,
    MCG_C6_VDIV0_MULT_FACTOR_36,
    MCG_C6_VDIV0_MULT_FACTOR_37,
    MCG_C6_VDIV0_MULT_FACTOR_38,
    MCG_C6_VDIV0_MULT_FACTOR_39,
    MCG_C6_VDIV0_MULT_FACTOR_40,
    MCG_C6_VDIV0_MULT_FACTOR_41,
    MCG_C6_VDIV0_MULT_FACTOR_42,
    MCG_C6_VDIV0_MULT_FACTOR_43,
    MCG_C6_VDIV0_MULT_FACTOR_44,
    MCG_C6_VDIV0_MULT_FACTOR_45,
    MCG_C6_VDIV0_MULT_FACTOR_46,
    MCG_C6_VDIV0_MULT_FACTOR_47,
    MCG_C6_VDIV0_MULT_FACTOR_48,
    MCG_C6_VDIV0_MULT_FACTOR_49,
    MCG_C6_VDIV0_MULT_FACTOR_50,
    MCG_C6_VDIV0_MULT_FACTOR_51,
    MCG_C6_VDIV0_MULT_FACTOR_52,
    MCG_C6_VDIV0_MULT_FACTOR_53,
    MCG_C6_VDIV0_MULT_FACTOR_54,
    MCG_C6_VDIV0_MULT_FACTOR_55
};

typedef enum MCG_C1_FRDIV_DIVIDERS {
    MCG_C1_FRDIV_DIV_1    = 1,
    MCG_C1_FRDIV_DIV_2    = 2,
    MCG_C1_FRDIV_DIV_4    = 4,
    MCG_C1_FRDIV_DIV_8    = 8,
    MCG_C1_FRDIV_DIV_16   = 16,
    MCG_C1_FRDIV_DIV_32   = 32,
    MCG_C1_FRDIV_DIV_64   = 64,
    MCG_C1_FRDIV_DIV_128  = 128,
    MCG_C1_FRDIV_DIV_256  = 256,
    MCG_C1_FRDIV_DIV_512  = 512,
    MCG_C1_FRDIV_DIV_1024 = 1024,
    MCG_C1_FRDIV_DIV_1280 = 1280,
    MCG_C1_FRDIV_DIV_1536 = 1536
} FLL_EXT_REF_DIV_T;

typedef enum MCG_C1_FRDIV_DIVIDER_ENCODINGS {
    MCG_C1_FRDIV_ENC_0,
    MCG_C1_FRDIV_ENC_1,
    MCG_C1_FRDIV_ENC_2,
    MCG_C1_FRDIV_ENC_3,
    MCG_C1_FRDIV_ENC_4,
    MCG_C1_FRDIV_ENC_5,
    MCG_C1_FRDIV_ENC_6,
    MCG_C1_FRDIV_ENC_7
} FRDIV_ENC_T;

typedef enum MCG_C2_RANGE0_FREQ_RANGES {
    MCG_C2_RANGE0_LOW_FREQ_RANGE,
    MCG_C2_RANGE0_HIGH_FREQ_RANGE,
    MCG_C2_RANGE0_VERY_HIGH_FREQ_RANGE
} RANGE0_FREQ_RANGE_T;

typedef enum MCG_C2_RANGE0_ENCODINGS {
    MCG_C2_RANGE0_ENC_0,
    MCG_C2_RANGE0_ENC_1,
    MCG_C2_RANGE0_ENC_2,
    MCG_C2_RANGE0_ENC_3
} RANGE0_ENC_T;

typedef enum MCG_C7_OSCSEL_FLL_EXT_REF_CLK_SELECTION {
    MCG_C7_OSCSEL_OSCCLK,
    MCG_C7_OSCSEL_OSCRTC
} FLL_EXT_REF_CLK_T;

typedef enum FLL_FACTORS {
    FLL_FACTOR_640  = 640,
    FLL_FACTOR_1280 = 1280,
    FLL_FACTOR_1920 = 1920,
    FLL_FACTOR_2560 = 2560
} FLL_FACTOR_T;

typedef enum MCG_C4_DRST_DRS_ENCODINGS {
    DRST_DRS_ENC_LOW_RANGE,
    DRST_DRS_ENC_MID_RANGE,
    DRST_DRS_ENC_MID_HIGH_RANGE,
    DRST_DRS_ENC_HIGH_RANGE
} DRST_DRS_ENC_T;

CLK_SRC mcg_determine_mcgoutclk_src(void);
void    mcg_set_extrefclk_divider_for_fll(void);
int     mcg_determine_fll_factor(void);
int     mcg_determine_fll_ext_ref_divider(void);
void    mcg_set_extrefclk_divider_for_fll(void);


int     mcg_switch_mode(MCG_MODE mcg_mode);

MCG_MODE_ROUTINE mcg_mode_routines[NUM_MCG_MODES] = {
    mcg_fei_mode,
    mcg_fee_mode,
    mcg_fbi_mode,
    mcg_fbe_mode,
    mcg_pee_mode,
    mcg_pbe_mode,
    mcg_blpi_mode,
    mcg_blpe_mode,
    mcg_stop_mode
};

int mcg_switch_mode(MCG_MODE mcg_mode) {
    MCG_MODE         next_mode    = FEI;
    MCG_MODE_ROUTINE mode_routine = NULL;

    next_mode    = mcg_transition_table[current_mode][mcg_mode];
    mode_routine = mcg_mode_routines[next_mode];

    if (NULL != mode_routine) {
        mode_routine();
    }

    current_mode = next_mode;
}

ULLONG mcg_determine_mcgoutclk_freq(void) {
    /* Relevant registers: 
     *   - MCG_C1[CLKS]
     *      Possible sources:
     *        - FLL
     *        - PLL
     *        - Internal Reference Clock
     *        - External Reference Clock
     *   - MCG_C1[FRDIV] (if FLL is selected as MCGOUTCLK SRC)
     *   - MCG_C1[IREFS] (whether internal reference selected as source for FLL)
     *   - MCG_C2[RANGE0]
     *   - MCG_C4[DMX32]
     *   - MCG_C4[DRST_DRS]
     *   - MCG_C5[PLLCLKEN0]
     *   - MCG_C5[PRDIV0]
     *   - MCG_C6[PLLS]
     *   - MCG_C6[VIDV0]
     *   - MCG_C7[OSCSEL]
     */

    /* 1. Determine clock source for MCGOUTCLK */
    ULLONG                mcgoutclk_freq           = 0;
    CLK_SRC               mcgoutclk_src            = mcg_determine_mcgoutclk_src();
    PLL_EXTREF_CLK_DIV_T  pll_extref_divide_factor = 0;
    UBYTE                 mcg_c5_prdiv0_reg_val    = 0;
    PLL_EXTREF_CLK_MULT_T pll_extref_mult_factor   = 0;
    UBYTE                 mcg_c6_vdiv0_reg_val     = 0;
    

    SET_PIN(&GPIOC_PSOR, 5);
    if (FLL_CLK_SRC == mcgoutclk_src) {
        /* if FLL then need to know source clk for the FLL */
        if (MCG_C1_IREFS_INTREF_CLK_SEL == ((MCG_C1 & (1 << IREFS)) >> IREFS)) {
            mcgoutclk_freq = SLOW_IRC_FREQ * mcg_determine_fll_factor();
        } else if (MCG_C1_IREFS_EXTREF_CLK_SEL == ((MCG_C1 & (1 << IREFS)) >> IREFS)) {
            mcgoutclk_freq = (XTAL_EXT_REF_CLK_FREQ / mcg_determine_fll_ext_ref_divider()) * mcg_determine_fll_factor();
        }
    } else if (PLL_CLK_SRC == mcgoutclk_src) {
        /* Source clk for PLL seems to always be the external 32.768 kHz ref. clk */

        mcg_c5_prdiv0_reg_val = MCG_C5_GET_PRDIV0;
        if (mcg_c5_prdiv0_reg_val < sizeof(pll_extref_clk_dividers)) {
            pll_extref_divide_factor = pll_extref_clk_dividers[mcg_c5_prdiv0_reg_val];
            mcg_c6_vdiv0_reg_val = MCG_C6_GET_VDIV0;
            if (mcg_c6_vdiv0_reg_val < sizeof(pll_extref_clk_multipliers)) {
                pll_extref_mult_factor = pll_extref_clk_multipliers[mcg_c6_vdiv0_reg_val];
                mcgoutclk_freq = (XTAL_EXT_REF_CLK_FREQ / pll_extref_divide_factor) * pll_extref_mult_factor;
            }
        }
        

    } else if (INTREF_CLK_SRC == mcgoutclk_src) {
        if (MCG_C2_IRCS_SLOW_INT_REF_CLK_SEL == MCG_C2_GET_IRCS) {
            mcgoutclk_freq = SLOW_IRC_FREQ;
        } else if (MCG_C2_IRCS_SLOW_INT_REF_CLK_SEL == MCG_C2_GET_IRCS) {
            mcgoutclk_freq = FAST_IRC_FREQ;
        }
    } else if (EXTREF_CLK_SRC == mcgoutclk_src) {
        /* In FBE mode, the MCGOUTCLK is derived from the OSCSEL external reference clock */
        if (MCG_C7_OSCSEL_OSCCLK == MCG_C7_GET_OSCSEL) {
            mcgoutclk_freq = XTAL_EXT_REF_CLK_FREQ;
        } else if (MCG_C7_OSCSEL_32kHz_RTC_OSC == MCG_C7_GET_OSCSEL) {
            mcgoutclk_freq = XTAL_EXT_REF_CLK_FREQ;
        }
    } else {
        // Panic mode
    }
    SET_PIN(&GPIOC_PCOR, 5);

    return mcgoutclk_freq;
}

CLK_SRC mcg_determine_mcgoutclk_src(void) {
    /* 1. Determine clock source for MCGOUTCLK */
    CLK_SRC clk_src_ret = MCG_GET_MCGOUTCLK_SRC;

    if (MCG_C1_CLKS_ENC_FLLPLL == clk_src_ret) {
        if (MCG_C6_PLLS_FLL_CLK_SRC_SEL == ((MCG_C6 & (1 << PLLS)) >> PLLS)) {
            clk_src_ret = FLL_CLK_SRC;
        } else if (MCG_C6_PLLS_PLL_CLK_SRC_SEL == ((MCG_C6 & (1 << PLLS)) >> PLLS)) {
            clk_src_ret = PLL_CLK_SRC;
        }
    } else if (MCG_C1_CLKS_ENC_INTREF == clk_src_ret) {
        clk_src_ret = INTREF_CLK_SRC;
    } else if (MCG_C1_CLKS_ENC_EXTREF == clk_src_ret) {
        clk_src_ret = EXTREF_CLK_SRC;
    }

    return clk_src_ret;
}

int mcg_determine_fll_factor(void) {
    FLL_FACTOR_T fll_factor_ret;
    DRST_DRS_ENC_T dco_range_select;

    dco_range_select = MCG_C4_GET_DRST_DRS;
    if (DRST_DRS_ENC_LOW_RANGE == dco_range_select) {
        fll_factor_ret = FLL_FACTOR_640;
    } else if (DRST_DRS_ENC_MID_RANGE == dco_range_select) {
        fll_factor_ret = FLL_FACTOR_1280;
    } else if (DRST_DRS_ENC_MID_HIGH_RANGE == dco_range_select) {
        fll_factor_ret = FLL_FACTOR_1920;
    } else if (DRST_DRS_ENC_HIGH_RANGE == dco_range_select) {
        fll_factor_ret = FLL_FACTOR_2560;
    }

    return fll_factor_ret;
}

int mcg_determine_fll_ext_ref_divider(void) {
    FLL_EXT_REF_DIV_T fll_ext_ref_div_ret = 0;
    FRDIV_ENC_T       frdiv_reg_val       = 0;

    frdiv_reg_val = MCG_C1_GET_FRDIV;

    if (MCG_C1_FRDIV_ENC_0 == frdiv_reg_val) {
        if ((MCG_C2_RANGE0_LOW_FREQ_RANGE == MCG_C2_GET_RANGE0) || (MCG_C7_OSCSEL_OSCRTC == MCG_C7_GET_OSCSEL)) {
            fll_ext_ref_div_ret = MCG_C1_FRDIV_DIV_1;
        } else {
            fll_ext_ref_div_ret = MCG_C1_FRDIV_DIV_32;
        }
    } else if (MCG_C1_FRDIV_ENC_1 == frdiv_reg_val) {
        if ((MCG_C2_RANGE0_LOW_FREQ_RANGE == MCG_C2_GET_RANGE0) || (MCG_C7_OSCSEL_OSCRTC == MCG_C7_GET_OSCSEL)) {
            fll_ext_ref_div_ret = MCG_C1_FRDIV_DIV_2;
        } else {
            fll_ext_ref_div_ret = MCG_C1_FRDIV_DIV_64;
        }
    } else if (MCG_C1_FRDIV_ENC_2 == frdiv_reg_val) {
        if ((MCG_C2_RANGE0_LOW_FREQ_RANGE == MCG_C2_GET_RANGE0) || (MCG_C7_OSCSEL_OSCRTC == MCG_C7_GET_OSCSEL)) {
            fll_ext_ref_div_ret = MCG_C1_FRDIV_DIV_4;
        } else {
            fll_ext_ref_div_ret = MCG_C1_FRDIV_DIV_128;
        }
    } else if (MCG_C1_FRDIV_ENC_3 == frdiv_reg_val) {
        if ((MCG_C2_RANGE0_LOW_FREQ_RANGE == MCG_C2_GET_RANGE0) || (MCG_C7_OSCSEL_OSCRTC == MCG_C7_GET_OSCSEL)) {
            fll_ext_ref_div_ret = MCG_C1_FRDIV_DIV_8;
        } else {
            fll_ext_ref_div_ret = MCG_C1_FRDIV_DIV_256;
        }
    } else if (MCG_C1_FRDIV_ENC_4 == frdiv_reg_val) {
        if ((MCG_C2_RANGE0_LOW_FREQ_RANGE == MCG_C2_GET_RANGE0) || (MCG_C7_OSCSEL_OSCRTC == MCG_C7_GET_OSCSEL)) {
            fll_ext_ref_div_ret = MCG_C1_FRDIV_DIV_16;
        } else {
            fll_ext_ref_div_ret = MCG_C1_FRDIV_DIV_512;
        }
    } else if (MCG_C1_FRDIV_ENC_5 == frdiv_reg_val) {
        if ((MCG_C2_RANGE0_LOW_FREQ_RANGE == MCG_C2_GET_RANGE0) || (MCG_C7_OSCSEL_OSCRTC == MCG_C7_GET_OSCSEL)) {
            fll_ext_ref_div_ret = MCG_C1_FRDIV_DIV_32;
        } else {
            fll_ext_ref_div_ret = MCG_C1_FRDIV_DIV_1024;
        }
    } else if (MCG_C1_FRDIV_ENC_6 == frdiv_reg_val) {
        if ((MCG_C2_RANGE0_LOW_FREQ_RANGE == MCG_C2_GET_RANGE0) || (MCG_C7_OSCSEL_OSCRTC == MCG_C7_GET_OSCSEL)) {
            fll_ext_ref_div_ret = MCG_C1_FRDIV_DIV_64;
        } else {
            fll_ext_ref_div_ret = MCG_C1_FRDIV_DIV_1280;
        }
    } else if (MCG_C1_FRDIV_ENC_7 == frdiv_reg_val) {
        if ((MCG_C2_RANGE0_LOW_FREQ_RANGE == MCG_C2_GET_RANGE0) || (MCG_C7_OSCSEL_OSCRTC == MCG_C7_GET_OSCSEL)) {
            fll_ext_ref_div_ret = MCG_C1_FRDIV_DIV_128;
        } else {
            fll_ext_ref_div_ret = MCG_C1_FRDIV_DIV_1536;
        }
    }

    return fll_ext_ref_div_ret;
}

void mcg_set_extrefclk_divider_for_fll(void) {
    /* Set C1[FRDIV] s.t. ext.ref. clk is between 31.25 - 39.0625 kHz */
    /* If OSCSEL is 1 then we're using external oscillator (16 MHz ?)*/
    /* Need to check C2[RANGE0] and C7[OSCSEL] */
    MCG_SET_FLL_EXTREF_DIV_512;
}

/*
 * FLL engaged internal (FEI) is the default mode of operation and is entered when all the following
 * condtions occur:
 * • C1[CLKS] bits are written to 00
 * • C1[IREFS] bit is written to 1
 * • C6[PLLS] bit is written to 0
 * In FEI mode, MCGOUTCLK is derived from the FLL clock (DCOCLK) that is controlled by the 32
 * kHz Internal Reference Clock (IRC). The FLL loop will lock the DCO frequency to the FLL factor, as
 * selected by C4[DRST_DRS] and C4[DMX32] bits, times the internal reference frequency. See the
 * C4[DMX32] bit description for more details. In FEI mode, the PLL is disabled in a low-power state
 * unless C5[PLLCLKEN0] is set.
 */
void mcg_fei_mode(void) {
    MCG_SET_MCGCOUTCLK_SRC_FLLPLL;
    MCG_SET_FLL_SRC_CLK_INTREF;
    MCG_SET_PLLS_FLL_SEL;
}

/*
 * FLL engaged external (FEE) mode is entered when all the following conditions occur:
 * • C1[CLKS] bits are written to 00
 * • C1[IREFS] bit is written to 0
 * • C1[FRDIV] must be written to divide external reference clock to be within the range of 31.25
 * kHz to 39.0625 kHz
 * • C6[PLLS] bit is written to 0
 * In FEE mode, MCGOUTCLK is derived from the FLL clock (DCOCLK) that is controlled by the
 * external reference clock. The FLL loop will lock the DCO frequency to the FLL factor, as selected by
 * C4[DRST_DRS] and C4[DMX32] bits, times the external reference frequency, as specified by
 * C1[FRDIV] and C2[RANGE0]. See the C4[DMX32] bit description for more details. In FEE mode,
 * the PLL is disabled in a low-power state unless C5[PLLCLKEN0] is set.
 */
void mcg_fee_mode(void) {
    MCG_SET_MCGCOUTCLK_SRC_FLLPLL;
    MCG_SET_FLL_SRC_CLK_EXTREF;
    
    mcg_set_extrefclk_divider_for_fll();

    MCG_SET_PLLS_FLL_SEL;
}

/*
 * FLL bypassed internal (FBI) mode is entered when all the following conditions occur:
 * • C1[CLKS] bits are written to 01
 * • C1[IREFS] bit is written to 1
 * • C6[PLLS] is written to 0
 * • C2[LP] is written to 0
 * In FBI mode, the MCGOUTCLK is derived either from the slow (32 kHz IRC) or fast (2 MHz IRC)
 * internal reference clock, as selected by the C2[IRCS] bit. The FLL is operational but its output is not
 * used. This mode is useful to allow the FLL to acquire its target frequency while the MCGOUTCLK is
 * driven from the C2[IRCS] selected internal reference clock. The FLL clock (DCOCLK) is controlled
 * by the slow internal reference clock, and the DCO clock frequency locks to a multiplication factor, as
 * selected by C4[DRST_DRS] and C4[DMX32] bits, times the internal reference frequency. See the
 * C4[DMX32] bit description for more details. In FBI mode, the PLL is disabled in a low-power state
 * unless C5[PLLCLKEN0] is set.
 */
void mcg_fbi_mode(void) {

}

/*
 * FLL bypassed external (FBE) mode is entered when all the following conditions occur:
 * • C1[CLKS] bits are written to 10
 * • C1[IREFS] bit is written to 0
 * • C1[FRDIV] must be written to divide external reference clock to be within the range of 31.25
 * kHz to 39.0625 kHz.
 * • C6[PLLS] bit is written to 0
 * • C2[LP] is written to 0
 * In FBE mode, the MCGOUTCLK is derived from the OSCSEL external reference clock. The FLL is
 * operational but its output is not used. This mode is useful to allow the FLL to acquire its target
 * frequency while the MCGOUTCLK is driven from the external reference clock. The FLL clock
 * (DCOCLK) is controlled by the external reference clock, and the DCO clock frequency locks to a
 * multiplication factor, as selected by C4[DRST_DRS] and C4[DMX32] bits, times the divided external
 * reference frequency. See the C4[DMX32] bit description for more details. In FBI mode the PLL is
 * disabled in a low-power state unless C5[PLLCLKEN0] is set.
 */
void mcg_fbe_mode(void) {
    OSC_SET_10_pF_CAPACITANCE_LOAD;
    OSC_ENABLE_OSCERCLK;

    MCG_XTAL_FREQ_RANGE_SET_VERY_HIGH;
    MCG_EXTREFCLK_SRC_OSC_SEL;
    MCG_DISABLE_LOW_POWER;

    mcg_set_extrefclk_divider_for_fll();
    MCG_SET_FLL_SRC_CLK_EXTREF;
    MCG_SET_PLLS_FLL_SEL;
    MCG_SET_MCGCOUTCLK_SRC_EXTREF;
    
    MCG_AWAIT_OSC_INITIALIZATION;
    MCG_AWAIT_FLL_EXTREF_CLK_SRC;
    
    MCG_AWAIT_MCGOUTCLK_EXTREF_CLK_SRC;
}

/*
 * PLL Engaged External (PEE) mode is entered when all the following conditions occur:
 * • C1[CLKS] bits are written to 00
 * • C1[IREFS] bit is written to 0
 * • C6[PLLS] bit is written to 1
 * In PEE mode, the MCGOUTCLK is derived from the PLL clock, which is controlled by the external
 * reference clock. The PLL clock frequency locks to a multiplication factor, as specified by C6[VDIV0],
 * times the external reference frequency, as specified by C5[PRDIV0]. The PLL's programmable
 * reference divider must be configured to produce a valid PLL reference clock. The FLL is disabled in
 * a low-power state.
 */
void mcg_pee_mode(void) {
    /* Time to do some clock dividing */
    SIM_CLKDIV1 = (
        SIM_CLKDIV1_OUTDIV1_DIV_FACTOR_4 | 
        SIM_CLKDIV1_OUTDIV2_DIV_FACTOR_4 | 
        SIM_CLKDIV1_OUTDIV4_DIV_FACTOR_4
    );
    MCG_SET_MCGCOUTCLK_SRC_FLLPLL;
    MCG_AWAIT_MCGOUTCLK_SRC_PLL_SEL;  
}

/*
 * PLL Bypassed External (PBE) mode is entered when all the following conditions occur:
 * • C1[CLKS] bits are written to 10
 * • C1[IREFS] bit is written to 0
 * • C6[PLLS] bit is written to 1
 * • C2[LP] bit is written to 0
 * In PBE mode, MCGOUTCLK is derived from the OSCSEL external reference clock; the PLL is
 * operational, but its output clock is not used. This mode is useful to allow the PLL to acquire its target
 * frequency while MCGOUTCLK is driven from the external reference clock. The PLL clock frequency
 * locks to a multiplication factor, as specified by its [VDIV], times the PLL reference frequency, as
 * specified by its [PRDIV]. In preparation for transition to PEE, the PLL's programmable reference
 * divider must be configured to produce a valid PLL reference clock. The FLL is disabled in a lowpower
 * state.
 */
void mcg_pbe_mode(void) {
    MCG_SET_PLL_DIV_FACTOR_4;
    MCG_SET_VCO_MULT_FACTOR_24;
    MCG_SET_MCGCOUTCLK_SRC_EXTREF;
    MCG_SET_FLL_SRC_CLK_EXTREF;
    MCG_SET_PLLS_PLL_SEL;
    MCG_DISABLE_LOW_POWER;

    
    MCG_SET_PLLS_PLL_SEL;

    MCG_AWAIT_PLL_CLK;
    MCG_AWAIT_PLL_LOCK_ACQUIRED;
}

/* 
 * Bypassed Low Power Internal (BLPI) mode is entered when all the following conditions occur:
 * • C1[CLKS] bits are written to 01
 * • C1[IREFS] bit is written to 1
 * • C6[PLLS] bit is written to 0
 * • C2[LP] bit is written to 1
 * In BLPI mode, MCGOUTCLK is derived from the internal reference clock. The FLL is disabled and
 * PLL is disabled even if the C5[PLLCLKEN0] is set to 1.
 */
void mcg_blpi_mode(void) {

}

/* 
 * Bypassed Low Power External (BLPE) mode is entered when all the following conditions occur:
 * • C1[CLKS] bits are written to 10
 * • C1[IREFS] bit is written to 0
 * • C2[LP] bit is written to 1
 * In BLPE mode, MCGOUTCLK is derived from the OSCSEL external reference clock. The FLL is
 * disabled and PLL is disabled even if the C5[PLLCLKEN0] is set to 1.
 */
void mcg_blpe_mode(void) {

}

void mcg_stop_mode(void) {

}