#ifndef SPI_DISP_CONTROLLER_H
#define SPI_DISP_CONTROLLER_H

/*============== Header Inclusion Flags ==============*/
#define INCLUDE_ADC_MODULE
#define INCLUDE_GPIO_MODULE
#undef  INCLUDE_PUSH_BUTTON
#define INCLUDE_MCG_MODULE
#define INCLUDE_SIM_MODULE

#include <stdio.h>

#define B_FALSE 0
#define B_TRUE  1
#define TEMP_THERMO_APP

typedef uint8_t            UBYTE;
typedef volatile uint32_t* REG_UL_T;
typedef volatile uint16_t* REG_US_T;
typedef volatile uint8_t*  REG_UB_T;
#if 0
#if defined(__MK20DX256__)
typedef volatile uint32_t* REG_T;
#else
typedef volatile uint8_t*  REG_T;
#endif
#endif

typedef uint32_t           REG_BITMASK;
typedef unsigned long      ULONG;
typedef unsigned long long ULLONG;
typedef uint16_t           USHORT;

typedef enum PIN_DATA_DIRECTION {
    PDD_OUT,
    PDD_IN,
    PDD_BI_DIR
} PDD_T;

enum COMMON_RET_CODES {
    RET_OK            =  0,
    RET_FAIL          = -1,
    RET_INVALID_PARAM = -2
};

#ifdef INCLUDE_MCG_MODULE
#include "MCG.h"
#endif

#ifdef INCLUDE_SIM_MODULE
#include "SIM.h"
#endif

#endif