#ifndef DS18B20_H
#define DS18B20_H
#include "SpiDispController.h"
#include "g_io_pin.h"
#include "util.h"

typedef enum DS18B20_ROM_COMMANDS {
    DS18B20_SEARCH_ROM   = 0xF0,
    DS18B20_READ_ROM     = 0x33,
    DS18B20_MATCH_ROM    = 0x55,
    DS18B20_SKIP_ROM     = 0xCC,
    DS18B20_ALARM_SEARCH = 0xEC
} DS18B20_ROM;

typedef enum DS18B20_FUNCTION_COMMANDS {
    DS18B20_CONVERT_TEMP      = 0x44,
    DS18B20_WRITE_SCRATCHPAD  = 0x4E,
    DS18B20_READ_SCRATCHPAD   = 0xBE,
    DS18B20_COPY_SCRATCHPAD   = 0x48,
    DS18B20_RECALL_E2         = 0xB8,
    DS18B20_READ_POWER_SUPPLY = 0xB4
} DS18B20_FUNC;

extern UBYTE parasite_power;
extern PIOP_CONF ds18b20_dq_pin;

void  DS18B20_init(void);
int   DS18B20_get_temp(float* temp);
int   DS18B20_send_data(UBYTE data, int num_bits_write);
UBYTE DS18B20_read_data(int num_bits_read);
int   DS18B20_read_string_data(UBYTE* data, int data_size, int num_bits_read);

#endif