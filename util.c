#include "util.h"

#define VECT_SIZE_CONST 3

/* PIT frequency comes from bus clock which is ~21MHz in FEI mode */
#define PIT_FREQ   20970000ULL   // in Hz (20.97 mHz)
#define NS_PER_CLK 48ULL         // in ns
#define S_TO_NS    1000000000ULL
#define MS_TO_NS   1000000ULL
#define US_TO_NS   1000ULL

ULLONG g_pit_ns_per_clk_cycle;
ULLONG g_cycles_rem;

#ifdef INCLUDE_SIM_MODULE
#endif
#if 1
ULLONG pit_calculate_ns_per_clk_cycle(void) {
    ULLONG bus_clk_freq             = 0;
    ULLONG pit_ns_per_clk_cycle_ret = 0;

    SET_PIN(&GPIOC_PSOR, 5);
    bus_clk_freq = sim_determine_bus_clk_freq();
    if (bus_clk_freq != 0) {
        pit_ns_per_clk_cycle_ret = (S_TO_NS / bus_clk_freq);
    }
    loop_delay(100);
    SET_PIN(&GPIOC_PCOR, 5);
    g_cycles_rem = bus_clk_freq;
    return pit_ns_per_clk_cycle_ret;
}   
#endif

void timers_init(void) {
    /* Enable peripheral bus clock to PIT module */
    SIM_SCGC6 |= (1 << PIT);

    /* Ensure the PIT Module is disabled before doing setups */
    PIT_MCR &= ~(1 << MDIS);

    /* Establish the working PIT frequencies */
    g_pit_ns_per_clk_cycle = pit_calculate_ns_per_clk_cycle();
}

void _delay_us(ULONG delayus) {
    ULONG cycles = 0;

    cycles = (delayus * US_TO_NS) / g_pit_ns_per_clk_cycle;
    _timer_delay(cycles);
}

void _delay_us_new(ULONG delayus) {
    ULLONG delayns;

    delayns = (delayus * US_TO_NS);
    _timer_delay(delayns);
}

void _delay_ms(ULONG delayms) {
    ULONG cycles = 0;

    cycles = (delayms * MS_TO_NS) / g_pit_ns_per_clk_cycle;
    _timer_delay(cycles);
}

void _delay_ms_new(ULONG delayms) {
    ULLONG delayns;

    delayns = ((ULLONG)delayms * MS_TO_NS);
    _timer_delay(delayns);
}

void _timer_delay(ULONG cycles) {
    if (cycles > 0) {
        PIT_LDVAL1 = cycles - 1;
    } else {
        PIT_LDVAL1 = 0;
    }
    //PIT_TCTRL1  = (1 << TIE);
    PIT_TCTRL1 |= (1 << TEN);

    /* Hold until the timer expires */
    while (!(PIT_TFLG1 & (1 << TIF)));
    //while (PIT_TIMER_N_EXPIRED(PIT_TIMER_1))

    PIT_TFLG1  |= (1 << TIF);
    PIT_TCTRL1 &= ~(1 << TEN);
}

void _timer_delay_new(ULLONG delayns) {
    ULONG cycles     = 0;
    ULONG ns_per_clk = 0;

    ns_per_clk = pit_calculate_ns_per_clk_cycle();

    if (ns_per_clk != 0) {
        cycles = delayns / ns_per_clk;
    }

    PIT_LDVAL1 = cycles - 1;
    PIT_TCTRL1  = (1 << TIE);
    PIT_TCTRL1 |= (1 << TEN);

    /* Hold until the timer expires */
    while (!(PIT_TFLG1 & (1 << TIF)));

    PIT_TFLG1  |= PIT_TIMER_INTERRUPT_FLAG_CLEAR;
    PIT_TCTRL1 &= ~(1 << TEN);
}

void loop_delay(ULONG ms) {
    int i;
    int q;

    for (i = 0; i < ms; i++) {
        for (q = 0; q < 2000; q++) {

        }
    }
}

void pit_set_timer(ULONG ms, ULONG us, ULONG ns) {
    ULLONG total_ns = 0;
    ULONG  cycles   = 0;

    total_ns = (MS_TO_NS * ms) + (US_TO_NS * us) + ns;
    cycles   = total_ns / g_pit_ns_per_clk_cycle;

    if (cycles > 0) {
        PIT_LDVAL0 = cycles - 1;
    } else {
        PIT_LDVAL0 = 0;
    }
    
    PIT_TCTRL0 |= PIT_TIMER_INTERRUPT_ENABLE;
    PIT_TCTRL0 |= PIT_TIMER_ENABLE;
}

int number_get_digit_count(int num) {
    int num_digits = 1;

    if (num > 0) {
        num_digits = log10(num) + 1;
    }

    return num_digits;
}

/* Matrix/Vector utility functions */
int matrix_multiply(float* matx_a, float* matx_b, UBYTE matx_a_num_rows, UBYTE matx_a_num_cols, UBYTE matx_b_num_rows, UBYTE matx_b_num_cols, float* matx_prod_out) {
    int ret = RET_OK;
    int row = 0;
    int col = 0;

    float col_vect[VECT_SIZE_CONST] = { 0 };

    if (NULL == matx_a || NULL == matx_b || NULL == matx_prod_out) {
        ret = RET_FAIL;
    } else if (matx_a_num_cols != matx_b_num_rows) {
        ret = RET_FAIL;
    } else {
        for (row = 0; row < matx_a_num_rows; row++) {
            for (col = 0; col < matx_b_num_cols; col++) {
                col_vect[0] = *((matx_b + (0 * 3)) + col);
                col_vect[1] = *((matx_b + (1 * 3)) + col);
                col_vect[2] = *((matx_b + (2 * 3)) + col);
                vect_dot_product((matx_a + (row * 3)), col_vect, matx_a_num_cols, matx_b_num_rows, ((matx_prod_out + (row * 3)) + col));
            }
        }
    }

    return ret;
}

int vect_magnitude(float* vect_a, UBYTE vect_a_size, float* mag_out) {
    int   ret = RET_OK;
    float mag = 0;
    int   i   = 0;

    if (NULL == vect_a || NULL == mag_out) {
        ret = RET_FAIL;
    } else if (vect_a_size <= 0) {
        ret = RET_FAIL;
    } else {
        for (i = 0; i < vect_a_size; i++) {
            mag += vect_a[i] * vect_a[i];
        }

        mag = sqrt(mag);
        *mag_out = mag;
    }

    return ret;
}

int vect_dot_product(float* vect_a, float* vect_b, UBYTE vect_a_size, UBYTE vect_b_size, float* dot_prod_out) {
    int   ret      = RET_OK;
    float dot_prod = 0;
    int   i        = 0;

    if (NULL == vect_a || NULL == vect_b || NULL == dot_prod_out) {
        ret = RET_FAIL;
    } else if (vect_a_size != vect_b_size) {
        ret = RET_FAIL;
    } else if (vect_a_size <= 0 || vect_b_size <= 0) {
        ret = RET_FAIL;
    } else {
        for (i = 0; i < vect_a_size; i++) {
            dot_prod += vect_a[i] * vect_b[i];
        }

        *dot_prod_out = dot_prod;
    }

    return ret;
}