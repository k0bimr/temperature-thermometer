#ifndef ONEWIRE_H
#define ONEWIRE_H
#include "SpiDispController.h"

extern UBYTE parasite_power;

void onewire_init(void);
int  onewire_get_measurement(float* measurement);

#endif