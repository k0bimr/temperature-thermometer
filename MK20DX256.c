#include "MK20DX256.h"

extern ULONG _etext;
extern ULONG _sdata;
extern ULONG _edata;
extern ULONG _sbss;
extern ULONG _ebss;
extern ULONG _estack;

typedef void (*VECT_HDL_F)(void);

void inf_loop_isr(void);

/* Vector Handlers */
void startup(void);
void nmi_handler(void)         __attribute__ ((weak, alias("inf_loop_isr")));
void hard_fault_handler(void)  __attribute__ ((weak, alias("inf_loop_isr")));
void mem_fault_handler(void)   __attribute__ ((weak, alias("inf_loop_isr")));
void bus_fault_handler(void)   __attribute__ ((weak, alias("inf_loop_isr")));
void usage_fault_handler(void) __attribute__ ((weak, alias("inf_loop_isr")));
void svcall_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));
void debugmonitor_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void pendablesrvreq_isr(void)  __attribute__ ((weak, alias("inf_loop_isr")));
void systick_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));

void dma_ch0_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));
void dma_ch1_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));
void dma_ch2_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));
void dma_ch3_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));
void dma_ch4_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));
void dma_ch5_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));
void dma_ch6_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));
void dma_ch7_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));
void dma_ch8_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));
void dma_ch9_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));
void dma_ch10_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));
void dma_ch11_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));
void dma_ch12_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));
void dma_ch13_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));
void dma_ch14_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));
void dma_ch15_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));
void dma_error_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void mcm_isr(void)		       __attribute__ ((weak, alias("inf_loop_isr")));
void flash_cmd_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void flash_error_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void low_voltage_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void wakeup_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));
void watchdog_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));
void i2c0_isr(void)		       __attribute__ ((weak, alias("inf_loop_isr")));
void i2c1_isr(void)		       __attribute__ ((weak, alias("inf_loop_isr")));
void i2c2_isr(void)		       __attribute__ ((weak, alias("inf_loop_isr")));
void spi0_isr(void)		       __attribute__ ((weak, alias("inf_loop_isr")));
void spi1_isr(void)		       __attribute__ ((weak, alias("inf_loop_isr")));
void spi2_isr(void)		       __attribute__ ((weak, alias("inf_loop_isr")));
void sdhc_isr(void)		       __attribute__ ((weak, alias("inf_loop_isr")));
void can0_message_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void can0_bus_off_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void can0_error_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void can0_tx_warn_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void can0_rx_warn_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void can0_wakeup_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void i2s0_tx_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));
void i2s0_rx_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));
void uart0_lon_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void uart0_status_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void uart0_error_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void uart1_status_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void uart1_error_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void uart2_status_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void uart2_error_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void uart3_status_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void uart3_error_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void uart4_status_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void uart4_error_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void uart5_status_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void uart5_error_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void adc0_isr(void)		       __attribute__ ((weak, alias("inf_loop_isr")));
void adc1_isr(void)		       /*__attribute__ ((weak, alias("inf_loop_isr")))*/;
void cmp0_isr(void)		       __attribute__ ((weak, alias("inf_loop_isr")));
void cmp1_isr(void)		       __attribute__ ((weak, alias("inf_loop_isr")));
void cmp2_isr(void)		       __attribute__ ((weak, alias("inf_loop_isr")));
void ftm0_isr(void)		       __attribute__ ((weak, alias("inf_loop_isr")));
void ftm1_isr(void)		       __attribute__ ((weak, alias("inf_loop_isr")));
void ftm2_isr(void)		       __attribute__ ((weak, alias("inf_loop_isr")));
void ftm3_isr(void)		       __attribute__ ((weak, alias("inf_loop_isr")));
void cmt_isr(void)		       __attribute__ ((weak, alias("inf_loop_isr")));
void rtc_alarm_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void rtc_seconds_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void pit0_isr(void)		       /*__attribute__ ((weak, alias("inf_loop_isr")))*/;
void pit1_isr(void)		       __attribute__ ((weak, alias("inf_loop_isr")));
void pit2_isr(void)		       __attribute__ ((weak, alias("inf_loop_isr")));
void pit3_isr(void)		       __attribute__ ((weak, alias("inf_loop_isr")));
void pdb_isr(void)		       __attribute__ ((weak, alias("inf_loop_isr")));
void usb_isr(void)		       __attribute__ ((weak, alias("inf_loop_isr")));
void usb_charge_isr(void)	   __attribute__ ((weak, alias("inf_loop_isr")));
void dac0_isr(void)		       __attribute__ ((weak, alias("inf_loop_isr")));
void dac1_isr(void)		       __attribute__ ((weak, alias("inf_loop_isr")));
void tsi0_isr(void)		       __attribute__ ((weak, alias("inf_loop_isr")));
void mcg_isr(void)		       __attribute__ ((weak, alias("inf_loop_isr")));
void lptmr_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));
void porta_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));
void portb_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));
void portc_isr(void)		   /*__attribute__ ((weak, alias("inf_loop_isr")))*/;
void portd_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));
void porte_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));
void software_isr(void)		   __attribute__ ((weak, alias("inf_loop_isr")));

extern void main(void);
extern void oled_turn_on(void);
extern void oled_turn_off(void);
extern volatile UBYTE oled_display_status;
extern PIOP_CONF switch_button_pin;
extern int iop_configure_interrupt(PIOP_CONF io_pin, IRQC int_type);

#define NVIC_ENABLE_PORTC_INTERRUPTS  (NVICISER2 |= (1 << 25))
#define NVIC_DISABLE_PORTC_INTERRUPTS (NVICISER2 &= ~(1 << 25))
#define NVIC_ENABLE_PIT0_INTERRUTPS   (NVICISER2 |= (1 << 4))
#define NVIC_ENABLE_ADC0_INTERRUPTS   (NVICISER1 |= (1 << 25))
#define NVIC_ENABLE_ADC1_INTERRUPTS   (NVICISER1 |= (1 << 26))

void portc_isr(void) {
    NVIC_DISABLE_PORTC_INTERRUPTS;
    // PORTC7 is switch pin (ard. 12)
    if (PORTC_ISFR & (1 << 7)) {
        iop_configure_interrupt(switch_button_pin, IRQC_INT_DMA_REQ_DISABLED);
        pit_set_timer(150, 0, 0);
        PORTC_ISFR |= (1 << 7);

        if (oled_display_status) {
            oled_turn_off();
        } else {
            oled_turn_on();
        }
    }
    NVIC_ENABLE_PORTC_INTERRUPTS;
}

void pit0_isr(void) {
    iop_configure_interrupt(switch_button_pin, IRQC_INT_FALLING_EDGE);
    PIT_TFLG0 |= PIT_TIMER_INTERRUPT_FLAG_CLEAR;
    PIT_TCTRL0 &= ~(1 << TEN);
}

void adc1_isr(void) {

}

/* FTFL_FOPT[LPBOOT] configured to be 1 ==> system clock is divided by 1
 *                                          bus clocked is divided by 1
 *                                          flash clock is divided by 2
 */
__attribute__ ((section(".flashconfig"), used))
const UBYTE flashconfig[] = {
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
    0xFF, 0xFF, 0xFF, 0xFF, 0xFE, 0xFF, 0xFF, 0xFF
};

__attribute__ ((section(".vectors"), used))
void (* const vectors[])(void) = {
    (void (*)(void))((ULONG)&_estack), // 0 - Initial Stack Pointer
    startup,                           // 1 - Initial Program Counter
    nmi_handler,                       // 2 - Non-maskable Interrup (NMI)
    hard_fault_handler,                // 3
    mem_fault_handler,                 // 4
    bus_fault_handler,                 // 5
    usage_fault_handler,               // 6
    inf_loop_isr,                      // 7
    inf_loop_isr,                      // 8
    inf_loop_isr,                      // 9
    inf_loop_isr,                      // 10
    svcall_isr,                        // 11 - SV Call
    debugmonitor_isr,                  // 12 - Debug Monitor
    inf_loop_isr,                      // 13 -
    inf_loop_isr,                      // 14 - PendableSrvReq
    inf_loop_isr,                      // 15 - SysTick
    dma_ch0_isr,					   // 16 DMA channel 0 transfer complete
	dma_ch1_isr,					   // 17 DMA channel 1 transfer complete
	dma_ch2_isr,					   // 18 DMA channel 2 transfer complete
	dma_ch3_isr,					   // 19 DMA channel 3 transfer complete
	dma_ch4_isr,					   // 20 DMA channel 4 transfer complete
	dma_ch5_isr,					   // 21 DMA channel 5 transfer complete
	dma_ch6_isr,					   // 22 DMA channel 6 transfer complete
	dma_ch7_isr,					   // 23 DMA channel 7 transfer complete
	dma_ch8_isr,					   // 24 DMA channel 8 transfer complete
	dma_ch9_isr,					   // 25 DMA channel 9 transfer complete
	dma_ch10_isr,					   // 26 DMA channel 10 transfer complete
	dma_ch11_isr,					   // 27 DMA channel 10 transfer complete
	dma_ch12_isr,					   // 28 DMA channel 10 transfer complete
	dma_ch13_isr,					   // 29 DMA channel 10 transfer complete
	dma_ch14_isr,					   // 30 DMA channel 10 transfer complete
	dma_ch15_isr,					   // 31 DMA channel 10 transfer complete
	dma_error_isr,					   // 32 DMA error interrupt channel
	inf_loop_isr,					   // 33 --
	flash_cmd_isr,					   // 34 Flash Memory Command complete
	flash_error_isr,				   // 35 Flash Read collision
	low_voltage_isr,				   // 36 Low-voltage detect/warning
	wakeup_isr,					       // 37 Low Leakage Wakeup
	watchdog_isr,					   // 38 Both EWM and WDOG interrupt
	inf_loop_isr,					   // 39 --
	i2c0_isr,					       // 40 I2C0
	i2c1_isr,					       // 41 I2C1
	spi0_isr,					       // 42 SPI0
	spi1_isr,					// 43 SPI1
	inf_loop_isr,					// 44 --
	can0_message_isr,				// 45 CAN OR'ed Message buffer (0-15)
	can0_bus_off_isr,				// 46 CAN Bus Off
	can0_error_isr,					// 47 CAN Error
	can0_tx_warn_isr,				// 48 CAN Transmit Warning
	can0_rx_warn_isr,				// 49 CAN Receive Warning
	can0_wakeup_isr,				// 50 CAN Wake Up
	i2s0_tx_isr,					// 51 I2S0 Transmit
	i2s0_rx_isr,					// 52 I2S0 Receive
	inf_loop_isr,					// 53 --
	inf_loop_isr,					// 54 --
	inf_loop_isr,					// 55 --
	inf_loop_isr,					// 56 --
	inf_loop_isr,					// 57 --
	inf_loop_isr,					// 58 --
	inf_loop_isr,					// 59 --
	uart0_lon_isr,					// 60 UART0 CEA709.1-B (LON) status
	uart0_status_isr,				// 61 UART0 status
	uart0_error_isr,				// 62 UART0 error
	uart1_status_isr,				// 63 UART1 status
	uart1_error_isr,				// 64 UART1 error
	uart2_status_isr,				// 65 UART2 status
	uart2_error_isr,				// 66 UART2 error
	inf_loop_isr,					// 67 --
	inf_loop_isr,					// 68 --
	inf_loop_isr,					// 69 --
	inf_loop_isr,					// 70 --
	inf_loop_isr,					// 71 --
	inf_loop_isr,					// 72 --
	adc0_isr,					// 73 ADC0
	adc1_isr,					// 74 ADC1
	cmp0_isr,					// 75 CMP0
	cmp1_isr,					// 76 CMP1
	cmp2_isr,					// 77 CMP2
	ftm0_isr,					// 78 FTM0
	ftm1_isr,					// 79 FTM1
	ftm2_isr,					// 80 FTM2
	cmt_isr,					// 81 CMT
	rtc_alarm_isr,					// 82 RTC Alarm interrupt
	rtc_seconds_isr,				// 83 RTC Seconds interrupt
	pit0_isr,					// 84 PIT Channel 0
	pit1_isr,					// 85 PIT Channel 1
	pit2_isr,					// 86 PIT Channel 2
	pit3_isr,					// 87 PIT Channel 3
	pdb_isr,					// 88 PDB Programmable Delay Block
	usb_isr,					// 89 USB OTG
	usb_charge_isr,					// 90 USB Charger Detect
	inf_loop_isr,					// 91 --
	inf_loop_isr,					// 92 --
	inf_loop_isr,					// 93 --
	inf_loop_isr,					// 94 --
	inf_loop_isr,					// 95 --
	inf_loop_isr,					// 96 --
	dac0_isr,					// 97 DAC0
	inf_loop_isr,					// 98 --
	tsi0_isr,					// 99 TSI0
	mcg_isr,					// 100 MCG
	lptmr_isr,					// 101 Low Power Timer
	inf_loop_isr,					// 102 --
	porta_isr,					// 103 Pin detect (Port A)
	portb_isr,					// 104 Pin detect (Port B)
	portc_isr,					// 105 Pin detect (Port C)
	portd_isr,					// 106 Pin detect (Port D)
	porte_isr,					// 107 Pin detect (Port E)
	inf_loop_isr,					// 108 --
	inf_loop_isr,					// 109 --
	software_isr					// 110 Software interrupt
};

void inf_loop_isr(void) {
    while (1) {
        SET_PIN(&GPIOC_PSOR, 5);
        //_delay_ms(1000);
        loop_delay(500);
        CLR_PIN(&GPIOC_PCOR, 5);
        //_delay_ms(1000);
        loop_delay(500);
        SET_PIN(&GPIOC_PSOR, 5);
        //_delay_ms(1000);
        loop_delay(500);
        CLR_PIN(&GPIOC_PCOR, 5);
        //_delay_ms(1000);
        loop_delay(500);
        loop_delay(500);
    }
}

__attribute__ ((section(".startup")))
void startup(void) {
    uint32_t* src  = &_etext;
    uint32_t* dest = &_sdata;

    UNLOCK_WDOG_REGS;
    DISABLE_WDOG_TIMER;

    while (dest < &_edata) {
        *dest++ = *src++;
    }

    dest = &_sbss;

    while (dest < &_ebss) {
        *dest++ = 0;
    }

#ifdef INCLUDE_GPIO_MODULE
    ENABLE_ALL_GPIO_CLOCKS;
#endif

#ifdef INCLUDE_ADC_MODULE
    ENABLE_ALL_ADC_CLOCKS;
#endif


    CONFIG_PIN_GPIO(&PORTC_PCR5);
    CONFIG_PIN_OUT(&GPIOC_PDDR, 5);
    
    mcg_switch_mode(FBE);
    mcg_switch_mode(PBE);
    mcg_switch_mode(PEE);

    // Enable NVIC interrupts for gpioc port
    NVIC_ENABLE_PORTC_INTERRUPTS;

    // Enable NVIC interrupts for PIT0 timer
    NVIC_ENABLE_PIT0_INTERRUTPS;

    // Enable NVIC interrupts for ADC1 module
    NVIC_ENABLE_ADC1_INTERRUPTS;

    main();

    while (1);
}