#ifndef SPI_DISP_CONTROLLER_H
#define SPI_DISP_CONTROLLER_H

#include <stdio.h>

typedef unsigned char      UBYTE;
#if defined(__MK20DX256__)
typedef volatile uint32_t* REG_T;
#else
typedef volatile uint8_t*  REG_T;
#endif

typedef uint32_t           REG_BITMASK;

typedef enum PIN_DATA_DIRECTION {
    PDD_OUT,
    PDD_IN
} PDD_T;

enum COMMON_RET_CODES {
    RET_OK   = 0,
    RET_FAIL = -1
};

#endif