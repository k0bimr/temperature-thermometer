#include "g_io_pin.h"

UBYTE iop_get_ard_pin(PIOP_CONF io_pin) {
    UBYTE ret = RET_OK;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        ret = io_pin->ard_pin;
    }

    return ret;
}

REG_T iop_get_pdd_reg(PIOP_CONF io_pin) {
    REG_T ret = RET_OK;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        ret = io_pin->pin_ctrl.pdd_reg;
    }

    return ret;
}

int iop_set_pdd_reg(PIOP_CONF io_pin, REG_T pdd_reg) {
    int ret = RET_OK;
    
    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        io_pin->pin_ctrl.pdd_reg = pdd_reg;
    }

    return ret;
}

PDD_T iop_get_pdd(PIOP_CONF io_pin) {
    PDD_T ret = RET_OK;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        ret = io_pin->pin_dd;
    }

    return ret;
}

int iop_set_pso_reg(PIOP_CONF io_pin, REG_T pso_reg) {
    int ret = RET_OK;
    
    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        io_pin->pin_ctrl.io_ctrl.out_ctrl.pso_reg = pso_reg;
    }

    return ret;
}

REG_T iop_get_pso_reg(PIOP_CONF io_pin) {
    REG_T ret = NULL;

    if (NULL != io_pin) {
        ret = io_pin->pin_ctrl.io_ctrl.out_ctrl.pso_reg;
    }

    return ret;
}

int iop_set_pco_reg(PIOP_CONF io_pin, REG_T pco_reg) {
    int ret = RET_OK;
    
    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        io_pin->pin_ctrl.io_ctrl.out_ctrl.pco_reg = pco_reg;
    }

    return ret;
}

REG_T iop_get_pco_reg(PIOP_CONF io_pin) {
    REG_T ret = NULL;

    if (NULL != io_pin) {
        ret = io_pin->pin_ctrl.io_ctrl.out_ctrl.pco_reg;
    }

    return ret;
}

int iop_set(PIOP_CONF io_pin) {
    int ret = RET_OK;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else if (iop_get_pdd(io_pin) != PDD_OUT || iop_get_pdd(io_pin) != PDD_BI_DIR) {
        ret = RET_FAIL;
    } else {
        *(iop_get_pso_reg(io_pin)) |= (1 << iop_get_prt_offset(io_pin));
    }

    return ret;
}

int iop_clr(PIOP_CONF io_pin) {
    int ret = RET_OK;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else if (iop_get_pdd(io_pin) != PDD_OUT) {
        ret = RET_FAIL;
    } else {
        *(iop_get_pco_reg(io_pin)) |= (1 << iop_get_prt_offset(io_pin));
    }

    return ret;
}

int iop_set_prt_offset(PIOP_CONF io_pin, UBYTE prt_offset) {
    int ret = RET_OK;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        io_pin->pin_ctrl.prt_offset = prt_offset;
    }

    return ret;
}

UBYTE iop_get_prt_offset(PIOP_CONF io_pin) {
    UBYTE ret = RET_OK;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        ret = io_pin->pin_ctrl.prt_offset;
    }

    return ret;
}

int iop_set_pri_reg(PIOP_CONF io_pin, REG_T pri_reg) {
    int ret = RET_OK;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        io_pin->pin_ctrl.io_ctrl.pri_reg = pri_reg;
    }

    return ret;
}

REG_T iop_get_pri_reg(PIOP_CONF io_pin) {
    REG_T ret = NULL;

    if (NULL != io_pin) {
        ret = io_pin->pin_ctrl.io_ctrl.pri_reg;
    }

    return ret;
}

UBYTE iop_read(PIOP_CONF io_pin) {
    int ret = RET_OK;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        ret = *(iop_get_pri_reg(io_pin));
    }

    return ret;
}

REG_T iop_get_pcr(PIOP_CONF io_pin) {
    REG_T ret = NULL;

    if (NULL != io_pin) {
        ret = io_pin->pin_ctrl.pc_reg;
    }

    return ret;
}

int iop_set_pcr(PIOP_CONF io_pin, REG_T pcr) {
    int ret = RET_OK;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        io_pin->pin_ctrl.pc_reg = pcr;
    }

    return ret;
}

int iop_set_pcr_bits(PIOP_CONF io_pin, REG_BITMASK pcr_bitmask) {
    int ret = RET_OK;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        *(iop_get_pcr(io_pin)) |= pcr_bitmask;
    }

    return ret;
}

int iop_clr_pcr_bits(PIOP_CONF io_pin, REG_BITMASK pcr_bitmask) {
    int ret = RET_OK;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        *(iop_get_pcr(io_pin)) &= ~pcr_bitmask;
    }

    return ret;
}
