#include "push_button.h"

int push_button_read_input(void) {
    int ret = RET_OK;

    ret = iop_read_in(switch_button_pin);

    return ret;
}