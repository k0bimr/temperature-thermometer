#ifndef SIM_H
#define SIM_H

#include "SpiDispController.h"
#include "util.h"

ULLONG sim_determine_core_sys_clk_freq(void);
ULLONG sim_determine_bus_clk_freq(void);
ULLONG sim_determine_flash_clk_freq(void);

#endif