#include "g_io_pin.h"

void io_pin_init(void) {
    int                   i;
    PIOP_CONF             p_conf = NULL;
    const PIN_REG_LOOKUP* p_regs = NULL;
    
    /* Configure all io pins as gpio and also configure their data direction */
    for (i = 0; i < NUM_IO_PINS; i++) {
        p_conf = &io_pins[i];
        p_regs = &pin_to_reg_map[iop_get_ard_pin(p_conf)];
        
        iop_set_pcr_reg(p_conf, p_regs->pc_reg);
        if (iop_get_ard_pin(p_conf) != ANALOG_DIAL_PIN) {
            CONFIG_PIN_GPIO(p_conf->pin_ctrl.pc_reg);
        }
        
        iop_set_pdd_reg(p_conf, p_regs->pdd_reg);
        iop_set_prt_offset(p_conf, p_regs->prt_offset);
        iop_set_pso_reg(p_conf, p_regs->pso_reg);
        iop_set_pco_reg(p_conf, p_regs->pco_reg);
        iop_set_pri_reg(p_conf, p_regs->pri_reg);
        iop_set_pro_reg(p_conf, p_regs->pro_reg);

        if (iop_get_ard_pin(p_conf) != ANALOG_DIAL_PIN) { 
            if (PDD_OUT == p_conf->pin_dd) {
                iop_set_output_mode(p_conf);
            } else if (PDD_IN == p_conf->pin_dd || PDD_BI_DIR == p_conf->pin_dd) {
                iop_set_input_mode(p_conf);
            }
        } else {
            // setting as analog mode (PINMUX)
            *(iop_get_pcr_reg(p_conf)) &= ~((1 << 10) | (1 << 9) | (1 << 8));
        }
        

        /* Set SPI pins */
        if (SPI_CLK_PIN == p_conf->ard_pin) {
            spi_pins.clk_pin = p_conf;
        } else if (SPI_DIN_PIN == p_conf->ard_pin) {
            spi_pins.din_pin = p_conf;
        } else if (SPI_CS_PIN == p_conf->ard_pin) {
            spi_pins.cs_pin = p_conf;
        } else if (SPI_DC_PIN == p_conf->ard_pin) {
            spi_pins.dc_pin = p_conf;
        } else if (SPI_RST_PIN == p_conf->ard_pin) {
            spi_pins.rst_pin = p_conf;
        }

        /* Set 1-Wire pin */
        if (DS18B20_DQ_PIN == p_conf->ard_pin) {
            ds18b20_dq_pin = p_conf;
        }

        /* Set analog dial pin */
        if (ANALOG_DIAL_PIN == p_conf->ard_pin) {
            analog_dial_pin = p_conf;
        }

        /* Set switch push button pin */
        if (SWITCH_BUTTON_PIN == p_conf->ard_pin) {
            switch_button_pin = p_conf;
            iop_configure_interrupt(switch_button_pin, IRQC_INT_FALLING_EDGE);
        }
    }
}

UBYTE iop_get_ard_pin(PIOP_CONF io_pin) {
    UBYTE ret = RET_OK;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        ret = io_pin->ard_pin;
    }

    return ret;
}

REG_UL_T iop_get_pdd_reg(PIOP_CONF io_pin) {
    REG_UL_T ret = RET_OK;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        ret = io_pin->pin_ctrl.pdd_reg;
    }

    return ret;
}

int iop_set_pdd_reg(PIOP_CONF io_pin, REG_UL_T pdd_reg) {
    int ret = RET_OK;
    
    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        io_pin->pin_ctrl.pdd_reg = pdd_reg;
    }

    return ret;
}

PDD_T iop_get_pdd(PIOP_CONF io_pin) {
    PDD_T ret = RET_OK;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        ret = io_pin->pin_dd;
    }

    return ret;
}

int iop_set_pso_reg(PIOP_CONF io_pin, REG_UL_T pso_reg) {
    int ret = RET_OK;
    
    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        io_pin->pin_ctrl.io_ctrl.out_ctrl.pso_reg = pso_reg;
    }

    return ret;
}

REG_UL_T iop_get_pso_reg(PIOP_CONF io_pin) {
    REG_UL_T ret = NULL;

    if (NULL != io_pin) {
        ret = io_pin->pin_ctrl.io_ctrl.out_ctrl.pso_reg;
    }

    return ret;
}

int iop_set_pco_reg(PIOP_CONF io_pin, REG_UL_T pco_reg) {
    int ret = RET_OK;
    
    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        io_pin->pin_ctrl.io_ctrl.out_ctrl.pco_reg = pco_reg;
    }

    return ret;
}

REG_UL_T iop_get_pco_reg(PIOP_CONF io_pin) {
    REG_UL_T ret = NULL;

    if (NULL != io_pin) {
        ret = io_pin->pin_ctrl.io_ctrl.out_ctrl.pco_reg;
    }

    return ret;
}

int iop_set(PIOP_CONF io_pin) {
    int      ret        = RET_OK;
    REG_UL_T psor_reg   = NULL;
    UBYTE    pin_offset = 0;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } /*else if ((iop_get_pdd(io_pin) != PDD_OUT) && (iop_get_pdd(io_pin) != PDD_BI_DIR)) {
        ret = RET_FAIL;
    }*/ else if ((psor_reg = iop_get_pso_reg(io_pin)) == NULL) {
        ret = RET_FAIL;
    } else if ((pin_offset = iop_get_prt_offset(io_pin)) < RET_OK) {
        ret = RET_FAIL;
    } else {
        *(psor_reg) |= (1 << pin_offset);
    }

    return ret;
}

int iop_clr(PIOP_CONF io_pin) {
    int ret = RET_OK;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } /*else if (iop_get_pdd(io_pin) != PDD_OUT) {
        ret = RET_FAIL;
    }*/ else {
        *(iop_get_pco_reg(io_pin)) |= (1 << iop_get_prt_offset(io_pin));
    }

    return ret;
}

#ifdef INCLUDE_ADC_MODULE
int iop_analog_read(PIOP_CONF io_pin, int* analog_read) {
    int   ret          = RET_OK;
    int   in_ch      = 0;
    UBYTE ard_pin    = 0;
    int   analog_ret = 0;

    if (NULL == io_pin || NULL == analog_read) {
        ret = RET_INVALID_PARAM;
    } else if ((int)(ard_pin = iop_get_ard_pin(io_pin)) < RET_OK) {
        ret = (int)ard_pin;
    } else if (ard_pin < 0 || ard_pin >= ARD_PIN_TO_ADC_CH_MAP_SIZE) {
        ret = RET_INVALID_PARAM; // TODO: better error code
    } else {
        in_ch = ard_pin_to_adc_ch_map[ard_pin];

        ret = adc_perform_conversion(in_ch, &analog_ret);
        if (RET_OK == ret) {
            *analog_read = analog_ret;
        }
    }

    return ret;
}

int iop_analog_write(PIOP_CONF io_pin, int analog_write) {
    int ret = RET_OK;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {

    }

    return ret;
}
#endif

int iop_set_prt_offset(PIOP_CONF io_pin, UBYTE prt_offset) {
    int ret = RET_OK;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        io_pin->pin_ctrl.prt_offset = prt_offset;
    }

    return ret;
}

UBYTE iop_get_prt_offset(PIOP_CONF io_pin) {
    UBYTE ret = RET_OK;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        ret = io_pin->pin_ctrl.prt_offset;
    }

    return ret;
}

int iop_set_pri_reg(PIOP_CONF io_pin, REG_UL_T pri_reg) {
    int ret = RET_OK;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        io_pin->pin_ctrl.io_ctrl.pri_reg = pri_reg;
    }

    return ret;
}

REG_UL_T iop_get_pri_reg(PIOP_CONF io_pin) {
    REG_UL_T ret = NULL;

    if (NULL != io_pin) {
        ret = io_pin->pin_ctrl.io_ctrl.pri_reg;
    }

    return ret;
}

int iop_set_pro_reg(PIOP_CONF io_pin, REG_UL_T pro_reg) {
    int ret = RET_OK;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        io_pin->pin_ctrl.io_ctrl.pro_reg = pro_reg;
    }

    return ret;
}

REG_UL_T iop_get_pro_reg(PIOP_CONF io_pin) {
    REG_UL_T ret = NULL;

    if (NULL != io_pin) {
        ret = io_pin->pin_ctrl.io_ctrl.pro_reg;
    }

    return ret;
}

UBYTE iop_read_in(PIOP_CONF io_pin) {
    int ret = RET_OK;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        ret = (
            (*(iop_get_pri_reg(io_pin)) & (1 << iop_get_prt_offset(io_pin))) >> iop_get_prt_offset(io_pin)
        );
    }

    return ret;
}

UBYTE iop_read_out(PIOP_CONF io_pin) {
    int ret = RET_OK;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        ret = (
            (*(iop_get_pro_reg(io_pin)) & (1 << iop_get_prt_offset(io_pin))) >> iop_get_prt_offset(io_pin)
        );
    }

    return ret;
}

REG_UL_T iop_get_pcr_reg(PIOP_CONF io_pin) {
    REG_UL_T ret = NULL;

    if (NULL != io_pin) {
        ret = io_pin->pin_ctrl.pc_reg;
    }

    return ret;
}

int iop_set_pcr_reg(PIOP_CONF io_pin, REG_UL_T pcr) {
    int ret = RET_OK;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        io_pin->pin_ctrl.pc_reg = pcr;
    }

    return ret;
}

int iop_set_pcr_bits(PIOP_CONF io_pin, REG_BITMASK pcr_bitmask) {
    int ret = RET_OK;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        *(iop_get_pcr_reg(io_pin)) |= pcr_bitmask;
    }

    return ret;
}

int iop_clr_pcr_bits(PIOP_CONF io_pin, REG_BITMASK pcr_bitmask) {
    int ret = RET_OK;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else {
        *(iop_get_pcr_reg(io_pin)) &= ~pcr_bitmask;
    }

    return ret;
}

int iop_set_input_mode(PIOP_CONF io_pin) {
    int      ret        = RET_OK;
    REG_UL_T pdd_reg    = NULL;
    UBYTE    pin_offset = 0;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else if (NULL == (pdd_reg = iop_get_pdd_reg(io_pin))) {
        ret = RET_FAIL;
    } else if ((pin_offset = iop_get_prt_offset(io_pin)) < RET_OK) {
        ret = RET_FAIL;
    } else {
        (*pdd_reg) &= ~(1 << pin_offset);
    }

    return ret;
}

int iop_set_output_mode(PIOP_CONF io_pin) {
    int      ret        = RET_OK;
    REG_UL_T pdd_reg    = NULL;
    UBYTE    pin_offset = 0;

    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else if (NULL == (pdd_reg = iop_get_pdd_reg(io_pin))) {
        ret = RET_FAIL;
    } else if ((pin_offset = iop_get_prt_offset(io_pin)) < RET_OK) {
        ret = RET_FAIL;
    } else {
        (*pdd_reg) |= (1 << pin_offset);
    }

    return ret;
}

int iop_configure_interrupt(PIOP_CONF io_pin, IRQC int_type) {
    int      ret     = RET_OK;
    REG_UL_T pcr_reg = NULL;


    if (NULL == io_pin) {
        ret = RET_FAIL;
    } else if (NULL == (pcr_reg = iop_get_pcr_reg(io_pin))) {
        ret = RET_FAIL;
    } else {
        (*pcr_reg) = ((*pcr_reg) & ~PORTX_PCRN_IRQC_BITMASK) | ((int_type << PORTX_PCRN_IRQC_SHIFT) & PORTX_PCRN_IRQC_BITMASK);
    }

    return ret;
}