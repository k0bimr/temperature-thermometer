#ifndef G_IO_PIN_H_
#define G_IO_PIN_H_

#include "SpiDispController.h"

#if 0
#define PIN1 1
#define PIN2 2
#define PIN3 3
#define PIN4 4
#define PIN5 5
#define PIN6 6
#define PIN7 7
#endif

typedef struct G_IO_PIN_CONFIG {
    UBYTE ard_pin;
    PDD_T pin_dd;
    struct {
        REG_T pc_reg;
        REG_T pdd_reg;
        union {
            struct {
                REG_T pso_reg;
                REG_T pco_reg;
            } out_ctrl;
            REG_T pri_reg;
        } io_ctrl;
        UBYTE prt_offset; // port offset (pin) - not to be confused with arduino pin number
    } pin_ctrl;
} IOP_CONF, *PIOP_CONF;

UBYTE iop_get_ard_pin(PIOP_CONF io_pin);

REG_T iop_get_pdd_reg(PIOP_CONF io_pin);

int iop_set_pdd_reg(PIOP_CONF io_pin, REG_T pdd_reg);

PDD_T iop_get_pdd(PIOP_CONF io_pin);

int iop_set_pso_reg(PIOP_CONF io_pin, REG_T pso_reg);

REG_T iop_get_pso_reg(PIOP_CONF io_pin);

int iop_set_pco_reg(PIOP_CONF io_pin, REG_T pco_reg);

REG_T iop_get_pco_reg(PIOP_CONF io_pin);

int iop_set(PIOP_CONF io_pin);

int iop_clr(PIOP_CONF io_pin);

int iop_set_prt_offset(PIOP_CONF io_pin, UBYTE prt_offset);

UBYTE iop_get_prt_offset(PIOP_CONF io_pin);

int iop_set_pri_reg(PIOP_CONF io_pin, REG_T pri_reg);

REG_T iop_get_pri_reg(PIOP_CONF io_pin);

UBYTE iop_read(PIOP_CONF io_pin);

REG_T iop_get_pcr(PIOP_CONF io_pin);

int iop_set_pcr(PIOP_CONF io_pin, REG_T pcr);

int iop_set_pcr_bits(PIOP_CONF io_pin, REG_BITMASK pcr_bitmask);

int iop_clr_pcr_bits(PIOP_CONF io_pin, REG_BITMASK pcr_bitmask);

#endif
