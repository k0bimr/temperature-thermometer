#include "SpiDispController.h"
#include "g_io_pin.h"

/*============== Defines ==============*/
#define INFINITE 1

#define SPI_CLK_PIN 21 /* --> PT D6 */
#define SPI_DIN_PIN 20 /* --> PT D5 */
#define SPI_CS_PIN  19 /* --> PT B2 */
#define SPI_DC_PIN  18 /* --> PT B3 */
#define SPI_RST_PIN 17 /* --> PT B1 */

#define NUM_IO_PINS  5
#define NUM_SPI_PINS 5

#define PORTX_PCRN_MUX_1    8
#define PORTX_PCRN_MUX_2    9
#define PORTX_PCRN_MUX_3    10
#define PORTX_PCRN_MUX_GPIO (1 << PORTX_PCRN_MUX_1)

#define DEREF_REG(reg_addr) *((REG_T) reg_addr)

//#define PORTB_PCR1 (*(volatile REG_T *)0x4004A004)
#define PORTB_PCR1 (DEREF_REG(0x4004A004))
#define PORTB_PCR2 (DEREF_REG(0x4004A008))
#define PORTB_PCR3 (DEREF_REG(0x4004A00C))
#define GPIOB_PDDR (DEREF_REG(0x400FF054))
#define GPIOB_PSOR (DEREF_REG(0x400FF044))
#define GPIOB_PCOR (DEREF_REG(0x400FF048))
#define GPIOB_PDIR (DEREF_REG(0x400FF050))

#define PORTC_PCR5 (DEREF_REG(0x4004B014))
#define GPIOC_PDDR (DEREF_REG(0x400FF094))
#define GPIOC_PSOR (DEREF_REG(0x400FF084))
#define GPIOC_PCOR (DEREF_REG(0x400FF088))
#define GPIOC_PDIR (DEREF_REG(0x400FF090))

#define PORTD_PCR5 (DEREF_REG(0x4004C014))
#define PORTD_PCR6 (DEREF_REG(0x4004C018))
#define GPIOD_PDDR (DEREF_REG(0x400FF0D4))
#define GPIOD_PSOR (DEREF_REG(0x400FF0C4))
#define GPIOD_PCOR (DEREF_REG(0x400FF0C8))
#define GPIOD_PDIR (DEREF_REG(0x400FF0D0))

/* PIT Memory Map */
#define PIT_MCR    (DEREF_REG(0x40037000)) /* PIT Module Control Register */
#define PIT_LDVAL0 (DEREF_REG(0x40037100)) /* Timer 1 Load Value Register */
#define PIT_CVAL0  (DEREF_REG(0x40037104))
#define PIT_TCTRL0 (DEREF_REG(0x40037108))
#define PIT_TFLG0  (DEREF_REG(0x4003710C))
#define PIT_LDVAL1 (DEREF_REG(0x40037110))
#define PIT_CVAL1  (DEREF_REG(0x40037114))
#define PIT_TCTRL1 (DEREF_REG(0x40037118))
#define PIT_TFLG1  (DEREF_REG(0x4003711C))
#define PIT_LDVAL2 (DEREF_REG(0x40037120))
#define PIT_CVAL2  (DEREF_REG(0x40037124))
#define PIT_TCTRL2 (DEREF_REG(0x40037128))
#define PIT_TFLG2  (DEREF_REG(0x4003712C))
#define PIT_LDVAL3 (DEREF_REG(0x40037130))
#define PIT_CVAL3  (DEREF_REG(0x40037134))
#define PIT_TCTRL3 (DEREF_REG(0x40037138))
#define PIT_TFLG3  (DEREF_REG(0x4003713C))

/* PIT_MCR Bits */
#define MDIS       1
#define FRZ        0

/* PIT_TCTRLn Bits */
#define CHN        2
#define TIE        1
#define TEN        0

/* PIT_TFLGn Bits */
#define TIF        0

#define CONFIG_PIN_GPIO(reg) ((*reg) |= (PORTX_PCRN_MUX_GPIO))
#define CONFIG_PIN_OUT(reg, pin) ((*reg) |= (1 << pin))
#define CONFIG_PIN_IN(reg, pin) ((*reg) &= ~(1 << pin))
#define REG_SET_BIT(reg, bit_n) ((*reg) |= (1 << bit_n))
#define SET_PIN(reg, pin) (REG_SET_BIT(reg, pin))
#define CLR_PIN(reg, pin) (REG_SET_BIT(reg, pin))

/*============== Typedefs ==============*/
typedef enum SPI_DataCommandType {
    DC_COMMAND,
    DC_DATA
} SPI_DC_T;

typedef struct SPI_CONTROLLER_PINS {
    IOP_CONF* clk_pin;
    IOP_CONF* din_pin;
    IOP_CONF* cs_pin;
    IOP_CONF* dc_pin;
    IOP_CONF* rst_pin;
} SPI_CTRL;

typedef struct ARD_PIN_MAP_LOOKUP {
    REG_T pc_reg;
    REG_T pdd_reg;
    REG_T pso_reg;
    REG_T pco_reg;
    REG_T pri_reg;
    UBYTE prt_offset;  // port number - not to be confused with arduino pin number
} PIN_REG_LOOKUP;

typedef enum SPI_COMMANDS {
    SET_COL_ADDR                           = 0x15,
    SET_ROW_ADDR                           = 0x75,
    SET_CONTRAST_CTRL                      = 0x81,
    NOP_1                                  = 0x84,
    SET_REMAP                              = 0xA0,
    SET_DISP_START_LINE                    = 0xA1,
    SET_DISP_OFFSET                        = 0xA2,
    SET_MULTIPLIER_RATIO                   = 0xA8,
    FUNCT_SELECT_A                         = 0xAB,
    SET_DISP_OFF                           = 0xAE,
    SET_DISP_ON                            = 0xAF,
    SET_PHASE_LEN                          = 0xB1,
    NOP_2                                  = 0xB2,
    SET_FRONT_CLK_DIV                      = 0xB3,
    SET_GPIO                               = 0xB5,
    SET_SECOND_PRE_CHRGE_PERIOD            = 0xB6,
    SET_GRAY_SCALE_TABLE                   = 0xB8,
    SELECT_DEFAULT_LINEAR_GRAY_SCALE_TABLE = 0xB9,
    NOP_3                                  = 0xBB,
    SET_PRE_CHARGE_VOLT                    = 0xBC,
    SET_V_COMH_VOLT                        = 0xBE,
    FUNCT_SELECT_B                         = 0xD5,
    SET_CMD_LOCK                           = 0xFD
} SPI_CMD;

typedef enum SPI_GRAPHIC_ACCEL_CMDS {
    HORIZONTAL_SCROLL_SETUP_1 = 0x26,
    HORIZONTAL_SCROLL_SETUP_2 = 0x27,
    DEACTIVATE_SCROLL         = 0x2E,
    ACTIVATE_SCROLL           = 0x2F
} SPI_GRAPH_ACCEL_CMD;

typedef enum SPI_SET_DISP_MODE_CMDS {
    NORMAL_DISP         = 0xA4,
    SET_ENTIRE_DISP_ON  = 0xA5,
    SET_ENTIRE_DISP_OFF = 0xA6,
    INVERSE_DISP        = 0xA7
} DISP_MODE_SUB_CMD;

/*============== Enums ==============*/


/*============== Globals ==============*/
SPI_CTRL spi_pins = { 
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
};

IOP_CONF io_pins[] = {
    {17, PDD_OUT, {0}},
    {18, PDD_OUT, {NULL}},
    {19, PDD_OUT, {NULL}},
    {20, PDD_OUT, {NULL}},
    {21, PDD_OUT, {NULL}}/*,
    {22, PDD_OUT, {0}}*/
};

const PIN_REG_LOOKUP pin_to_reg_map[] = {
    {0}, {0}, {0}, {0}, {0}, {0}, 
    {0}, {0}, {0}, {0}, {0}, {0}, 
    {0}, {0}, {0}, {0}, {0}, 
    {&PORTB_PCR1, &GPIOB_PDDR, &GPIOB_PSOR, &GPIOB_PCOR, &GPIOB_PDIR, 1}, // Ard. 17
    {&PORTB_PCR3, &GPIOB_PDDR, &GPIOB_PSOR, &GPIOB_PCOR, &GPIOB_PDIR, 3}, // Ard. 18
    {&PORTB_PCR2, &GPIOB_PDDR, &GPIOB_PSOR, &GPIOB_PCOR, &GPIOB_PDIR, 2}, // Ard. 19
    {&PORTD_PCR5, &GPIOD_PDDR, &GPIOD_PSOR, &GPIOD_PCOR, &GPIOD_PDIR, 5}, // Ard. 20
    {&PORTD_PCR6, &GPIOD_PDDR, &GPIOD_PSOR, &GPIOD_PCOR, &GPIOD_PDIR, 6}  // Ard. 21
};

/*============== Prototypes ==============*/
void spi_init(void);
void spi_power_on_seq(void);
void spi_send_data(SPI_DC_T data_type, UBYTE data);
void io_pin_init(void);
void timers_init(void);
void _delay_us(unsigned long delayus);
void _delay_ms(unsigned long delayms);
void _timer_delay(unsigned long cycles);

int main(void) {
    int               ret               = RET_OK;
    SPI_CMD           spi_cmd           = SET_DISP_ON;
    DISP_MODE_SUB_CMD spi_disp_mode_cmd = SET_ENTIRE_DISP_ON;

    //timers_init();

    CONFIG_PIN_GPIO(&PORTC_PCR5);
    CONFIG_PIN_OUT(&GPIOC_PDDR, 5);

    while (1) {
        SET_PIN(&GPIOC_PSOR, 5);
        //_delay_ms(1000);
        //CLR_PIN(&GPIOC_PCOR, 5);
        //_delay_ms(1000);
    }
#if 0
    
    io_pin_init();
    //spi_power_on_seq();
    spi_init();

    spi_cmd = SET_DISP_ON;
    spi_send_data(DC_COMMAND, spi_cmd);
    
    while (INFINITE) {
        spi_disp_mode_cmd = SET_ENTIRE_DISP_ON;
        spi_send_data(DC_COMMAND, spi_disp_mode_cmd);
        _delay_ms(1000);
        spi_disp_mode_cmd = SET_ENTIRE_DISP_OFF;
        spi_send_data(DC_COMMAND, spi_disp_mode_cmd);
        _delay_ms(1000);
    }
#endif
    return ret;
}

#define IVDDREG 0 // offset for Vdd Internal Regulator control
#define MCULOCK 2 // offset for MCU interface lock
#define SET_CMD_LOCK_DATA ((1 << 4) | (1 << 1)) // hardcoded sequence for data for set cmd lock command
void spi_init(void) {
    spi_power_on_seq();

    /* Bring CLK LOW */
    iop_clr(spi_pins.clk_pin);
    //CLR_PIN(spi_pins.clk_pin->pin_ctrl.io_ctrl.out_ctrl.pco_reg, spi_pins.clk_pin->pin_ctrl.prt_offset);

    /* Make sure the display is off */
    spi_send_data(DC_COMMAND, SET_DISP_OFF);

    /* Enable Internal Vdd Regulator */
    spi_send_data(DC_COMMAND, FUNCT_SELECT_A);
    spi_send_data(DC_DATA, (1 << IVDDREG));

    /* Unlock the MCU interface so that it can accept our SPI commands */
    spi_send_data(DC_COMMAND, SET_CMD_LOCK);
    spi_send_data(DC_DATA, (SET_CMD_LOCK_DATA & ~(1 << MCULOCK)));
}

void spi_power_on_seq(void) {
    /* Pull RST pin High to do normal operations */
    iop_set(spi_pins.rst_pin);
    //SET_PIN(spi_pins.rst_pin->pin_ctrl.io_ctrl.out_ctrl.pso_reg, spi_pins.rst_pin->pin_ctrl.prt_offset);

    _delay_ms(1);

    /* Pull RST pin low to do initialiation hardware */
    iop_clr(spi_pins.rst_pin);
    //CLR_PIN(spi_pins.rst_pin->pin_ctrl.io_ctrl.out_ctrl.pco_reg, spi_pins.rst_pin->pin_ctrl.prt_offset);

    _delay_us(100);

    /* Pull RST pin High to do normal operations */
    iop_set(spi_pins.rst_pin);
    //SET_PIN(spi_pins.rst_pin->pin_ctrl.io_ctrl.out_ctrl.pso_reg, spi_pins.rst_pin->pin_ctrl.prt_offset);

    _delay_us(100);

}

void spi_send_data(SPI_DC_T data_type, UBYTE data) {
    int i;

    /* Bring DC (Data-Command) to proper level */
    if (DC_COMMAND == data_type) {
        iop_clr(spi_pins.dc_pin);
        //CLR_PIN(spi_pins.dc_pin->pin_ctrl.io_ctrl.out_ctrl.pco_reg, spi_pins.dc_pin->pin_ctrl.prt_offset);
    } else if (DC_DATA == data_type) {
        iop_set(spi_pins.dc_pin);
        //SET_PIN(spi_pins.dc_pin->pin_ctrl.io_ctrl.out_ctrl.pso_reg, spi_pins.dc_pin->pin_ctrl.prt_offset);
    }

    /* Set CLK LOW */
    iop_clr(spi_pins.clk_pin);
    //CLR_PIN(spi_pins.clk_pin->pin_ctrl.io_ctrl.out_ctrl.pco_reg, spi_pins.clk_pin->pin_ctrl.prt_offset);

    /* Set CS LOW */
    iop_clr(spi_pins.cs_pin);
    //CLR_PIN(spi_pins.cs_pin->pin_ctrl.io_ctrl.out_ctrl.pco_reg, spi_pins.cs_pin->pin_ctrl.prt_offset);

    /* Set DIN LOW */
    iop_clr(spi_pins.din_pin);
    //CLR_PIN(spi_pins.din_pin->pin_ctrl.io_ctrl.out_ctrl.pco_reg, spi_pins.din_pin->pin_ctrl.prt_offset);

    /* Sending MSB first */
    for (i = 7; i >= 0; i--) {
        /* Load bit of data into DIN pin register */
        if ((data) & (1 << i)) {
            iop_set(spi_pins.din_pin);
            //SET_PIN(spi_pins.din_pin->pin_ctrl.io_ctrl.out_ctrl.pso_reg, spi_pins.din_pin->pin_ctrl.prt_offset);
        } else {
            iop_clr(spi_pins.din_pin);
            //CLR_PIN(spi_pins.din_pin->pin_ctrl.io_ctrl.out_ctrl.pco_reg, spi_pins.din_pin->pin_ctrl.prt_offset);
        }

        /* Set CLK HIGH to send bit */
        iop_set(spi_pins.clk_pin);
        //SET_PIN(spi_pins.clk_pin->pin_ctrl.io_ctrl.out_ctrl.pso_reg, spi_pins.clk_pin->pin_ctrl.prt_offset);

        /* Set CLK LOW to stop slave from reading */
        iop_clr(spi_pins.clk_pin);
        //CLR_PIN(spi_pins.clk_pin->pin_ctrl.io_ctrl.out_ctrl.pco_reg, spi_pins.clk_pin->pin_ctrl.prt_offset);
    }

    /* Pull CS HIGH */
    iop_set(spi_pins.cs_pin);
    //SET_PIN(spi_pins.cs_pin->pin_ctrl.io_ctrl.out_ctrl.pso_reg, spi_pins.cs_pin->pin_ctrl.prt_offset);
}

void io_pin_init(void) {
    int                   i;
    IOP_CONF*             p_conf = NULL;
    const PIN_REG_LOOKUP* p_regs = NULL;
    
    /* Configure all io pins as gpio and also configure their data direction */
    for (i = 0; i < NUM_IO_PINS; i++) {
        p_conf = &io_pins[i];
        p_regs = &pin_to_reg_map[iop_get_ard_pin(p_conf)];
        
        iop_set_pcr(p_conf, p_regs->pc_reg);
        //p_conf->pin_ctrl.pc_reg              = p_regs->pc_reg;
        CONFIG_PIN_GPIO(p_conf->pin_ctrl.pc_reg);
        iop_set_pdd_reg(p_conf, p_regs->pdd_reg);
        //p_conf->pin_ctrl.pdd_reg             = p_regs->pdd_reg;
        iop_set_prt_offset(p_conf, p_regs->prt_offset);
        //p_conf->pin_ctrl.prt_offset          = p_regs->prt_offset;
        if (PDD_OUT == p_conf->pin_dd) {
            iop_set_pso_reg(p_conf, p_regs->pso_reg);
            //p_conf->pin_ctrl.io_ctrl.out_ctrl.pso_reg = p_regs->pso_reg;
            iop_set_pco_reg(p_conf, p_regs->pco_reg);
            //p_conf->pin_ctrl.io_ctrl.out_ctrl.pco_reg = p_regs->pco_reg;
            CONFIG_PIN_OUT(p_conf->pin_ctrl.pdd_reg, p_conf->pin_ctrl.prt_offset);
        } else if (PDD_IN == p_conf->pin_dd) {
            iop_set_pri_reg(p_conf, p_regs->pri_reg);
            // p_conf->pin_ctrl.io_ctrl.pri_reg = p_regs->pri_reg;
            CONFIG_PIN_IN(p_conf->pin_ctrl.pdd_reg, p_conf->pin_ctrl.prt_offset);
        }

        /* Set SPI pins */
        if (SPI_CLK_PIN == p_conf->ard_pin) {
            spi_pins.clk_pin = p_conf;
        } else if (SPI_DIN_PIN == p_conf->ard_pin) {
            spi_pins.din_pin = p_conf;
        } else if (SPI_CS_PIN == p_conf->ard_pin) {
            spi_pins.cs_pin = p_conf;
        } else if (SPI_DC_PIN == p_conf->ard_pin) {
            spi_pins.dc_pin = p_conf;
        } else if (SPI_RST_PIN == p_conf->ard_pin) {
            spi_pins.rst_pin = p_conf;
        }
    }
}

#define PIT_FREQ   50000000
#define CLK_PER_NS 20
#define MS_TO_NS   1000000
void timers_init(void) {
    /* Activate PIT Module */
    PIT_MCR &= ~(1 << MDIS);
}

void _delay_us(unsigned long delayus) {
    unsigned long numCycles;

    numCycles = delayus / CLK_PER_NS;
    _timer_delay(numCycles);
}

void _delay_ms(unsigned long delayms) {
    unsigned long numCycles;

    numCycles = (delayms * MS_TO_NS) / CLK_PER_NS;
    _timer_delay(numCycles);
}

void _timer_delay(unsigned long cycles) {
    PIT_LDVAL1 = cycles;
    PIT_TCTRL1 |= (1 << TEN);

    while (!(PIT_TFLG1 & (1 << TIF)));
}
