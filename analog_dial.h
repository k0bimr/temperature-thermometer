#ifndef ANALOG_DIAL_H
#define ANALOG_DIAL_H

#include "g_io_pin.h"

#define ANALOG_DIAL_PIN 14
extern PIOP_CONF analog_dial_pin;

int dial_init(void);
int read_dial_reading(void);

#endif