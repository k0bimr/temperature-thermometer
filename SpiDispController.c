#include "SpiDispController.h"
#include "g_io_pin.h"
#include "DS18B20.h"
#include "util.h"
#include "oled_display.h"
#include "MK20DX256.h"
#include "arm_math.h"
#include <float.h>
#include <math.h>

#ifdef INCLUDE_ADC_MODULE
#include "ADC.h"
#endif

#ifdef INCLUDE_PUSH_BUTTON
#include "push_button.h"
#endif

/*============== Defines ==============*/
#ifndef F_CPU
    #define F_CPU 72000000
#endif

#define BUS_FREQ     F_CPU

#define INFINITE     1

#undef  DISPLAY_MCGOUTCLK_FREQ
#undef  CHECK_NS_PER_CLK_PIT

/*============== Typedefs ==============*/

/*============== Enums ==============*/

/*============== Prototypes ==============*/

/*============== Globals ==============*/
IOP_CONF io_pins[] = {
    {12, PDD_IN, {0}},
    {14, PDD_IN, {0}},
    {17, PDD_OUT, {0}},
    {18, PDD_OUT, {NULL}},
    {19, PDD_OUT, {NULL}},
    {20, PDD_OUT, {NULL}},
    {21, PDD_OUT, {NULL}},
    {22, PDD_BI_DIR, {NULL}}
};

const PIN_REG_LOOKUP pin_to_reg_map[] = {
    {0}, {0}, {0}, {0}, {0}, {0}, 
    {0}, {0}, {0}, {0}, {0}, {0}, 
    {&PORTC_PCR7, &GPIOC_PDDR, &GPIOC_PSOR, &GPIOC_PCOR, &GPIOC_PDIR, &GPIOC_PDOR, 7}, // Ard. 12
    {0}, 
    {&PORTD_PCR1, &GPIOD_PDDR, &GPIOD_PSOR, &GPIOD_PCOR, &GPIOD_PDIR, &GPIOD_PDOR, 1}, // Ard. 14
    {0}, {0}, 
    {&PORTB_PCR1, &GPIOB_PDDR, &GPIOB_PSOR, &GPIOB_PCOR, &GPIOB_PDIR, &GPIOB_PDOR, 1}, // Ard. 17
    {&PORTB_PCR3, &GPIOB_PDDR, &GPIOB_PSOR, &GPIOB_PCOR, &GPIOB_PDIR, &GPIOB_PDOR, 3}, // Ard. 18
    {&PORTB_PCR2, &GPIOB_PDDR, &GPIOB_PSOR, &GPIOB_PCOR, &GPIOB_PDIR, &GPIOB_PDOR, 2}, // Ard. 19
    {&PORTD_PCR5, &GPIOD_PDDR, &GPIOD_PSOR, &GPIOD_PCOR, &GPIOD_PDIR, &GPIOD_PDOR, 5}, // Ard. 20
    {&PORTD_PCR6, &GPIOD_PDDR, &GPIOD_PSOR, &GPIOD_PCOR, &GPIOD_PDIR, &GPIOD_PDOR, 6}, // Ard. 21
    {&PORTC_PCR1, &GPIOC_PDDR, &GPIOC_PSOR, &GPIOC_PCOR, &GPIOC_PDIR, &GPIOC_PDOR, 1}  // Ard. 22
};

SPI_CTRL spi_pins = { 
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
};

PIOP_CONF ds18b20_dq_pin = NULL;

PIOP_CONF analog_dial_pin = NULL;

PIOP_CONF switch_button_pin = NULL;

int main(void) {
    int               ret               = RET_OK;
    SPI_CMD           spi_cmd           = SET_DISP_ON;
    DISP_MODE_SUB_CMD spi_disp_mode_cmd = SET_ENTIRE_DISP_ON;
    float             temp_float_read   = 0;
    int               temp_reading      = 1;
    int               prev_temp_reading = 0;
    UBYTE             ds18b20_data[9]   = "";
    int               i                 = 0;
    int               analog_reading    = 0;

    timers_init();
    

    CONFIG_PIN_GPIO(&PORTC_PCR5);
    CONFIG_PIN_OUT(&GPIOC_PDDR, 5);

#ifdef INCLUDE_ADC_MODULE
    SET_PIN(&GPIOC_PSOR, 5);
    adc_module_init();
    _delay_ms(5000);
    CLR_PIN(&GPIOC_PCOR, 5);
#endif

    //SET_PIN(&GPIOC_PSOR, 5);
    //_delay_ms(5000);
    //CLR_PIN(&GPIOC_PCOR, 5);
    
    //SET_PIN(&GPIOC_PSOR, 5);
    io_pin_init();
    
    //CLR_PIN(&GPIOC_PCOR, 5);
    //SET_PIN(&GPIOC_PSOR, 5);
    spi_init();
    
    //CLR_PIN(&GPIOC_PCOR, 5);

    spi_cmd = SET_DISP_ON;
    spi_send_data(DC_COMMAND, spi_cmd);

    oled_reset_bitmap_buf();
    oled_draw_buffered_bitmap();

#if 0
    oled_draw_number(g_cycles_rem);
    oled_draw_buffered_bitmap();
    loop_delay(1000);

#if 0
    while (1) {
        SET_PIN(&GPIOC_PSOR, 5);
        _delay_ms(5000);
        SET_PIN(&GPIOC_PCOR, 5);
        _delay_ms(5000);
    }
#endif
#endif
#if 0
    CONFIG_PIN_IN(iop_get_pdd_reg(ds18b20_dq_pin), iop_get_prt_offset(ds18b20_dq_pin));
    DS18B20_read_string_data(
        ds18b20_data,
        9,
        (9 * 8)
    );
#endif
#if 0
    for (i = 0; i < 9; i++) {
        //oled_draw_char(i);
#if 1
        if (((ds18b20_data[i] & 0xF0) >> 4) >= 0 && ((ds18b20_data[i] & 0xF0) >> 4) < 10) {
            oled_draw_char((ds18b20_data[i] & 0xF0) >> 4);
        } else {
            switch ((ds18b20_data[i] & 0xF0) >> 4) {
                case 10:
                    oled_draw_char('A');
                    break;
                case 11:
                    oled_draw_char('B');
                    break;
                case 12:
                    oled_draw_char('C');
                    break;
                case 13:
                    oled_draw_char('D');
                    break;
                case 14:
                    oled_draw_char('E');
                    break;
                case 15:
                    oled_draw_char('F');
                    break;
            }
        }

        //oled_draw_char(' ');
        if ((ds18b20_data[i] & 0x0F) >= 0 && (ds18b20_data[i] & 0x0F) < 10) {
            oled_draw_char(ds18b20_data[i] & 0x0F);
        } else {
            switch (ds18b20_data[i] & 0x0F) {
                case 10:
                    oled_draw_char('A');
                    break;
                case 11:
                    oled_draw_char('B');
                    break;
                case 12:
                    oled_draw_char('C');
                    break;
                case 13:
                    oled_draw_char('D');
                    break;
                case 14:
                    oled_draw_char('E');
                    break;
                case 15:
                    oled_draw_char('F');
                    break;
            }
        }
#endif

        oled_draw_char(' ');
        oled_draw_char(' ');
    }
#endif

#ifdef TEMP_THERMO_APP
    SET_PIN(&GPIOC_PSOR, 5);
    oled_draw_string("TEMP: ", 6);
#if 0
    oled_draw_char('T');
    oled_draw_char('E');
    oled_draw_char('M');
    oled_draw_char('P');
    oled_draw_char(':');
#endif
    
    oled_draw_buffered_bitmap();
    CLR_PIN(&GPIOC_PCOR, 5);
#endif

#ifdef INCLUDE_ADC_MODULE
#if 0
    //oled_reset_bitmap_buf();
    //oled_draw_buffered_bitmap();
    SET_PIN(&GPIOC_PSOR, 5);
    oled_draw_string("A_R: ", 5);
    CLR_PIN(&GPIOC_PCOR, 5);
    oled_draw_buffered_bitmap();
#endif
#endif

#ifdef DISPLAY_MCGOUTCLK_FREQ
    oled_reset_bitmap_buf();
    oled_draw_string("B_FRE: ", 7);
    oled_draw_buffered_bitmap();
#endif
    
    while (INFINITE) {
#ifdef CHECK_NS_PER_CLK_PIT
        oled_reset_bitmap_buf();
        _delay_us(1000);
        oled_draw_number(g_cycles_rem);
        oled_draw_buffered_bitmap();
        oled_reset_bitmap_buf();
        oled_draw_number(0);
        oled_draw_buffered_bitmap();
#endif
#ifdef INCLUDE_ADC_MODULE
#if 0
        ret = iop_analog_read(analog_dial_pin, &analog_reading);
        if (ret < RET_OK) {
            oled_reset_bitmap_buf();
            oled_draw_number(ret);
            oled_draw_buffered_bitmap();

            inf_loop_isr();
        }
        oled_set_brightness(analog_reading / 16);
#endif
#if 0
        //CLR_PIN(&GPIOC_PCOR, 5);
        oled_draw_number(analog_reading);
        oled_draw_buffered_bitmap();
        //_delay_ms(5000);

        for (i = 0; i < number_get_digit_count(analog_reading); i++) {
            oled_draw_char(BACKSPACE_CHAR);
        }
        oled_draw_buffered_bitmap();
#endif
#endif
#ifdef INCLUDE_PUSH_BUTTON
        ret = push_button_read_input();
        if (!ret) {
            oled_turn_off();
        } else {
            oled_turn_on();
        }
        _delay_ms(50);
#endif
#ifdef DISPLAY_MCGOUTCLK_FREQ
        ret = sim_determine_bus_clk_freq();
        oled_draw_number(ret);
        oled_draw_buffered_bitmap();
        SET_PIN(&GPIOC_PSOR, 5);
        _delay_ms(10000);
        SET_PIN(&GPIOC_PCOR, 5);
        for (i = 0; i < number_get_digit_count(ret); i++) {
            oled_draw_char(BACKSPACE_CHAR);
        }
        oled_draw_buffered_bitmap();
#endif
#ifdef TEMP_THERMO_APP
        //SET_PIN(&GPIOC_PSOR, 5);
        #if 1
        if (DS18B20_get_temp(&temp_float_read) < RET_OK) {
            ret = RET_FAIL;
            oled_reset_bitmap_buf();
            oled_draw_number(ret);
            oled_draw_buffered_bitmap();
            inf_loop_isr();
        }
        #endif
        
        //loop_delay(100);
        
        //loop_delay(100);
        //_delay_ms(1000);
        temp_reading = (int)temp_float_read;
        // temp checking
        //temp_reading = 1;
        oled_draw_number(temp_reading);
        
#if 0
        oled_draw_char((temp_reading / 10));
        oled_draw_char((temp_reading % 10));
#endif
        oled_draw_char(DEGREE_CHAR);
        oled_draw_char('C');
        oled_draw_buffered_bitmap();
        //SET_PIN(&GPIOC_PCOR, 5);
        loop_delay(100);
#if 0
        oled_draw_char(0);
        oled_draw_buffered_bitmap();
        SET_PIN(&GPIOC_PSOR, 5);
        _delay_ms(5000);
        CLR_PIN(&GPIOC_PCOR, 5);
        oled_draw_char(BACKSPACE_CHAR);
        oled_draw_char(1);
        oled_draw_buffered_bitmap();
        SET_PIN(&GPIOC_PSOR, 5);
        _delay_ms(5000);
        CLR_PIN(&GPIOC_PCOR, 5);
        oled_draw_char(BACKSPACE_CHAR);
#endif
        
        oled_draw_char(BACKSPACE_CHAR);
        oled_draw_char(BACKSPACE_CHAR);
#if 1
        for (i = 0; i < number_get_digit_count(temp_reading); i++) {
            oled_draw_char(BACKSPACE_CHAR);
        }
        oled_draw_buffered_bitmap();
#endif
#if 0
        oled_draw_char(BACKSPACE_CHAR);
        oled_draw_char(BACKSPACE_CHAR);
#endif
#endif
    }

    return ret;
}