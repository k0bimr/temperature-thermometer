#ifndef PUSH_BUTTON_H
#define PUSH_BUTTON_H

#include "g_io_pin.h"

extern PIOP_CONF switch_button_pin;

int push_button_read_input(void);

#endif