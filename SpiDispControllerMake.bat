@ECHO OFF

SETLOCAL EnableDelayedExpansion

SET "_src_files=util.c,g_io_pin.c,MK20DX256.c,SpiDispController.c,DS18B20.c,oled_display.c,analog_dial.c,push_button.c,ADC.c,MCG.c,SIM.c"
SET "_obj_files="


REM IF EXIST g_io_pin.c.o DEL g_io_pin.c.o
REM IF EXIST DS18B20.c.o DEL DS18B20.c.o
REM IF EXIST MK20DX256.c.o DEL MK20DX256.c.o
REM IF EXIST util.c.o DEL util.c.O
REM IF EXIST SpiDispController.c.d DEL SpiDispController.c.d
REM IF EXIST SpiDispController.c.o DEL SpiDispController.c.o
REM IF EXIST SpiDispController.elf DEL SpiDispController.elf
REM IF EXIST SpiDispController.hex DEL SpiDispController.hex

SET _CC="C:\Program Files (x86)\Arduino\hardware\tools\arm\bin\arm-none-eabi-gcc.exe"
SET _OBJCP="C:\Program Files (x86)\Arduino\hardware\tools\arm\bin\arm-none-eabi-objcopy.exe"

SET "_C_C_FLAGS=-c -Wall -mcpu=cortex-m4 -nostdlib -mthumb -D__MX20DX256__ -DF_CPU=72000000 -fsingle-precision-constant"
SET "_C_L_FLAGS=-mcpu=cortex-m4 -T mk20dx256.ld -mthumb -fsingle-precision-constant -nostartfiles"

SET /A "_count=1"
:_L_START
FOR /F "tokens=%_count% delims=," %%G IN ("!_src_files!") DO (
    IF EXIST "%%G.o" DEL "%%G.o"
    SET _t_flag="1"
    REM ECHO %%G
    REM PAUSE
    %_CC% %_C_C_FLAGS% "%%G" -o "%%G.o"
    SET "_obj_files=^"%%G.o^" !_obj_files!"
    REM ECHO !_obj_files!
    REM PAUSE
)

IF NOT %_t_flag% == "1" (
    REM ECHO NOT DEFINED
    REM PAUSE
    GOTO _L_END
)
SET /A "_count=%_count% + 1"
SET _t_flag="0"
GOTO _L_START
:_L_END




%_CC% %_C_L_FLAGS% -o "SpiDispController.elf" %_obj_files% -L"C:\Program Files (x86)\Arduino\hardware\tools\arm\arm-none-eabi\lib\armv7e-m" -larm_cortexM4l_math -lm


REM "C:\Program Files (x86)\Arduino\hardware\tools\arm\bin\arm-none-eabi-gcc.exe" -c -O2 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -mthumb -mcpu=cortex-m4 -fsingle-precision-constant -D__MX20DX256__ -DTEENSYDUINO=153 -DARDUINO=10813 -DARDUINO_TEENSY32 -DF_CPU=72000000 -DUSB_SERIAL -DLAYOUT_US_ENGLISH "g_io_pin.c" -o "g_io_pin.c.o"
REM %_CC% %_C_C_FLAGS% "g_io_pin.c" -o "g_io_pin.c.o"
REM "C:\Program Files (x86)\Arduino\hardware\tools\arm\bin\arm-none-eabi-gcc.exe" -c -O2 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -mthumb -mcpu=cortex-m4 -fsingle-precision-constant -D__MX20DX256__ -DTEENSYDUINO=153 -DARDUINO=10813 -DARDUINO_TEENSY32 -DF_CPU=72000000 -DUSB_SERIAL -DLAYOUT_US_ENGLISH "SpiDispController.c" -o "SpiDispController.c.o"
REM %_CC% %_C_C_FLAGS% "SpiDispController.c" -o "SpiDispController.c.o"
REM "C:\Program Files (x86)\Arduino\hardware\tools\arm\bin\arm-none-eabi-ld.exe"  SpiDispController.c.o g_io_pin.c.o -nostdlib -nostartfiles -T linker_script.ld -o SpiDispController.elf
REM %_CC% %_C_C_FLAGS% "DS18B20.c" -o "DS18B20.c.o"
REM %_CC% %_C_C_FLAGS% "util.c" -o "util.c.o"

REM %_CC% %_C_L_FLAGS% -o "SpiDispController.elf" "SpiDispController.c.o" "g_io_pin.c.o" "DS18B20.c.o" "util.c.o" -L"C:\Program Files (x86)\Arduino\hardware\tools\arm\arm-none-eabi\lib\armv7e-m" -larm_cortexM4l_math -lm
REM --specs=nosys.specs
REM "C:\Program Files (x86)\Arduino\hardware\tools\arm\bin\arm-none-eabi-gcc.exe" -O2 -Wl,--gc-sections,--relax,--defsym=__rtc_localtime=1642331774 -mthumb -mcpu=cortex-m4 -fsingle-precision-constant -o "SpiDispController.elf" "SpiDispController.c.o" "g_io_pin.c.o" -larm_cortexM4l_math -lm
REM "C:\Program Files (x86)\Arduino\hardware\tools\arm\bin\arm-none-eabi-objcopy.exe" -O ihex -j .eeprom --set-section-flags=.eeprom=alloc,load --no-change-warnings --change-section-lma .eeprom=0 "SpiDispController.elf" "SpiDispController.eep"
%_OBJCP% -O ihex -R .eeprom "SpiDispController.elf" "SpiDispController.hex"
REM "C:\Program Files (x86)\Arduino\hardware\tools\teensy_post_compile.exe" "-file=C:\\Users\\nil\\Documents\\Programming\\programs\\C\\SPI_Controller\\SpiDispController.c" "-path=C:\\Users\\nil\\Documents\\Programming\\programs\\C\\SPI_Controller" "-tools=C:\\Program Files (x86)\\Arduino\\hardware\\teensy/../tools/" -board=TEENSY32
PAUSE
ENDLOCAL
