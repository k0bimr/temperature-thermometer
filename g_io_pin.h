#ifndef G_IO_PIN_H_
#define G_IO_PIN_H_

#include "SpiDispController.h"
#include "util.h"

#ifdef INCLUDE_ADC_MODULE
    #include "ADC.h"
#endif

#define NUM_IO_PINS  8
#define NUM_SPI_PINS 5

/* Teensy/Arduino pins */
#define SPI_CLK_PIN       21 /* --> PT D6 */
#define SPI_DIN_PIN       20 /* --> PT D5 */
#define SPI_CS_PIN        19 /* --> PT B2 */
#define SPI_DC_PIN        18 /* --> PT B3 */
#define SPI_RST_PIN       17 /* --> PT B1 */

#define DS18B20_DQ_PIN    22

#define ANALOG_DIAL_PIN   14 /* --> PT D1 */ /* A0 */

#define SWITCH_BUTTON_PIN 12 /* --> PT C7 */

/* Typedefs */
typedef enum INTERRUPT_CONFIGURATIONS {
    IRQC_INT_DMA_REQ_DISABLED        = 0x0,
    IRQC_DMA_REQ_RISING_EDGE         = 0x1,
    IRQC_DMA_REQ_FALLING_EDGE        = 0x2,
    IRQC_DMA_REQ_RISING_FALLING_EDGE = 0x3,
    IRQC_INT_LOGIC_ZERO              = 0x8,
    IRQC_INT_RISING_EDGE             = 0x9,
    IRQC_INT_FALLING_EDGE            = 0xA,
    IRQC_INT_RISING_FALLING_EDGE     = 0xB,
    IRQC_INT_LOGIC_ONE               = 0xC
} IRQC;

typedef struct ARD_PIN_MAP_LOOKUP {
    REG_UL_T pc_reg;
    REG_UL_T pdd_reg;
    REG_UL_T pso_reg;
    REG_UL_T pco_reg;
    REG_UL_T pri_reg;
    REG_UL_T pro_reg;
    UBYTE    prt_offset;  // port number - not to be confused with arduino pin number
} PIN_REG_LOOKUP;

typedef struct G_IO_PIN_CONFIG {
    UBYTE ard_pin;
    PDD_T pin_dd;
    struct {
        REG_UL_T pc_reg;
        REG_UL_T pdd_reg;
        struct {
            struct {
                REG_UL_T pso_reg;
                REG_UL_T pco_reg;
            } out_ctrl;
            REG_UL_T pri_reg;
            REG_UL_T pro_reg;
        } io_ctrl;
        UBYTE prt_offset; // port offset (pin) - not to be confused with arduino pin number
    } pin_ctrl;
} IOP_CONF, *PIOP_CONF;

typedef struct SPI_CONTROLLER_PINS {
    PIOP_CONF clk_pin;
    PIOP_CONF din_pin;
    PIOP_CONF cs_pin;
    PIOP_CONF dc_pin;
    PIOP_CONF rst_pin;
} SPI_CTRL;

/* Globals */
extern       IOP_CONF       io_pins[];
extern const PIN_REG_LOOKUP pin_to_reg_map[];
extern       SPI_CTRL       spi_pins;
extern       PIOP_CONF      ds18b20_dq_pin;
extern       PIOP_CONF      analog_dial_pin;
extern       PIOP_CONF      switch_button_pin;

/* Prototypes */
void     io_pin_init(void);
UBYTE    iop_get_ard_pin(PIOP_CONF io_pin);
REG_UL_T iop_get_pdd_reg(PIOP_CONF io_pin);
int      iop_set_pdd_reg(PIOP_CONF io_pin, REG_UL_T pdd_reg);
PDD_T    iop_get_pdd(PIOP_CONF io_pin);
int      iop_set_pso_reg(PIOP_CONF io_pin, REG_UL_T pso_reg);
REG_UL_T iop_get_pso_reg(PIOP_CONF io_pin);
int      iop_set_pco_reg(PIOP_CONF io_pin, REG_UL_T pco_reg);
REG_UL_T iop_get_pco_reg(PIOP_CONF io_pin);
int      iop_set(PIOP_CONF io_pin);
int      iop_clr(PIOP_CONF io_pin);
#ifdef INCLUDE_ADC_MODULE
    int iop_analog_read(PIOP_CONF io_pin, int* analog_read);
    int iop_analog_write(PIOP_CONF io_pin, int analog_write);
#endif
int      iop_set_prt_offset(PIOP_CONF io_pin, UBYTE prt_offset);
UBYTE    iop_get_prt_offset(PIOP_CONF io_pin);
int      iop_set_pri_reg(PIOP_CONF io_pin, REG_UL_T pri_reg);
REG_UL_T iop_get_pri_reg(PIOP_CONF io_pin);
int      iop_set_pro_reg(PIOP_CONF io_pin, REG_UL_T pro_reg);
REG_UL_T iop_get_pro_reg(PIOP_CONF io_pin);
UBYTE    iop_read_in(PIOP_CONF io_pin);
UBYTE    iop_read_out(PIOP_CONF io_pin);
REG_UL_T iop_get_pcr_reg(PIOP_CONF io_pin);
int      iop_set_pcr_reg(PIOP_CONF io_pin, REG_UL_T pcr);
int      iop_set_pcr_bits(PIOP_CONF io_pin, REG_BITMASK pcr_bitmask);
int      iop_clr_pcr_bits(PIOP_CONF io_pin, REG_BITMASK pcr_bitmask);
int      iop_set_input_mode(PIOP_CONF io_pin);
int      iop_set_output_mode(PIOP_CONF io_pin);
int      iop_configure_interrupt(PIOP_CONF io_pin, IRQC int_type);

#endif