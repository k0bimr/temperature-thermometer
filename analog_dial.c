#include "analog_dial.h"

int dial_init(void) {

}

int read_dial_reading(void) {
    int analog_ret = 0;

    if (iop_analog_read(analog_dial_pin, &analog_ret) < RET_OK) {
        analog_ret = RET_FAIL;
    }
    
    return analog_ret;
}