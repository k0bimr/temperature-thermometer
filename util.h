#ifndef UTIL_H
#define UTIL_H

#include "SpiDispController.h"
#include <math.h>
#include <float.h>

#define NUM_BITS_IN_BYTE 8

#define N_MAX(x, y) (x > y ? (x) : (y))
#define N_MIN(x, y) (x < y ? (x) : (y))
#define N_ABS(x) (x < 0 ? (-1 * x) : (x))

#define DEREF_REG(reg_addr) *((REG_UL_T) reg_addr)
#define DEREF_REG_SHORT(reg_addr) *((REG_US_T) reg_addr)
#define DEREF_REG_BYTE(reg_addr) *((REG_UB_T) reg_addr)

#define CONFIG_PIN_GPIO(reg) ((*reg) |= (PORTX_PCRN_MUX_GPIO))
#define CONFIG_PIN_OUT(reg, pin) ((*reg) |= (1 << pin))
#define CONFIG_PIN_IN(reg, pin) ((*reg) &= ~(1 << pin))
#define REG_SET_BIT(reg, bit_n) ((*reg) |= (1 << bit_n))
#define SET_PIN(reg, pin) (REG_SET_BIT(reg, pin))
#define CLR_PIN(reg, pin) (REG_SET_BIT(reg, pin))

#define PORTX_PCRN_MUX_1    8
#define PORTX_PCRN_MUX_2    9
#define PORTX_PCRN_MUX_3    10
#define PORTX_PCRN_MUX_GPIO (1 << PORTX_PCRN_MUX_1)

#define OSC_CR   (DEREF_REG_BYTE(0x40065000))
    /* OSC_CR Bits */
    #define ERCLKEN  7
    #define EREFSTEN 5
    #define SC2P     3
    #define SC4P     2
    #define SC8P     1
    #define SC16P    0

    #define OSC_ENABLE_OSCERCLK            (OSC_CR |= (1 << ERCLKEN))
    #define OSC_SET_10_pF_CAPACITANCE_LOAD (OSC_CR |= ((1 << SC2P) | (1 << SC8P)))

#define SIM_CLKDIV1 (DEREF_REG(0x40048044))
    /* SIM_CLKDIV1 Bits */
    #define OUTDIV1_E 31
    #define OUTDIV1_S 28
    #define OUTDIV1_3 31
    #define OUTDIV1_2 30
    #define OUTDIV1_1 29
    #define OUTDIV1_0 28
    #define OUTDIV2_3 27
    #define OUTDIV2_2 26
    #define OUTDIV2_1 25
    #define OUTDIV2_0 24
    #define OUTDIV2_E 27
    #define OUTDIV2_S 24
    #define OUTDIV4_E 19
    #define OUTDIV4_S 16
    #define OUTDIV4_3 19
    #define OUTDIV4_2 18
    #define OUTDIV4_1 17
    #define OUTDIV4_0 16


    #define SIM_CLKDIV1_OUTDIV1_BITMASK ((1 << OUTDIV1_3) | (1 << OUTDIV1_2) | (1 << OUTDIV1_1) | (1 << OUTDIV1_0))
    #define SIM_CLKDIV1_OUTDIV1_SHIFT   (OUTDIV1_0)
    #define SIM_SET_OUTDIV1_DIVIDER_4   (SIM_CLKDIV1 = ((SIM_CLKDIV1 & ~(SIM_CLKDIV1_OUTDIV1_BITMASK)) | ((0 << OUTDIV1_3) | (0 << OUTDIV1_2) | (1 << OUTDIV1_1) | (1 << OUTDIV1_0))))

    
    #define SIM_CLKDIV1_OUTDIV1(n) ()
    #define SIM_CLKDIV1_OUTDIV2_BITMASK ((1 << OUTDIV2_3) | (1 << OUTDIV2_2) | (1 << OUTDIV2_1) | (1 << OUTDIV2_0))
    #define SIM_CLKDIV1_OUTDIV2_SHIFT   (OUTDIV2_0)
    #define SIM_SET_OUTDIV2_DIVIDER_4   (SIM_CLKDIV1 = ((SIM_CLKDIV1 & ~(SIM_CLKDIV1_OUTDIV2_BITMASK)) | (0 << OUTDIV2_3) | (0 << OUTDIV2_2) | (1 << OUTDIV2_1) | (1 << OUTDIV2_0)))
    #define SIM_CLKDIV1_GET_OUTDIV2     ((SIM_CLKDIV1 & SIM_CLKDIV1_OUTDIV2_BITMASK) >> SIM_CLKDIV1_OUTDIV2_SHIFT)
    #define SIM_CLKDIV1_OUTDIV4(n) ()

    #define SIM_CLKDIV1_OUTDIV1_DIV_FACTOR_4 ((0 << OUTDIV1_3) | (0 << OUTDIV1_2) | (1 << OUTDIV1_1) | (1 << OUTDIV1_0)) 
    #define SIM_CLKDIV1_OUTDIV2_DIV_FACTOR_4 ((0 << OUTDIV2_3) | (0 << OUTDIV2_2) | (1 << OUTDIV2_1) | (1 << OUTDIV2_0)) 
    #define SIM_CLKDIV1_OUTDIV4_DIV_FACTOR_4 ((0 << OUTDIV4_3) | (0 << OUTDIV4_2) | (1 << OUTDIV4_1) | (1 << OUTDIV4_0)) 

#define SIM_CLKDIV2 (DEREF_REG(0x40048048))
    /* SIM_CLKDIV2 Bits */
    #define USBDIV_E 3
    #define USBDIV_S 1
    #define USBFRAC  0

#define SIM_SOPT1    (DEREF_REG(0x40047000))
    #define USBREGEN     31
    #define USBSSTBY     30
    #define USBVSTBY     29
    #define OSC32KSEL_E  19
    #define OSC32KSEL_S  18
    #define RAMSIZE_E    15
    #define RAMSIZE_S    12


#define SIM_SOPT2    (DEREF_REG(0x40048004))
    /* SIM_SOPT2 Bits */
    #define USBSRC       18
    #define PLLFLLSEL    16
    #define TRACECLKSEL  12
    #define PTD7PAD      11
    #define CLKOUTSEL_E  7
    #define CLKOUTSEL_S  5
    #define RTCCLKOUTSEL 4

#define MCG_C1       (DEREF_REG_BYTE(0x40064000))
    /* MCG_C1 Bits */
    /*
    Clock Source Select
    Selects the clock source for MCGOUTCLK .
    00 Encoding 0 — Output of FLL or PLL is selected (depends on PLLS control bit).
    01 Encoding 1 — Internal reference clock is selected.
    10 Encoding 2 — External reference clock is selected.
    11 Encoding 3 — Reserved.
    */
    #define CLKS_E   7
    #define CLKS_S   6
    #define CLKS_1   7
    #define CLKS_0   6
    #define FRDIV_E  5
    #define FRDIV_S  3
    #define FRDIV_2  5
    #define FRDIV_1  4
    #define FRDIV_0  3
    #define IREFS    2
    #define IRCLKEN  1
    #define IREFSTEN 0
    
    #define MCG_C1_CLKS_BITMASK       ((1 << CLKS_1) | (1 << CLKS_0))
    #define MCG_C1_CLKS_SHIFT         (CLKS_0)
    /* Returns the clock source for the MCGCOUTCLK */
    #define MCG_C1_GET_CLKS           ((MCG_C1 & MCG_C1_CLKS_BITMASK) >> MCG_C1_CLKS_SHIFT)
    #define MCG_GET_MCGOUTCLK_SRC     MCG_C1_GET_CLKS
    #define MCG_C1_CLKS_FLLPLLCLK     ((0 << CLKS_1) | (0 << CLKS_0))
    #define MCG_C1_CLKS_INTREFCLK     ((0 << CLKS_1) | (1 << CLKS_0))
    #define MCG_C1_CLKS_EXTREFCLK     ((1 << CLKS_1) | (0 << CLKS_0))

    #define MCG_SET_MCGCOUTCLK_SRC_FLLPLL (MCG_C1 = ((MCG_C1 & ~MCG_C1_CLKS_BITMASK) | MCG_C1_CLKS_FLLPLLCLK))
    #define MCG_SET_MCGCOUTCLK_SRC_INTREF (MCG_C1 = ((MCG_C1 & ~MCG_C1_CLKS_BITMASK) | MCG_C1_CLKS_INTREFCLK))
    #define MCG_SET_MCGCOUTCLK_SRC_EXTREF (MCG_C1 = ((MCG_C1 & ~MCG_C1_CLKS_BITMASK) | MCG_C1_CLKS_EXTREFCLK))

    #define MCG_C1_FRDIV_BITMASK       ((1 << FRDIV_0) | (1 << FRDIV_1) | (1 << FRDIV_2))
    #define MCG_C1_FRDIV_SHIFT         (FRDIV_0)
    #define MCG_C1_GET_FRDIV           ((MCG_C1 & MCG_C1_FRDIV_BITMASK) >> MCG_C1_FRDIV_SHIFT)
    #define MCG_SET_FLL_EXTREF_DIV_1   (MCG_C1 = (MCG_C1 & ~(MCG_C1_FRDIV_BITMASK)) | ((0 << FRDIV_2) | (0 << FRDIV_1) | (0 << FRDIV_0)))
    #define MCG_SET_FLL_EXTREF_DIV_2   (MCG_C1 = (MCG_C1 & ~(MCG_C1_FRDIV_BITMASK)) | ((0 << FRDIV_2) | (0 << FRDIV_1) | (1 << FRDIV_0)))
    #define MCG_SET_FLL_EXTREF_DIV_4   (MCG_C1 = (MCG_C1 & ~(MCG_C1_FRDIV_BITMASK)) | ((0 << FRDIV_2) | (1 << FRDIV_1) | (0 << FRDIV_0)))
    #define MCG_SET_FLL_EXTREF_DIV_8   (MCG_C1 = (MCG_C1 & ~(MCG_C1_FRDIV_BITMASK)) | ((0 << FRDIV_2) | (1 << FRDIV_1) | (1 << FRDIV_0)))
    #define MCG_SET_FLL_EXTREF_DIV_16  (MCG_C1 = (MCG_C1 & ~(MCG_C1_FRDIV_BITMASK)) | ((1 << FRDIV_2) | (0 << FRDIV_1) | (0 << FRDIV_0)))
    #define MCG_SET_FLL_EXTREF_DIV_512 (MCG_C1 = (MCG_C1 & ~(MCG_C1_FRDIV_BITMASK)) | ((1 << FRDIV_2) | (0 << FRDIV_1) | (0 << FRDIV_0)))

    #define MCG_C1_IREFS_EXTREFCLK_SEL      (0 << IREFS)
    #define MCG_C1_IREFS_SLOW_INTREFCLK_SEL (1 << IREFS)

    #define MCG_SET_FLL_SRC_CLK_EXTREF      (MCG_C1 &= ~(1 << IREFS))
    #define MCG_SET_FLL_SRC_CLK_INTREF      (MCG_C1 |=  (1 << IREFS))

    #define MCGOUTCLK_SET_FLLPLL_SRCLK (MCG_C1 &= ~((1 << CLKS_S) | (1 << CLKS_E)))
    #define MCGOUTCLK_SET_IRCLK_SRCLK ( \
        MCG_C1 &= ~(1 << CLKS_E), \
        MCG_C1 |=  (1 << CLKS_S)  \
    )
    #define MCGOUTCLK_SET_ERCLK_SRCLK ( \
        MCG_C1 |=  (1 << CLKS_E), \
        MCG_C1 &= ~(1 << CLKS_S)  \
    )

#define MCG_C2   (DEREF_REG_BYTE(0x40064001))
    /* MCG_C2 Bits */
    #define LOCRE0   7
    #define RANGE0_E 5
    #define RANGE0_S 4
    #define RANGE0_1 5
    #define RANGE0_0 4
    #define HGO0     3
    #define EREFS0   2
    #define LP       1
    #define IRCS     0

    #define MCG_DISABLE_LOW_POWER           (MCG_C2 &= ~(1 << LP))
    #define MCG_EXTREFCLK_SRC_OSC_SEL       (MCG_C2 |=  (1 << EREFS0))
    #define MCG_EXTREFCLK_SRC_EXTREFCLK_SEL (MCG_C2 &= ~(1 << EREFS0))

    #define XTAL_FREQ_RANGE_SET_LOW       (MCG_C2 &= ~((1 << RANGE0_E) | (1 << RANGE0_S)))
    #define XTAL_FREQ_RANGE_SET_HIGH      ( \
        MCG_C2 &= ~(1 << RANGE0_E), \
        MCG_C2 |=  (1 << RANGE0_S)  \
    )
    #define MCG_XTAL_FREQ_RANGE_SET_VERY_HIGH (MCG_C2 |= (1 << RANGE0_1))
    #define MCG_C2_RANGE0_BITMASK ((1 << RANGE0_E) | (1 << RANGE0_S))
    #define MCG_C2_RANGE0_SHIFT   (RANGE0_S)
    #define MCG_C2_GET_RANGE0     ((MCG_C2 & MCG_C2_RANGE0_BITMASK) >> MCG_C2_RANGE0_SHIFT)

    #define MCG_C2_GET_IRCS       (MCG_C2 & (1 << IRCS))

    #define MCG_C2_RANGE0(n) ()

#define MCG_C3   (DEREF_REG_BYTE(0x40064002))
    /* MCG_C3 Bits */
    #define SCTRIM_E 7
    #define SCTRIM_S 0

    #define SCTRIM_7 7
    #define SCTRIM_6 6
    #define SCTRIM_5 5
    #define SCTRIM_4 4
    #define SCTRIM_3 3
    #define SCTRIM_2 2
    #define SCTRIM_1 1
    #define SCTRIM_0 0

#define MCG_C4   (DEREF_REG_BYTE(0x40064003))
    /* MCG_C4 Bits */
    #define DMX32      7
    #define DRST_DRS_E 6
    #define DRST_DRS_S 5
    #define FCTRIM_E   4
    #define FCTRIM_S   1
    #define SCFTRIM    0

    #define MCG_C4_DRST_DRS_BITMASK ((1 << DRST_DRS_E) | (1 << DRST_DRS_S))
    #define MCG_C4_DRST_DRS_SHIFT   (DRST_DRS_S)
    #define MCG_C4_GET_DRST_DRS     ((MCG_C4 & MCG_C4_DRST_DRS_BITMASK) >> MCG_C4_DRST_DRS_SHIFT)

#define MCG_C5   (DEREF_REG_BYTE(0x40064004))
    /* MCG_C5 Bits */
    #define PLLCLKEN0 6
    #define PLLSTEN0  5
    #define PRDIV0_E  4
    #define PRDIV0_S  0
    #define PRDIV0_4  4
    #define PRDIV0_3  3
    #define PRDIV0_2  2
    #define PRDIV0_1  1
    #define PRDIV0_0  0

    #define MCG_C5_PRDIV0_BITMASK ((1 << PRDIV0_0) | (1 << PRDIV0_1) | (1 << PRDIV0_2) | (1 << PRDIV0_3) | (1 << PRDIV0_4))
    #define MCG_C5_PRDIV0_SHIFT   (PRDIV0_0)
    #define MCG_C5_GET_PRDIV0     ((MCG_C5 & MCG_C5_PRDIV0_BITMASK) >> MCG_C5_PRDIV0_SHIFT)

    #define MCG_SET_PLL_DIV_FACTOR_4 (MCG_C5 = ((MCG_C5 & ~(MCG_C5_PRDIV0_BITMASK)) | ((0 << PRDIV0_4) | (0 << PRDIV0_3) | (0 << PRDIV0_2) | (1 << PRDIV0_1) | (1 << PRDIV0_0))))

#define MCG_C6   (DEREF_REG_BYTE(0x40064005))
    /* MCG_C6 Bits */
    #define LOLIE0  7
    #define PLLS    6
    #define CME0    5
    #define VDIV0_E 4
    #define VDIV0_S 0
    #define VDIV0_4 4
    #define VDIV0_3 3
    #define VDIV0_2 2
    #define VDIV0_1 1
    #define VDIV0_0 0

    #define MCG_C6_VDIV0_BITMASK ((1 << VDIV0_0) | (1 << VDIV0_1) | (1 << VDIV0_2) | (1 << VDIV0_3) | (1 << VDIV0_4))
    #define MCG_C6_VDIV0_SHIFT   (VDIV0_0)
    #define MCG_C6_GET_VDIV0     ((MCG_C6 & MCG_C6_VDIV0_BITMASK) >> MCG_C6_VDIV0_SHIFT)

    #define MCG_SET_VCO_MULT_FACTOR_24 (MCG_C6 = ((MCG_C6 & ~(MCG_C6_VDIV0_BITMASK)) | ((0 << VDIV0_4) | (0 << VDIV0_3) | (0 << VDIV0_2) | (0 << VDIV0_1) | (0 << VDIV0_0))))

    #define MCG_C6_PLLS_FLL_SEL  (0 << PLLS)
    #define MCG_C6_PLLS_PLL_SEL  (1 << PLLS)

    #define MCG_SET_PLLS_FLL_SEL (MCG_C6 &= ~(1 << PLLS))
    #define MCG_SET_PLLS_PLL_SEL (MCG_C6 |=  (1 << PLLS))

#define MCG_C7   (DEREF_REG_BYTE(0x4006400C))
    #define OSCSEL   0

    #define MCG_C7_GET_OSCSEL (MCG_C7 & (1 << OSCSEL))

#define MCG_S    (DEREF_REG_BYTE(0x40064006))
    /* MCG_S Bits */
    #define LOLS0    7
    #define LOCK0    6
    #define PLLST    5
    #define IREFST   4
    #define CLKST1   3
    #define CLKST0   2 
    #define OSCINIT0 1
    #define IRCST    0

    #define MCG_S_CLKST_BITMASK                ((1 << CLKST1) | (1 << CLKST0))
    #define MCG_S_CLKST_SHIFT                  (CLKST0)
    #define MCG_S_GET_CLKST                    ((MCG_S & MCG_S_CLKST_BITMASK) >> MCG_S_CLKST_SHIFT)
    #define MCG_S_CLKST_MCGOUTCLK_SRC_PLL_SEL  ((1 << CLKST1) | (1 << CLKST0))

    #define MCG_AWAIT_OSC_INITIALIZATION       while (!(MCG_S & (1 << OSCINIT0)))
    #define MCG_AWAIT_FLL_EXTREF_CLK_SRC       while ((MCG_S & (1 << IREFST)))
    #define MCG_AWAIT_MCGOUTCLK_EXTREF_CLK_SRC while ((MCG_C1_CLKS_EXTREFCLK >> MCG_C1_CLKS_SHIFT) != MCG_GET_MCGOUTCLK_SRC)
    #define MCG_AWAIT_PLL_CLK                  while (!(MCG_S & (1 << PLLST)))
    #define MCG_AWAIT_PLL_LOCK_ACQUIRED        while (!(MCG_S & (1 << LOCK0)))
    #define MCG_AWAIT_MCGOUTCLK_SRC_PLL_SEL    while ((MCG_S_CLKST_MCGOUTCLK_SRC_PLL_SEL >> MCG_S_CLKST_SHIFT) != MCG_S_GET_CLKST)

#define SMC_PMPROT   (DEREF_REG_BYTE(0x4007E000))
    /* SMC_PMPROT Bits */
    #define AVLP  5
    #define ALLS  3
    #define AVLLS 1

#define PMC_REGSC (DEREF_REG_BYTE(0x4007D002))
    #define BGEN   4
    #define ACKISO 3
    #define REGONS 2
    #define BGBE   0

#define ENABLE_ALL_PWR_MODES ( \
    SMC_PMPROT = ( \
        (1 << AVLP) | \
        (1 << ALLS) | \
        (1 < AVLLS) \
    ) \
)

#define WDOG_UNLOCK  (DEREF_REG_SHORT(0x4005200E))
#define WDOG_UNLOCK_SEQ_1 (0xC520)
#define WDOG_UNLOCK_SEQ_2 (0xD928)
#define UNLOCK_WDOG_REGS ( \
    WDOG_UNLOCK = WDOG_UNLOCK_SEQ_1, \
    WDOG_UNLOCK = WDOG_UNLOCK_SEQ_2  \
)
#define WDOG_STCTRLH (DEREF_REG_SHORT(0x40052000))
    /* WDOG_STCTRLH Bits */
    #define DISTESTWDOG 14
    #define BYTESEL1    13
    #define BYTESEL0    12
    #define TESTSEL     11
    #define TESTWDOG    10
    #define WAITEN      7
    #define STOPEN      6
    #define DBGEN       5
    #define ALLOWUPDATE 4
    #define WINEN       3
    #define IRQRSTEN    2
    #define CLKSRC      1
    #define WDOGEN      0

    #define DISABLE_WDOG_TIMER ( \
        WDOG_STCTRLH &= ~(1 << WDOGEN), \
        WDOG_STCTRLH |=  (1 << ALLOWUPDATE) \
    )

#define SIM_SCGC3   (DEREF_REG(0x40048030))
    #define FTM2        24
    #define ADC1        27

#define SIM_SCGC5   (DEREF_REG(0x40048038))
    /* SIM_SCGC5 Bits */
    #define PORTA       9
    #define PORTB       10
    #define PORTC       11
    #define PORTD       12
    #define PORTE       13

#define SIM_SCGC6   (DEREF_REG(0x4004803C))
    #define FTFL        0
    #define DMAMUX      1
    #define FLEXCAN0    4
    #define SPI0        12
    #define SPI1        13
    #define I2S         15
    #define CRC         18
    #define USBDCD      21
    #define PDB         22
    #define PIT         23
    #define FTM0        24
    #define FTM1        25
    #define ADC0        27
    #define RTC         29

#define ENABLE_ALL_GPIO_CLOCKS ( \
    SIM_SCGC5 |= ( \
        (1 << PORTA) | \
        (1 << PORTB) | \
        (1 << PORTC) | \
        (1 << PORTD) | \
        (1 << PORTE) \
    ) \
)

#define ENABLE_ALL_ADC_CLOCKS ( \
    SIM_SCGC6 |= (1 << ADC0), \
    SIM_SCGC3 |= (1 << ADC1)  \
)

#define PORTA_GPCLR (DEREF_REG(0x40049080))
#define PORTA_GPCHR (DEREF_REG(0x40049084))

#define PORTB_GPCLR (DEREF_REG(0x4004A080))
#define PORTB_GPCHR (DEREF_REG(0x4004A084))

#define PORTC_GPCLR (DEREF_REG(0x4004B080))
#define PORTC_GPCHR (DEREF_REG(0x4004B084))

#define PORTD_GPCLR (DEREF_REG(0x4004C080))
#define PORTD_GPCHR (DEREF_REG(0x4004C084))

#define PORTE_GPCLR (DEREF_REG(0x4004D080))
#define PORTE_GPCHR (DEREF_REG(0x4004D084))

#define PORTB_PCR1 (DEREF_REG(0x4004A004))
#define PORTB_PCR2 (DEREF_REG(0x4004A008))
#define PORTB_PCR3 (DEREF_REG(0x4004A00C))

#define GPIOA_PDOR (DEREF_REG(0x400FF000)

#define GPIOB_PDOR (DEREF_REG(0x400FF040))
#define GPIOB_PDDR (DEREF_REG(0x400FF054))
#define GPIOB_PSOR (DEREF_REG(0x400FF044))
#define GPIOB_PCOR (DEREF_REG(0x400FF048))
#define GPIOB_PDIR (DEREF_REG(0x400FF050))

#define PORTC_PCR1 (DEREF_REG(0x4004B004))
#define PORTC_PCR5 (DEREF_REG(0x4004B014))
#define PORTC_PCR7 (DEREF_REG(0x4004B01C))
#define GPIOC_PDOR (DEREF_REG(0x400FF080))
#define GPIOC_PDDR (DEREF_REG(0x400FF094))
#define GPIOC_PDOR (DEREF_REG(0x400FF080))
#define GPIOC_PSOR (DEREF_REG(0x400FF084))
#define GPIOC_PCOR (DEREF_REG(0x400FF088))
#define GPIOC_PDIR (DEREF_REG(0x400FF090))

#define PORTD_PCR1 (DEREF_REG(0x4004C004))
#define PORTD_PCR5 (DEREF_REG(0x4004C014))
#define PORTD_PCR6 (DEREF_REG(0x4004C018))
#define GPIOD_PDDR (DEREF_REG(0x400FF0D4))
#define GPIOD_PSOR (DEREF_REG(0x400FF0C4))
#define GPIOD_PCOR (DEREF_REG(0x400FF0C8))
#define GPIOD_PDIR (DEREF_REG(0x400FF0D0))
#define GPIOD_PDOR (DEREF_REG(0x400FF0C0))

#define PORTA_ISFR (DEREF_REG(0x400490A0))
#define PORTB_ISFR (DEREF_REG(0x4004A0A0))
#define PORTC_ISFR (DEREF_REG(0x4004B0A0))
#define PORTD_ISFR (DEREF_REG(0x4004C0A0))
#define PORTE_ISFR (DEREF_REG(0x4004D0A0))

/* PORTX_PCRN Bits */
#define ISF    24
#define IRQC_3 19
#define IRQC_2 18
#define IRQC_1 17
#define IRQC_0 16
#define LK     15
#define MUX_2  10
#define MUX_1  9
#define MUX_0  8
#define DSE    6
#define ODE    5
#define PFE    4
#define SRE    2
#define PE     1
#define PS     0

#define PORTX_PCRN_IRQC_BITMASK ((1 << IRQC_3) | (1 << IRQC_2) | (1 << IRQC_1) | (1 << IRQC_0))
#define PORTX_PCRN_IRQC_SHIFT   (IRQC_0)

/* PIT Memory Map */
#define PIT_MCR    (DEREF_REG(0x40037000)) /* PIT Module Control Register */
    /* PIT_MCR Bits */
    #define MDIS       1
    #define FRZ        0

#define PIT_LDVAL0 (DEREF_REG(0x40037100)) /* Timer 1 Load Value Register */
#define PIT_CVAL0  (DEREF_REG(0x40037104))
#define PIT_TCTRL0 (DEREF_REG(0x40037108))
#define PIT_TFLG0  (DEREF_REG(0x4003710C))
#define PIT_LDVAL1 (DEREF_REG(0x40037110))
#define PIT_CVAL1  (DEREF_REG(0x40037114))
#define PIT_TCTRL1 (DEREF_REG(0x40037118))
#define PIT_TFLG1  (DEREF_REG(0x4003711C))
#define PIT_LDVAL2 (DEREF_REG(0x40037120))
#define PIT_CVAL2  (DEREF_REG(0x40037124))
#define PIT_TCTRL2 (DEREF_REG(0x40037128))
#define PIT_TFLG2  (DEREF_REG(0x4003712C))
#define PIT_LDVAL3 (DEREF_REG(0x40037130))
#define PIT_CVAL3  (DEREF_REG(0x40037134))
#define PIT_TCTRL3 (DEREF_REG(0x40037138))
#define PIT_TFLG3  (DEREF_REG(0x4003713C))

/* PIT_TCTRLn Bits */
#define CHN        2
#define TIE        1
#define TEN        0

#define PIT_TIMER_INTERRUPT_ENABLE (1 << TIE)
#define PIT_TIMER_ENABLE           (1 << TEN)

/* PIT_TFLGn Bits */
#define TIF        0

#define PIT_TIMER_INTERRUPT_FLAG_CLEAR (1 << TIF)

#define PIT_TIMER_0 0
#define PIT_TIMER_1 1
#define PIT_TIMER_2 2
#define PIT_TIMER_3 3

#define PIT_TIMER_RESOLVE_N(timer) (PIT_TFLG ## timer)
#define PIT_TIMER_N_EXPIRED(timer) (PIT_TIMER_RESOLVE_N(timer) & (1 << TIF))


#define VREF_TRM (DEREF_REG_BYTE(0x40074000))
    #define CHOPEN 6
    #define TRIM_5 5
    #define TRIM_4 4
    #define TRIM_3 3
    #define TRIM_2 2
    #define TRIM_1 1
    #define TRIM_0 0

#define VREF_SC  (DEREF_REG_BYTE(0x40074001))
    #define VREFEN    7
    #define REGEN     6
    #define ICOMPEN   5
    #define VREFST    2
    #define MODE_LV_1 1
    #define MODE_LV_0 0

extern ULLONG g_cycles_rem;

/* Timer functions */
ULLONG pit_calculate_ns_per_clk_cycle(void);
void   timers_init(void);
void   _delay_us(ULONG delayus);
void   _delay_ms(ULONG delayms);
void   _timer_delay(ULONG delayns);
void   loop_delay(ULONG ms);
void   pit_set_timer(ULONG ms, ULONG us, ULONG ns);

int  number_get_digit_count(int num);

int vect_dot_product(float* vect_a, float* vect_b, UBYTE vect_a_size, UBYTE vect_b_size, float* dot_prod_out);
int vect_magnitude(float* vect_a, UBYTE vect_a_size, float* mag_out);
int matrix_multiply(float* matx_a, float* matx_b, UBYTE matx_a_num_rows, UBYTE matx_a_num_cols, UBYTE matx_b_num_rows, UBYTE matx_b_num_cols, float* matx_prod_out);

//#include "oled_display.h"

#endif