#include "onewire.h"

UBYTE parasite_power = B_FALSE;

typedef enum DS18B20_TEMP_RESOLUTIONS {
    DS18B20_TEMP_RES_9  = 9,  // 0.5 C inc
    DS18B20_TEMP_RES_10 = 10, // 0.25 C inc
    DS18B20_TEMP_RES_11 = 11, // 0.125 C inc
    DS18B20_TEMP_RES_12 = 12 // 0.0625 C inc
} DS18B20_TEMP_RES;

void onewire_init(void) {

}

int onewire_get_measurement(float* measurement) {

}