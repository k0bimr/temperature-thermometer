#ifndef MCG_H
#define MCG_H

#include "util.h"

#define XTAL32_REF_CLK_FREQ   32768    // in hz (32.768 kHz) | TOOD: outsource this to a external file
#define XTAL_EXT_REF_CLK_FREQ 16000000 // in hz (16 MHz)
#define SLOW_IRC_FREQ         32768    // in hz (32.768 kHz)
#define FAST_IRC_FREQ         2000000  // in hz (2 MHz)

#define NUM_MCG_MODES 9
typedef enum MCG_MODES {
    FEI,
    FEE,
    FBI,
    FBE,
    PEE,
    PBE,
    BLPI,
    BLPE,
    STOP
} MCG_MODE;

void    mcg_fei_mode(void);
void    mcg_fee_mode(void);
void    mcg_fbi_mode(void);
void    mcg_fbe_mode(void);
void    mcg_pee_mode(void);
void    mcg_pbe_mode(void);
void    mcg_blpi_mode(void);
void    mcg_blpe_mode(void);
void    mcg_stop_mode(void);

ULLONG mcg_determine_mcgoutclk_freq(void);
int    mcg_switch_mode(MCG_MODE mcg_mode);



#endif