#include "DS18B20.h"

#define DS18B20_MASTER_PULLDOWN_INIT_MIN_TIME_US          480
#define DS18B20_SLAVE_PRE_PRESENCE_PULSE_MIN_TIME_WAIT_US 15
#define DS18B20_SLAVE_PRE_PRESENCE_PULSE_MAX_TIME_WAIT_US 60
#define DS18B20_SLAVE_PRESENCE_PULSE_MAX_DURATION_TIME_US 240
#define DS18B20_WRITE_TIME_SLOT_MIN_DURATION_US           60
#define DS18B20_WRITE_TIME_SLOT_MIN_SAMPLE_TIME_US        15
#define DS18B20_WRITE_TIME_SLOT_MAX_SAMPLE_TIME_US        60
#define DS18B20_TIME_SLOT_RECOVERY_MIN_TIME               1
#define DS18B20_READ_TIME_SLOT_MIN_DURATION_US            1

#define DS18B20_TEMP_CONVERSION_IN_PROGRESS               1
#define DS18B20_TEMP_CONVERSION_READY                     0

#define DS18B20_INITIATE_WRITE_TIME_SLOT                  (iop_clr(ds18b20_dq_pin))
#define DS18B20_INITIATE_READ_TIME_SLOT                   ( \
    iop_set_output_mode(ds18b20_dq_pin), \
    iop_clr(ds18b20_dq_pin), \
    _delay_us(DS18B20_READ_TIME_SLOT_MIN_DURATION_US), \
    iop_set(ds18b20_dq_pin) \
)

#define DS18B20_SCRATCHPAD_DATA_SIZE_BYTES        9
#define DS18B20_DEFAULT_TEMP_RESOLUTION           12
#define DS18B20_SCRATCHPAD_TEMP_BITMASK           0x00000000000000FFFF // 9 bytes total / 2 bytes temp
#define DS18B20_SCRATCHPAD_TEMP_FRACT_BITMASK     0x00000000000000000F // 
#define DS18B20_SCRATCHPAD_TEMP_WHOLE_OFFSET      4
#define DS18B20_SCRATCHPAD_LSB_TEMP_WHOLE_BITMASK 0xF0
#define DS18B20_SCRATCHPAD_MSB_TEMP_WHOLE_BITMASK 0x07

UBYTE parasite_power = B_FALSE;

/*
 * Transaction Sequence
 * The transaction sequence for accessing the DS18B20 is
 * as follows:
 * Step 1. Initialization
 * Step 2. ROM Command (followed by any required data
 *         exchange)
 * Step 3. DS18B20 Function Command (followed by any
 *         required data exchange)
 * It is very important to follow this sequence every time the
 * DS18B20 is
 */


/*
 * All transactions on the 1-Wire bus begin with an initialization
 * sequence. The initialization sequence consists of a
 * reset pulse transmitted by the bus master followed by
 * presence pulse(s) transmitted by the slave(s).
 */
void DS18B20_init(void) {
    /* Go into Tx Mode */
    iop_set_output_mode(ds18b20_dq_pin);
    //CONFIG_PIN_OUT(iop_get_pdd_reg(ds18b20_dq_pin), iop_get_prt_offset(ds18b20_dq_pin));
    /*
     * During the initialization sequence the bus master transmits
     * (TX) the reset pulse by pulling the 1-Wire bus low
     * for a minimum of 480μs.
     */
    /* Make sure it's logic high first */
    //iop_set(ds18b20_dq_pin);
    iop_clr(ds18b20_dq_pin);
    _delay_us(DS18B20_MASTER_PULLDOWN_INIT_MIN_TIME_US);
    //_delay_us(10);
    /* "Release" the bus line */
    //iop_clr(ds18b20_dq_pin);
    iop_set(ds18b20_dq_pin);
    
    /* 
     * When the DS18B20 detects this rising edge, it waits
     * 15μs to 60μs and then transmits a presence pulse by pulling
     * the 1-Wire bus low for 60μs to 240μs.
     */
    /* Go into Rx mode */
    iop_set_input_mode(ds18b20_dq_pin);
    _delay_us(DS18B20_SLAVE_PRE_PRESENCE_PULSE_MAX_TIME_WAIT_US);
    //CONFIG_PIN_IN(iop_get_pdd_reg(ds18b20_dq_pin), iop_get_prt_offset(ds18b20_dq_pin));
    //_delay_us(DS18B20_SLAVE_PRE_PRESENCE_PULSE_MAX_TIME_WAIT_US);
    //SET_PIN(&GPIOC_PSOR, 5);
    /* Await Presence pulse from DS18B20 */
    while (iop_read_in(ds18b20_dq_pin));
    //_delay_ms(100);
    //CLR_PIN(&GPIOC_PCOR, 5);
    
    //while (!iop_read_in(ds18b20_dq_pin));
    _delay_us(DS18B20_SLAVE_PRESENCE_PULSE_MAX_DURATION_TIME_US);
    //_delay_us(25);
    _delay_us(DS18B20_SLAVE_PRESENCE_PULSE_MAX_DURATION_TIME_US);
    //CONFIG_PIN_OUT(iop_get_pdd_reg(ds18b20_dq_pin), iop_get_prt_offset(ds18b20_dq_pin));

}

int DS18B20_send_data(UBYTE data, int num_bits_write) {
    int ret = RET_OK;
    int i;

    iop_set_output_mode(ds18b20_dq_pin);
    /* Make sure we're starting from a logic high state */
    iop_set(ds18b20_dq_pin);
    for (i = 0; i < num_bits_write; i++) {
        DS18B20_INITIATE_WRITE_TIME_SLOT;

        if (data & (1 << i)) {
            /*
             * To generate a Write 1 time slot, after pulling the 1-Wire
             * bus low, the bus master must release the 1-Wire bus
             * within 15μs.
             */
            iop_set(ds18b20_dq_pin);
            _delay_us(DS18B20_WRITE_TIME_SLOT_MIN_DURATION_US-1);
        } else {
            /*
             * To generate a Write 0 time
             * slot, after pulling the 1-Wire bus low, the bus master must
             * continue to hold the bus low for the duration of the time
             * slot (at least 60μs).
             */
            _delay_us(DS18B20_WRITE_TIME_SLOT_MIN_DURATION_US-1);
        }
        _delay_us(40);
        iop_set(ds18b20_dq_pin);
        _delay_us(DS18B20_TIME_SLOT_RECOVERY_MIN_TIME);
    }

    _delay_us(120);
    return ret;
}

UBYTE DS18B20_read_data(int num_bits_read) {
    int   i           = 0;
    UBYTE ds18b20_out = 0;
    UBYTE data        = 0;
    
    
    if (num_bits_read <= 0 || num_bits_read > 8) {
        data = RET_FAIL;
    } else {
        for (i = 0; i < num_bits_read; i++) {
            DS18B20_INITIATE_READ_TIME_SLOT;
            iop_set_input_mode(ds18b20_dq_pin);
            _delay_us(15);
            ds18b20_out = iop_read_in(ds18b20_dq_pin);
            data |= (ds18b20_out << i);
            _delay_us(120);
        }
    }

    return data;
}

int DS18B20_read_string_data(UBYTE* data, int data_size, int num_bits_read) {
    int   ret         = RET_OK;
    UBYTE ds18b20_out = 0;
    int   i           = 0;
    int   sdatabyte   = 0;
    int   edatabit    = 0;
    int   num_read    = 0;

    if (NULL == data) {
        ret = RET_FAIL;
    } else if (data_size <= 0) {
        ret = RET_FAIL;
    } else if ((data_size * 8) < num_bits_read) {
        ret = RET_FAIL;
    } else if (0 == num_bits_read) {
        ret = RET_FAIL;
    } else {
        sdatabyte = ((num_bits_read - 1) / 8);
        edatabit  = (num_bits_read - 1) % 8;

        for (i = sdatabyte; i >= 0; i--) {
            if ((num_bits_read / 8) > 0) {
                num_read = 8;
                num_bits_read -= 8;
            } else {
                num_read = num_bits_read;
                num_bits_read = 0;
            }

            
            ds18b20_out = DS18B20_read_data(num_read);
            if (ds18b20_out < RET_OK) {
                ret = RET_FAIL;
                break;
            }

            *(data + i) = ds18b20_out;
        }
    }
    
    return ret;
}

int DS18B20_get_temp(float* temp) {
    int   ret = RET_OK;
    UBYTE scratchpad[DS18B20_SCRATCHPAD_DATA_SIZE_BYTES] = "";

    SET_PIN(&GPIOC_PSOR, 5);
    loop_delay(100);
    
    if (NULL == temp) {
        ret = RET_FAIL;
    } else {
        
        DS18B20_init();
        SET_PIN(&GPIOC_PCOR, 5);
        
        
        DS18B20_send_data(DS18B20_SKIP_ROM, 8);
        
        
        DS18B20_send_data(DS18B20_CONVERT_TEMP, 8);
        

        if (!parasite_power) {
            //while (iop_read_in(ds18b20_dq_pin) == DS18B20_TEMP_CONVERSION_IN_PROGRESS);
            _delay_ms(750);
        } else {
            _delay_ms(750);
        }

        
        DS18B20_init();
        DS18B20_send_data(DS18B20_SKIP_ROM, 8);
        DS18B20_send_data(DS18B20_READ_SCRATCHPAD, 8);
        

        
        CONFIG_PIN_IN(iop_get_pdd_reg(ds18b20_dq_pin), iop_get_prt_offset(ds18b20_dq_pin));
        DS18B20_read_string_data(
            scratchpad,
            DS18B20_SCRATCHPAD_DATA_SIZE_BYTES,
            (DS18B20_SCRATCHPAD_DATA_SIZE_BYTES * 8)
        );
        

        // Get the whole part first (last 8 bits)
        *temp = ((scratchpad[8] & DS18B20_SCRATCHPAD_LSB_TEMP_WHOLE_BITMASK) >> DS18B20_SCRATCHPAD_TEMP_WHOLE_OFFSET);
        *temp += ((scratchpad[7] & DS18B20_SCRATCHPAD_MSB_TEMP_WHOLE_BITMASK) << (DS18B20_SCRATCHPAD_TEMP_WHOLE_OFFSET));

        *temp += (((scratchpad[8] & DS18B20_SCRATCHPAD_TEMP_FRACT_BITMASK) & (1 << 3)) >> 3) / 2;
        *temp += (((scratchpad[8] & DS18B20_SCRATCHPAD_TEMP_FRACT_BITMASK) & (1 << 2)) >> 2) / 4;
        *temp += (((scratchpad[8] & DS18B20_SCRATCHPAD_TEMP_FRACT_BITMASK) & (1 << 1)) >> 1) / 8;
        *temp += (((scratchpad[8] & DS18B20_SCRATCHPAD_TEMP_FRACT_BITMASK) & (1 << 0)) >> 0) / 16;

        // temp checking for now
        //*temp = 2;
    }
    //_delay_ms(1000);
    loop_delay(100);
    

    return ret;
}
