#ifndef OLED_DISPLAY_H
#define OLED_DISPLAY_H

#define DEGREE_CHAR    -1
#define BACKSPACE_CHAR 10

#define SSD1327_ROW_SIZE       128
#define SSD1327_COL_SIZE       128
#define SSD1327_NUM_PIX_IN_ROW 128

#define SSD1327_SET_REMAP  0xA0
#define COM_SPLIT_ODD_EVEN 6
#define COM_REMAP_EN       4
#define ADDR_DIR_INC       2
#define NIBBLE_REMAP_EN    1
#define COL_ADDR_REMAP_EN  0

#define NUM_BITS_IN_BYTE         8
#define SSD1327_NUM_BITS_ENCODE  4
#define SSD1327_NUM_BYTES_IN_ROW ((SSD1327_ROW_SIZE * SSD1327_NUM_BITS_ENCODE) / NUM_BITS_IN_BYTE)

enum NIBBLE_ORDER {
    LOWER_NIBBLE_FIRST,
    HIGHER_NIBBLE_FIRST
};

typedef enum SPI_DataCommandType {
    DC_COMMAND,
    DC_DATA
} SPI_DC_T;

typedef enum SPI_COMMANDS {
    SET_COL_ADDR                           = 0x15,
    SET_ROW_ADDR                           = 0x75,
    SET_CONTRAST_CTRL                      = 0x81,
    NOP_1                                  = 0x84,
    SET_REMAP                              = 0xA0,
    SET_DISP_START_LINE                    = 0xA1,
    SET_DISP_OFFSET                        = 0xA2,
    SET_MULTIPLIER_RATIO                   = 0xA8,
    FUNCT_SELECT_A                         = 0xAB,
    SET_DISP_OFF                           = 0xAE,
    SET_DISP_ON                            = 0xAF,
    SET_PHASE_LEN                          = 0xB1,
    NOP_2                                  = 0xB2,
    SET_FRONT_CLK_DIV                      = 0xB3,
    SET_GPIO                               = 0xB5,
    SET_SECOND_PRE_CHRGE_PERIOD            = 0xB6,
    SET_GRAY_SCALE_TABLE                   = 0xB8,
    SELECT_DEFAULT_LINEAR_GRAY_SCALE_TABLE = 0xB9,
    NOP_3                                  = 0xBB,
    SET_PRE_CHARGE_VOLT                    = 0xBC,
    SET_V_COMH_VOLT                        = 0xBE,
    FUNCT_SELECT_B                         = 0xD5,
    SET_CMD_LOCK                           = 0xFD
} SPI_CMD;

typedef enum SPI_GRAPHIC_ACCEL_CMDS {
    HORIZONTAL_SCROLL_SETUP_1 = 0x26,
    HORIZONTAL_SCROLL_SETUP_2 = 0x27,
    DEACTIVATE_SCROLL         = 0x2E,
    ACTIVATE_SCROLL           = 0x2F
} SPI_GRAPH_ACCEL_CMD;

typedef enum SPI_SET_DISP_MODE_CMDS {
    NORMAL_DISP         = 0xA4,
    SET_ENTIRE_DISP_ON  = 0xA5,
    SET_ENTIRE_DISP_OFF = 0xA6,
    INVERSE_DISP        = 0xA7
} DISP_MODE_SUB_CMD;


#include "SpiDispController.h"
#include "g_io_pin.h"
#include "util.h"

/* Globals */
extern SPI_CTRL spi_pins;
extern PIOP_CONF ds18b20_dq_pin;

/* Prototypes */
void spi_init(void);
void spi_power_on_seq(void);
void spi_send_data(SPI_DC_T data_type, UBYTE data);
void spi_send_data_ex(SPI_DC_T data_type, UBYTE data, UBYTE nib_order);

void oled_display_gradient(void);
void oled_display_cross(void);
void oled_display_quad_cross(void);
void oled_entire_disp_on(void);
void oled_entire_disp_off(void);
void oled_reset_all_addr(void);
void oled_reset_bitmap_buf(void);
int  oled_draw_star_of_david(void);
int  oled_draw_line(UBYTE x1, UBYTE y1, UBYTE x2, UBYTE y2);
int  oled_draw_circle(UBYTE x1, UBYTE y1, UBYTE x2, UBYTE y2);
int  oled_draw_curve(UBYTE x0, UBYTE y0, int mx, int my, UBYTE x2, UBYTE y2);
void oled_draw_buffered_bitmap(void);
int  oled_draw_char(int c);
void oled_draw_number(int num);
void oled_set_brightness(UBYTE brightness);
void oled_move_cursor_forward(void);
void oled_move_cursor_backward(void);
void oled_turn_on(void);
void oled_turn_off(void);
int  oled_draw_string(const char* str, size_t size);

#endif