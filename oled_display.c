#include "oled_display.h"

#define IVDDREG 0 // offset for Vdd Internal Regulator control
#define MCULOCK 2 // offset for MCU interface lock
#define SET_CMD_LOCK_DATA ((1 << 4) | (1 << 1)) // hardcoded sequence for data for set cmd lock command

#define CHAR_PADDING   3 // 2 pixels
#define LINE_PADDING   (font_size / 2) // set it to half the font size

#define SSD1327_BITMAP_BUF_SIZE ((SSD1327_COL_SIZE * SSD1327_ROW_SIZE) * SSD1327_NUM_BITS_ENCODE / NUM_BITS_IN_BYTE)

// Pixel size is roughly ~0.210474752837558259921875 mm
typedef enum FONT_SIZES {
    FONT_TWO_AND_ONE_HALF   = 4,  // ~4 pixels | ~0.882 mm
    FONT_THREE              = 5,  // ~5 pixels | ~1.058 mm
    FONT_THREE_AND_ONE_HALF = 6,  // ~6 pixels | ~1.235 mm
    FONT_FOUR               = 7,  // ~7 pixels | ~1.411 mm
    FONT_FOUR_AND_ONE_HALF  = 8,  // ~8 pixels | ~1.588 mm
    FONT_FIVE               = 9,  // ~9 pixels | ~1.764 mm
    FONT_FIVE_AND_ONE_HALF  = 10, // ~10 pixels | ~1.940 mm
    FONT_SIX                = 11  // ~11 pixels | ~
} FONT;

typedef enum BRIGHTNESS_LEVELS {
    OLED_BRIGHTNESS_0,
    OLED_BRIGHTNESS_1,
    OLED_BRIGHTNESS_2,
    OLED_BRIGHTNESS_3,
    OLED_BRIGHTNESS_4,
    OLED_BRIGHTNESS_5,
    OLED_BRIGHTNESS_6,
    OLED_BRIGHTNESS_7,
    OLED_BRIGHTNESS_8,
    OLED_BRIGHTNESS_9,
    OLED_BRIGHTNESS_10,
    OLED_BRIGHTNESS_11,
    OLED_BRIGHTNESS_12,
    OLED_BRIGHTNESS_13,
    OLED_BRIGHTNESS_14,
    OLED_BRIGHTNESS_15,
} OLED_BRIGHTNESS;

volatile UBYTE  oled_display_status                         = B_TRUE;
UBYTE           SSD1327_bitmap_buf[SSD1327_BITMAP_BUF_SIZE] = { 0 };
FONT            font_size                                   = FONT_THREE;
OLED_BRIGHTNESS brightness                                  = OLED_BRIGHTNESS_15;



#undef DEBUG_OLED_PRINTING
#ifdef DEBUG_OLED_PRINTING
UBYTE debug_record = B_TRUE;
int debug_var1;
int debug_var2;
#endif

void spi_send_data(SPI_DC_T data_type, UBYTE data) {
    spi_send_data_ex(data_type, data, HIGHER_NIBBLE_FIRST);
}

void spi_send_data_ex(SPI_DC_T data_type, UBYTE data, UBYTE nib_order) {
    int i;

    /* Bring DC (Data-Command) to proper level */
    if (DC_COMMAND == data_type) {
        iop_clr(spi_pins.dc_pin);
    } else if (DC_DATA == data_type) {
        iop_set(spi_pins.dc_pin);
    }

    /* Set CLK LOW */
    iop_clr(spi_pins.clk_pin);

    /* Set CS LOW */
    iop_clr(spi_pins.cs_pin);

    /* Set DIN LOW */
    iop_clr(spi_pins.din_pin);

    if (HIGHER_NIBBLE_FIRST == nib_order) {
        /* Sending MSB first */
        for (i = 7; i >= 0; i--) {
            /* Load bit of data into DIN pin register */
            if ((data) & (1 << i)) {
                iop_set(spi_pins.din_pin);
            } else {
                iop_clr(spi_pins.din_pin);
            }

            /* Set CLK HIGH to send bit */
            iop_set(spi_pins.clk_pin);

            /* Set CLK LOW to stop slave from reading */
            iop_clr(spi_pins.clk_pin);
        }
    } else {
        /* Sending MSB first */
        /* Sending lower nibble first */
        for (i = 3; i >= 0; i--) {
            /* Load bit of data into DIN pin register */
            if ((data) & (1 << i)) {
                iop_set(spi_pins.din_pin);
            } else {
                iop_clr(spi_pins.din_pin);
            }

            /* Set CLK HIGH to send bit */
            iop_set(spi_pins.clk_pin);

            /* Set CLK LOW to stop slave from reading */
            iop_clr(spi_pins.clk_pin);
        }

        /* Sending higher nibble last */
        for (i = 7; i >= 4; i--) {
            /* Load bit of data into DIN pin register */
            if ((data) & (1 << i)) {
                iop_set(spi_pins.din_pin);
            } else {
                iop_clr(spi_pins.din_pin);
            }

            /* Set CLK HIGH to send bit */
            iop_set(spi_pins.clk_pin);

            /* Set CLK LOW to stop slave from reading */
            iop_clr(spi_pins.clk_pin);
        }
    }
    

    /* Pull CS HIGH */
    iop_set(spi_pins.cs_pin);
}

void spi_init(void) {
    spi_power_on_seq();

    /* Bring CLK LOW */
    iop_clr(spi_pins.clk_pin);

    /* Make sure the display is off */
    spi_send_data(DC_COMMAND, SET_DISP_OFF);

    /* Enable Internal Vdd Regulator */
    spi_send_data(DC_COMMAND, FUNCT_SELECT_A);
    spi_send_data(DC_COMMAND, (1 << IVDDREG));

    /* Unlock the MCU interface so that it can accept our SPI commands */
    spi_send_data(DC_COMMAND, SET_CMD_LOCK);
    spi_send_data(DC_COMMAND, (SET_CMD_LOCK_DATA & ~(1 << MCULOCK)));

    /* Set Re-Map */
    spi_send_data(DC_COMMAND, SET_REMAP);
    spi_send_data(DC_COMMAND, 0x00);
}

void spi_power_on_seq(void) {
    /* Pull RST pin High to do normal operations */
    iop_set(spi_pins.rst_pin);

    _delay_ms(1);

    /* Pull RST pin low to do initialization hardware */
    iop_clr(spi_pins.rst_pin);

    _delay_us(100);

    /* Pull RST pin High to do normal operations */
    iop_set(spi_pins.rst_pin);

    //loop_delay(1);
    _delay_us(100);

}

void oled_display_gradient(void) {
    int i = 0;

    for (i = 0; i < 8192; i++) {
        spi_send_data(DC_DATA, (i % 16));
    }
}

void oled_display_cross(void) {
    int i = 0;
    int q = 0;

    for (i = 0; i < SSD1327_ROW_SIZE; i+=2) {
        for (q = 0; q < SSD1327_NUM_BYTES_IN_ROW; q++) {
            if (q == (i / 2)) {
                spi_send_data(DC_DATA, (0xF << (i - (q * 2))));
            } else if (q == (SSD1327_NUM_BYTES_IN_ROW - (i / 2) - 1)) {
                spi_send_data(DC_DATA, (0xF << (SSD1327_ROW_SIZE - ((q * 2) + i))));
            }    
            else {
                spi_send_data(DC_DATA, 0x00);
            }
        }
    }

    for (i = 1; i < SSD1327_ROW_SIZE; i+=2) {
        for (q = 0; q < SSD1327_NUM_BYTES_IN_ROW; q++) {
            if (q == (i / 2)) {
                spi_send_data(DC_DATA, (0xF << (i - (q * 2))));
            } else if (q == (SSD1327_NUM_BYTES_IN_ROW - (i / 2) - 1)) {
                spi_send_data(DC_DATA, (0xF << (SSD1327_ROW_SIZE - ((q * 2) + i))));
            } else {
                spi_send_data(DC_DATA, 0x00);
            }
        }
    }
}

void oled_display_quad_cross(void) {

}

void send_by_nibble_msb(UBYTE data) {
    int i;

    /* Start with the higher nibble */

    /* End with the lower nibble */
}

int oled_draw_line(UBYTE x1, UBYTE y1, UBYTE x2, UBYTE y2) {
    int   ret     = RET_OK;
    int   x       = 0;
    int   x_min   = 0;
    int   x_max   = 0;
    int   y       = 0;
    int   y_min   = 0;
    int   y_max   = 0;
    int   y_ref   = 0;
    int   i       = 0;

    int   rise    = 0;
    int   run     = 0;

    float u_x     = 0;
    float u_y     = 0;

    float proj_x  = 0;
    float proj_y  = 0;
    float proj_co = 0;

    float dist    = 0;

    float p_x     = 0;
    float p_y     = 0;

    if ((x1 < 0 || y1 < 0 || x2 < 0 || y2 < 0) || 
        (x1 >= SSD1327_COL_SIZE || y1 >= SSD1327_ROW_SIZE ||
         x2 >= SSD1327_COL_SIZE || y2 >= SSD1327_ROW_SIZE)) {
        ret = RET_FAIL;
    } else {
        x_min = N_MIN(x1, x2);
        x_max = N_MAX(x1, x2);

        y_min = N_MIN(y1, y2);
        y_max = N_MAX(y1, y2);

        if (x2 > x1) {
            y_ref = y1;
            rise = (y2 - y1);
            run  = (x2 - x1);
        } else {
            y_ref = y2;
            rise = (y1 - y2);
            run  = (x1 - x2);
        }
        

        for (y = y_min; y <= y_max; y++) {
            for (x = x_min; x <= x_max; x++) {
                /* 1. Determine line u going through (x_min, y_ref) and (x, y) */
                u_x = (x - x_min);
                u_y = (y - y_ref);

                /* 2. Determine direction vector d of the line to draw */

                /* 3. Determine projection of u onto d */
                proj_co = (float)((u_x * run) + (u_y * rise)) / (float)((run * run) + (rise * rise));
                proj_x = proj_co * run;
                proj_y = proj_co * rise;

                p_x = (u_x - proj_x);
                p_y = (u_y - proj_y);

                dist = sqrt((float)(p_x * p_x) + (p_y * p_y));

                if ((dist < 0.5f) || (x == x1 && y == y1) || (x == x2 && y == y2)) {
                    if (x % 2 == 0) {
                        SSD1327_bitmap_buf[(x / 2) + (y * SSD1327_NUM_BYTES_IN_ROW)] |= (0x0F << 4);
                    } else {
                        SSD1327_bitmap_buf[(x / 2) + (y * SSD1327_NUM_BYTES_IN_ROW)] |= 0x0F;
                    }
                }
            }
        }
    }

    return ret;
}

#define DISTANCE_FUNCTION(p_x0,p_y0,p_mx,p_my,p_x2,p_y2,x,y,t) (\
    sqrt( \
        ((x- \
            (p_mx+ \
                ((1-t)*(1-t)*(p_x0-p_mx))+ \
                (t*t*(p_x2-p_mx)) \
            ) \
        )* \
        (x- \
            (p_mx+ \
                ((1-t)*(1-t)*(p_x0-p_mx))+ \
                (t*t*(p_x2-p_mx)) \
            ) \
        ))+ \
        ((y- \
            (p_my+ \
                ((1-t)*(1-t)*(p_y0-p_my))+ \
                (t*t*(p_y2-p_my)) \
            ) \
        )* \
        (y- \
            (p_my+ \
                ((1-t)*(1-t)*(p_y0-p_my))+ \
                (t*t*(p_y2-p_my)) \
            ) \
        )) \
    ) \
)

#define DISTANCE_FIRST_DERIV_FUNCTION(t) (\
    \
)

#define DISTANCE_SECOND_DERIV_FUNCTION(t) (\
    \
)

#define DISTANCE_THIRD_DERIV_FIND_T(c0,c1,c2,c3,c4,c5,c6,c7) (\
    (((24*c6)-(24*c5)*c7)-(48*c6*c6)+(72*c5*c6)-(24*c5*c5)+((24*c2)-(24*c1)*c3)-(48*c2*c2)+(72*c1*c2)-(24*c1*c1)) / \
    ((24*c7*c7)+(((48*c5)-(96*c6))*c7)+(96*c6*c6)-(96*c5*c6)+(24*c5*c5)+(24*c3*c3)+(((48*c1)-(96*c2))*(c5))+(96*c2*c2)-(96*c1*c2)+(24*c1*c1)) \
)

#define QR_NUM_ITERATIONS 150
int oled_draw_curve(UBYTE x0, UBYTE y0, int mx, int my, UBYTE x2, UBYTE y2) {
	int   ret = RET_OK;

	int   x = 0;
	int   x_min = 0;
	int   x_max = 0;
	int   y = 0;
	int   y_min = 0;
	int   y_max = 0;

	int   i = 0;

	int x_cords[3] = { 0 };
	int y_cords[3] = { 0 };

	float a_k_matrix[3][3] = { 0 }; // Starts off as Companion Matrix for our monic polynomial
	float f_k_matrix[3][3] = { 0 };
	float q_k_matrix[3][3] = { 0 };
	float r_k_matrix[3][3] = { 0 };

	float GS_f1_coef = 0; // Gram-Schmidt coefficient for k=2
	float GS_f2_coef = 0; // Gram-Schmidt coefficient for k=3

	float q_coef = 0;

	float root = 0;
	float min_dist = 0;
	float temp_dist = 0;

	x_cords[i++] = x0;
	x_cords[i++] = mx;
	x_cords[i++] = x2;

	i = 0;
	y_cords[i++] = y0;
	y_cords[i++] = my;
	y_cords[i++] = y2;

	/* Find x minimum and maximum */
	x_min = (int)x0;
	x_max = (int)x0;

	y_min = (int)y0;
	y_max = (int)y0;

	/* Find mins and maxs */
	for (i = 0; i < 3; i++) {
		if (x_cords[i] < x_min) {
			x_min = (int)x_cords[i];
		}
		if (x_cords[i] > x_max) {
			x_max = (int)x_cords[i];
		}
		if (y_cords[i] < y_min) {
			y_min = (int)y_cords[i];
		}
		if (y_cords[i] > y_max) {
			y_max = (int)y_cords[i];
		}
	}

	if (x_min < 0) {
		x_min = 0;
	}

	if (x_max >= SSD1327_NUM_PIX_IN_ROW) {
		x_max = SSD1327_NUM_PIX_IN_ROW - 1;
	}

	if (y_min < 0) {
		y_min = 0;
	}

	if (y_max >= SSD1327_COL_SIZE) {
		y_max = SSD1327_COL_SIZE - 1;
	}

#if DEBUG_OLED_PRINTING
    if (debug_record) {
        debug_var1 = y_min;
        debug_var2 = y_max;

        debug_record = B_FALSE;
    }
    
    oled_draw_buffered_bitmap();
#endif
#if 0
	if (mx > ((x_max - x_min) / 2)) {
		mx += mx;
	}
	else if (mx < ((x_max - x_min) / 2)) {
		mx -= mx;
	}
	else {
		// no adjustments if it's on the line of (x0, y0), (x2, y2)
	}

	if (my > ((y_max - y_min) / 2)) {
		my += my;
	}
	else if (my < ((y_max - y_min) / 2)) {
		my -= my;
	}
	else {
		// no adjustments if it's on the line of (y0, y0), (y2, y2)
	}
#endif

	const int c1 = my - y0;
	const int c2 = mx - x0;
	const int c3 = y0 * my;
	const int c4 = x0 * mx;
	const int c5 = y0 * y0;
	const int c6 = x0 * x0;
	const int c7 = my * my;
	const int c8 = mx * mx;
	const int c9 = y0 - my;
	const int c10 = x0 - mx;
	const int c11 = y2 * y2;
	const int c12 = x2 * x2;
	const int c13 = (c3)+(c4)-(c5)-(c6);
	const int c14 = (-1 * y2) + (2 * my) - y0;
	const int c15 = (-1 * x2) + (2 * mx) - x0;
	const int c16 = y0 * y2 + x0 * x2 + 2 * c7 - 6 * c3 + 2 * c8 - 6 * c4 + 3 * c5 + 3 * c6;
	const int c17 = (3 * my - 3 * y0) * y2 + (3 * mx - 3 * x0) * x2 - 6 * c7 + 9 * c3 - 6 * c8 + 9 * c4 - 3 * c5 - 3 * c6;
	const int denom = (c11 + (2 * y0 - 4 * my) * y2 + c12 + (2 * x0 - 4 * mx) * x2 + 4 * c7 - 4 * c3 + 4 * c8 - 4 * c4 + c5 + c6);

	if ((x0 < 0 || y0 < 0 || /*mx < 0 || my < 0 ||*/ x2 < 0 || y2 < 0) ||
		(x0 >= SSD1327_COL_SIZE || y0 >= SSD1327_ROW_SIZE ||
			/*mx >= SSD1327_COL_SIZE || my >= SSD1327_ROW_SIZE ||*/
			x2 >= SSD1327_COL_SIZE || y2 >= SSD1327_ROW_SIZE)) {
		ret = RET_FAIL;
	}
	else if (0 == denom) {
		ret = oled_draw_line(x0, y0, x2, y2);
	}
	else {
		for (y = y_min; y <= y_max; y++) {
			for (x = x_min; x <= x_max; x++) {
				/* Constructing Companion Maxtrix */
				a_k_matrix[0][0] = 0;
				a_k_matrix[0][1] = 1;
				a_k_matrix[0][2] = 0;

				a_k_matrix[1][0] = 0;
				a_k_matrix[1][1] = 0;
				a_k_matrix[1][2] = 1;

				a_k_matrix[2][0] = -1.0f * (float)((c9)*y + (c10)*x + c13) / (float)denom;
				a_k_matrix[2][1] = -1.0f * (float)((c14)*y + (c15)*x + c16) / (float)denom;
				a_k_matrix[2][2] = -1.0f * (float)(c17) / (float)denom;

				if (a_k_matrix[2][0] != 0 || a_k_matrix[2][1] != 0 || a_k_matrix[2][2] != 0) {

					for (i = 0; i < QR_NUM_ITERATIONS; i++) {
						/* First we find orthogonal columns to the maxtrix A. */
						// k = 1
						f_k_matrix[0][0] = a_k_matrix[0][0];
						f_k_matrix[1][0] = a_k_matrix[1][0];
						f_k_matrix[2][0] = a_k_matrix[2][0];

						GS_f1_coef = (a_k_matrix[0][1] * f_k_matrix[0][0] + a_k_matrix[1][1] * f_k_matrix[1][0] + a_k_matrix[2][1] * f_k_matrix[2][0]) / (f_k_matrix[0][0] * f_k_matrix[0][0] + f_k_matrix[1][0] * f_k_matrix[1][0] + f_k_matrix[2][0] * f_k_matrix[2][0]);

						// k = 2
						f_k_matrix[0][1] = a_k_matrix[0][1] - (GS_f1_coef * f_k_matrix[0][0]);
						f_k_matrix[1][1] = a_k_matrix[1][1] - (GS_f1_coef * f_k_matrix[1][0]);
						f_k_matrix[2][1] = a_k_matrix[2][1] - (GS_f1_coef * f_k_matrix[2][0]);

						GS_f1_coef = (a_k_matrix[0][2] * f_k_matrix[0][0] + a_k_matrix[1][2] * f_k_matrix[1][0] + a_k_matrix[2][2] * f_k_matrix[2][0]) / (f_k_matrix[0][0] * f_k_matrix[0][0] + f_k_matrix[1][0] * f_k_matrix[1][0] + f_k_matrix[2][0] * f_k_matrix[2][0]);
						GS_f2_coef = (a_k_matrix[0][2] * f_k_matrix[0][1] + a_k_matrix[1][2] * f_k_matrix[1][1] + a_k_matrix[2][2] * f_k_matrix[2][1]) / (f_k_matrix[0][1] * f_k_matrix[0][1] + f_k_matrix[1][1] * f_k_matrix[1][1] + f_k_matrix[2][1] * f_k_matrix[2][1]);

						// k = 3
						f_k_matrix[0][2] = a_k_matrix[0][2] - (GS_f1_coef * f_k_matrix[0][0]) - (GS_f2_coef * f_k_matrix[0][1]);
						f_k_matrix[1][2] = a_k_matrix[1][2] - (GS_f1_coef * f_k_matrix[1][0]) - (GS_f2_coef * f_k_matrix[1][1]);
						f_k_matrix[2][2] = a_k_matrix[2][2] - (GS_f1_coef * f_k_matrix[2][0]) - (GS_f2_coef * f_k_matrix[2][1]);

						/* Now we make them orthonormal */
						q_coef = 1 / sqrt(f_k_matrix[0][0] * f_k_matrix[0][0] + f_k_matrix[1][0] * f_k_matrix[1][0] + f_k_matrix[2][0] * f_k_matrix[2][0]);

						q_k_matrix[0][0] = q_coef * f_k_matrix[0][0];
						q_k_matrix[1][0] = q_coef * f_k_matrix[1][0];
						q_k_matrix[2][0] = q_coef * f_k_matrix[2][0];

						q_coef = 1 / sqrt(f_k_matrix[0][1] * f_k_matrix[0][1] + f_k_matrix[1][1] * f_k_matrix[1][1] + f_k_matrix[2][1] * f_k_matrix[2][1]);

						q_k_matrix[0][1] = q_coef * f_k_matrix[0][1];
						q_k_matrix[1][1] = q_coef * f_k_matrix[1][1];
						q_k_matrix[2][1] = q_coef * f_k_matrix[2][1];

						q_coef = 1 / sqrt(f_k_matrix[0][2] * f_k_matrix[0][2] + f_k_matrix[1][2] * f_k_matrix[1][2] + f_k_matrix[2][2] * f_k_matrix[2][2]);

						q_k_matrix[0][2] = q_coef * f_k_matrix[0][2];
						q_k_matrix[1][2] = q_coef * f_k_matrix[1][2];
						q_k_matrix[2][2] = q_coef * f_k_matrix[2][2];


						/* Now we construct our R matrix */
						r_k_matrix[0][0] = sqrt(f_k_matrix[0][0] * f_k_matrix[0][0] + f_k_matrix[1][0] * f_k_matrix[1][0] + f_k_matrix[2][0] * f_k_matrix[2][0]);
						r_k_matrix[0][1] = (a_k_matrix[0][1] * q_k_matrix[0][0] + a_k_matrix[1][1] * q_k_matrix[1][0] + a_k_matrix[2][1] * q_k_matrix[2][0]);
						r_k_matrix[0][2] = (a_k_matrix[0][2] * q_k_matrix[0][0] + a_k_matrix[1][2] * q_k_matrix[1][0] + a_k_matrix[2][2] * q_k_matrix[2][0]);

						r_k_matrix[1][0] = 0;
						r_k_matrix[1][1] = sqrt(f_k_matrix[0][1] * f_k_matrix[0][1] + f_k_matrix[1][1] * f_k_matrix[1][1] + f_k_matrix[2][1] * f_k_matrix[2][1]);
						r_k_matrix[1][2] = (a_k_matrix[0][2] * q_k_matrix[0][1] + a_k_matrix[1][2] * q_k_matrix[1][1] + a_k_matrix[2][2] * q_k_matrix[2][1]);

						r_k_matrix[2][0] = 0;
						r_k_matrix[2][1] = 0;
						r_k_matrix[2][2] = (float)sqrt((float)((f_k_matrix[0][2] * f_k_matrix[0][2]) + (f_k_matrix[1][2] * f_k_matrix[1][2]) + (f_k_matrix[2][2] * f_k_matrix[2][2])));

						/* Let's compute new A matrix */
						matrix_multiply(&r_k_matrix[0][0], &q_k_matrix[0][0], 3, 3, 3, 3, &a_k_matrix[0][0]);
					}

					min_dist = FLT_MAX;
					for (i = 0; i < 3; i++) {
						/* Now let's process our approximated roots */
						root = a_k_matrix[i][i];
						if (root >= 0 && root <= 1) {
							temp_dist = DISTANCE_FUNCTION(x0, y0, mx, my, x2, y2, x, y, root);
							if (temp_dist < min_dist) {
								min_dist = temp_dist;
							}
						}
					}
				}

				if (min_dist <= 0.5f || (x == x0 && y == y0) || (x == x2 && y == y2)) {
					if (x % 2 == 0) {
						SSD1327_bitmap_buf[(x / 2) + (y * SSD1327_NUM_BYTES_IN_ROW)] |= (0x0F << 4);
					}
					else {
						SSD1327_bitmap_buf[(x / 2) + (y * SSD1327_NUM_BYTES_IN_ROW)] |= 0x0F;
					}
				}
			}
		}
	}

	return ret;
}

int quad_bezier_curve(UBYTE x1, UBYTE y1, UBYTE x2, UBYTE y2, UBYTE mx, UBYTE my, float ratio) {

}

int oled_draw_circle(UBYTE x1, UBYTE y1, UBYTE x2, UBYTE y2) {
    int   ret    = RET_OK;
    float radius = 0.0f;


    return ret;
}

int oled_draw_star_of_david(void) {
    int ret = RET_OK;

    oled_draw_line(64, 32, 32, 82);
    oled_draw_line(64, 32, 96, 82);
    oled_draw_line(32, 82, 96, 82);
    oled_draw_line(32, 50, 96, 50);
    oled_draw_line(32, 50, 64, 100);
    oled_draw_line(96, 50, 64, 100);

    return ret;
}

void oled_draw_buffered_bitmap(void) {
    int i;
    int q;
    int offset = 0;

    for (i = 0; i < 128; i++) {
        for (q = 0; q < 64; q++) {
            spi_send_data_ex(DC_DATA, SSD1327_bitmap_buf[q + offset + (i * 64)], LOWER_NIBBLE_FIRST);
        }
    }
}

int swap_nibble(UBYTE* data) {

}

void oled_entire_disp_on(void) {
    int i;

    oled_reset_all_addr();

    for (i = 0; i < sizeof(SSD1327_bitmap_buf); i++) {
        SSD1327_bitmap_buf[i] = 0xFF;
    }

    oled_draw_buffered_bitmap();
}

void oled_entire_disp_off(void) {
    int i;

    oled_reset_all_addr();

    for (i = 0; i < sizeof(SSD1327_bitmap_buf); i++) {
        SSD1327_bitmap_buf[i] = 0x00;
    }

    oled_draw_buffered_bitmap();
}

void oled_reset_all_addr(void) {
    /* Reset Column Address */
    spi_send_data(DC_COMMAND, SET_COL_ADDR);
    spi_send_data(DC_COMMAND, 0x00); // Start Addr
    spi_send_data(DC_COMMAND, 0x3F); // End Addr

    /* Reset Row Address */
    spi_send_data(DC_COMMAND, SET_ROW_ADDR);
    spi_send_data(DC_COMMAND, 0x00); // Start Addr
    spi_send_data(DC_COMMAND, 0x7F); // End Addr
}

int char_index = 0;
int line_index = 0;

void oled_reset_bitmap_buf(void) {
    int i;
    char_index = 0;
    line_index = 0;

    for (i = 0; i < sizeof(SSD1327_bitmap_buf); i++) {
        SSD1327_bitmap_buf[i] = 0;
    }
}

int oled_draw_char(int c) {
    UBYTE valid_char = B_TRUE;
    UBYTE inc_char   = B_TRUE;
    int i;
    int q;

    switch (c) {
        case BACKSPACE_CHAR:
            char_index -= 1;
            inc_char = B_FALSE;
            for (
                i = (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING);
                i <= (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size;
                i++
            ) {
                for (
                    q = (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING);
                    q <= (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size;
                    q++
                ) {
                    if (i % 2 == 0) {
						SSD1327_bitmap_buf[(i / 2) + (q * SSD1327_NUM_BYTES_IN_ROW)] &= ~(0x0F << 4);
					}
					else {
						SSD1327_bitmap_buf[(i / 2) + (q * SSD1327_NUM_BYTES_IN_ROW)] &= ~0x0F;
					}
                }
            }
            break;

        case 0:
        case '0':
            // Draw over-arching curve '^'
            oled_draw_curve(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) - (font_size / 2),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size,
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2)
            );
            // Draw under-arching curve 'u'
            oled_draw_curve(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (3 * (font_size / 2)),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size,
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2)
            );
            break;

        case 1:
        case '1':
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 4)
            );
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size
            );
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size,
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size,
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size
            );
            break;

        case 2:
        case '2':
            // Draw over-arching curve '^'
            oled_draw_curve(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 4),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) - (font_size / 4),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size,
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 4)
            );
            // Draw Diagonal line '/'
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size,
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 4),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size
            );
            // Draw horizontal line '_'
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size,
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size,
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size
            );
            break;

        case 3:
        case '3':
            oled_draw_curve(
               (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
               (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
               (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size,
               (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 4),
               (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 4),
               (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2)
            );
            oled_draw_curve(
               (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 4),
               (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
               (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size,
               (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (3 * (font_size / 4)),
               (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
               (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size
            );
            break;

        case 4:
        case '4':
            // Draw diagonal straight line component '/'
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (3 * (font_size / 4)),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (3 * (font_size / 4))
            );
            // Draw horizontal straight line component '-'
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (3 * (font_size / 4)),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size,
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (3 * (font_size / 4))
            );
            // Draw vertical straight line component '|'
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (3 * (font_size / 4)),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (3 * (font_size / 4)),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size
            );
            break;

        case 5:
        case '5':
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size,
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING)
            );
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2)
            );
            oled_draw_curve(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size,
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (3 * (font_size / 4)),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size
            );
            break;

        case 6:
        case '6':
            oled_draw_curve(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size,
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size
            );
            oled_draw_curve(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size,
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size,
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2)
            );
            break;

        case 7:
        case '7':
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size,
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING)
            );
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size,
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size
            );
            break;

        case 8:
        case '8':
            oled_draw_curve(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 4),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2)
            );
            oled_draw_curve(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size,
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 4),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2)
            );
            oled_draw_curve(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (3 * (font_size / 4)),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size
            );
            oled_draw_curve(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size,
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (3 * (font_size / 4)),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size
            );
            break;

        case 9:
        case '9':
            oled_draw_curve(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 4),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2)
            );
            oled_draw_curve(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size)
            );
            break;

        case 'a':
        case 'b':
        case 'c':
        case 'd':
            break;

        case 'e':
            // Draw horizontal straight line '-'
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2), 
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size, 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2)
            );
            // Draw over-arching curve '^'
            oled_draw_curve(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size, 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2), 
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) - (font_size / 2),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2)
            );
            // Draw under crescent
            oled_draw_curve(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2), 
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size, 
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size
            );
            break;

        case 'f':
        case 'g':
        case 'h':
        case 'i':
        case 'j':
        case 'k':
        case 'l':
            break;

        case 'm':
            // Draw 'm'
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2), 
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size
            );
            oled_draw_curve(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2), 
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2), 
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size
            );
            oled_draw_curve(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size, 
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2), 
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size, 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2)
            );
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size, 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2), 
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size,
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size
            );
            break;

        case 'n':                                               
        case 'o':
            break;

        case 'p':
            // Draw 'p'
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 3), 
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size
            );
            oled_draw_curve(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 4), 
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (4 * font_size / 3), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2), 
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (3 * (font_size / 4))
            );
            break;

        case 'q':
        case 'r':
        case 's':
        case 't':
        case 'u':
        case 'v':
        case 'w':
        case 'x':
        case 'y':
        case 'z':
            break;

        case 'A':
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size)
            );
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size)
            );
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 4),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (3 * (font_size / 4)),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (3 * (font_size / 4)),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (3 * (font_size / 4))
            );
            break;
        case 'B':
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size)
            );
            oled_draw_curve(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size,
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 4),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2)
            );
            oled_draw_curve(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size,
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (3 * (font_size / 4)),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size)
            );
            break;
        case 'C':
            oled_draw_curve(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) - (font_size / 2),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size)
            );
            break;

        case 'D':
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size)
            );
            oled_draw_curve(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size,
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size)
            );
            break;
        case 'E':
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size)
            );
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING)
            );
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2)
            );
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size)
            );
            break;
        case 'F':
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size)
            );
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING)
            );
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2)
            );
            break;
        case 'G':
        case 'H':
        case 'I':
        case 'J':
        case 'K':
        case 'L':
        case 'M':
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size)
            );
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (3 * (font_size / 4))
            );
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (3 * (font_size / 4)),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING)
            );
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size)
            );
            break;
        case 'N':
        case 'O':
        case 'P':
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size)
            );
            oled_draw_curve(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING), 
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (4 * font_size / 3), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 4), 
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2)
            );
            break;
        case 'Q':
        case 'R':
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size)
            );
            oled_draw_curve(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING), 
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (4 * font_size / 3), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 4), 
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2)
            );
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size)
            );
        case 'S':
            break;

        case 'T':
            // Draw 'T'
            // font size, current char index, padding
            // starting coords dictated by [char_index] + [padding]
            // starting x = (char_index * (font_size + padding) + padding)
            // starting y = (line_index * (font_size + padding) + padding)
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING), 
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size, 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING)
            );
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING), 
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + font_size
            );
            break;

        case 'U':
        case 'V':
        case 'W':
        case 'X':
        case 'Y':
        case 'Z':
            break;

        case ':':
            // Draw ':'
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 3), 
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 3)
            );
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (2 * (font_size / 3)),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (2 * (font_size / 3))
            );
            //oled_draw_line(39, 14, 39, 14);
            break;

        case '!':
        case ',':
            break;

        case '-':
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 4), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (3 * (font_size / 4)), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2)
            );
            break;
        
        case '_':
            oled_draw_line(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size), 
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size)
            );
            break;

        case ' ':
            // TODO: loop through all pixels in area and set to 0
            break;
        case DEGREE_CHAR:
            oled_draw_curve(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 4),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 4),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) - (font_size / 4),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 4)
            );
            oled_draw_curve(
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 4),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 4),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 2),
                (line_index * (font_size + CHAR_PADDING) + CHAR_PADDING) + (font_size / 4)
            );
            break;

        default:
            valid_char = B_FALSE;
            break;
    }

    if (valid_char && inc_char) {
        char_index++;
    }

    if ((char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) >= SSD1327_NUM_PIX_IN_ROW) {
        char_index = 0;
        line_index++;
    }
}

void oled_draw_number(int num) {
    UBYTE num_digits   = 0;
    int   i            = 0;
    int   q            = 0;
    int   divisor      = 1;

    if (num < 0) {
        num = N_ABS(num);
        oled_draw_char('-');
    }

    if (num > 0) {
        num_digits = log10(num);
    }
    
    for (i = num_digits; i >= 0; i--) {
        divisor = 1;
        for (q = 0; q < i; q++) {
            divisor *= 10;
        }

        oled_draw_char((num / divisor));
        
        num -= ((num / divisor) * divisor);
    }

}

void oled_set_brightness(UBYTE brightness) {
    int i;

    if (OLED_BRIGHTNESS_0 == brightness) {
        oled_turn_off();
    } else {
        for (i = 0; i < sizeof(SSD1327_bitmap_buf); i++) {
            if (SSD1327_bitmap_buf[i] & (0xFF << ((i % 2) + 1))) {
                SSD1327_bitmap_buf[i] = SSD1327_bitmap_buf[i] & ~(0xFF << ((i % 2) + 1)) | (brightness << ((i % 2) + 1));
            }
        }
        if (!oled_display_status) {
            oled_turn_on();
        }
        oled_draw_buffered_bitmap();
    }
    
}

void oled_move_cursor_forward(void) {
    char_index++;

    if ((char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) >= SSD1327_NUM_PIX_IN_ROW) {
        char_index = 0;
        line_index++;
    }
}

void oled_move_cursor_backward(void) {
    char_index--;

    if ((char_index * (font_size + CHAR_PADDING) + CHAR_PADDING) <= 0) {
        char_index = (SSD1327_NUM_PIX_IN_ROW / (font_size + CHAR_PADDING)) - 1;
        line_index--;
    }
}

void oled_turn_on(void) {
    spi_send_data(DC_COMMAND, SET_DISP_ON);
    oled_display_status = B_TRUE;
}

void oled_turn_off(void) {
    spi_send_data(DC_COMMAND, SET_DISP_OFF);
    oled_display_status = B_FALSE;
}

int oled_draw_string(const char* str, size_t size) {
    int i;
    int ret = RET_OK;

    if (size < 0) {
        ret = RET_FAIL;
    } else if (NULL == str) {
        ret = RET_FAIL;
    } else {
        for (i = 0; i < size; i++) {
            oled_draw_char(*(str + i));
        }
    }

    return ret;
}