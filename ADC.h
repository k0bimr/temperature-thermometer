#ifndef ADC_H
#define ADC_H

#include "MK20DX256.h"
#include "util.h"
#include "SpiDispController.h"

#define ARD_PIN_TO_ADC_CH_MAP_SIZE 15
#define ADC_MAX_SINGLE_ENDED_CHANNELS 24

#define ADC_8_BIT_CONV_MODE       ((0 << MODE1) | (0 << MODE0))
#define ADC_SOFTWARE_TRIGGER_MODE (0 << ADTRG)
#define ADC_INITIATE_CALIBARTION  (ADC0_SC3 |= (1 << CAL))
#define ADC_CALIBRATION_COMPLETED (ADC0_SC1A & (1 << COCO))
#define ADC_CONVERSION_COMPLETED  (ADC0_SC1A & (1 << COCO))
#define ADC_RETRIEVE_CONVERSION   (ADC0_RA & ADC0_RA_D_BITMASK)

extern const UBYTE ard_pin_to_adc_ch_map[];

void adc_module_init(void);
void adc_initiate_calibration(void);
void adc_complete_calibration(void);
void adc_perform_calibration(void);
int  adc_perform_conversion(UBYTE in_ch, int* analog_ret);
int  adc_check_conversion_completed(void);

#endif